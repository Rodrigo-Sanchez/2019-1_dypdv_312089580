var GameEngine = (function(GameEngine) {
  const PI2 = 2*Math.PI;
  const PI_180 = Math.PI/180;

  class Ball {
    constructor(x, y, size, color) {
      this.x = x;
      this.y = y;
      this.size = size;
      this.color = color;

      this.mass = 1;
      this.speed = 50 +Math.random()*300;
      this.angle = (360 * Math.random()) * PI_180;
      this.vx = Math.cos(this.angle) * this.speed;
      this.vy = Math.sin(this.angle) * this.speed;
    }

    update(elapsed) {
      this.x += this.vx * elapsed;
      this.y += this.vy * elapsed;
    }

    render(ctx, index) {
      ctx.fillStyle = this.color;
      ctx.beginPath();
      ctx.arc(this.x, this.y, this.size, 0, PI2);
      ctx.fill();
    }
  }

  GameEngine.Ball = Ball;
  return GameEngine;
})(GameEngine || {})
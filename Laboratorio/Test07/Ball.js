var GameEngine = (function(GameEngine) {
  let PI2 = 2*Math.PI;

  class Ball {
    constructor(x, y, size, color) {
      this.x = x;
      this.y = y;
      this.size = size;
      this.color = color;

      this.speed = 50 +Math.random()*300;
      this.angle = (360 * Math.random()) * Math.PI/180;
      this.vx = Math.cos(this.angle) * this.speed;
      this.vy = Math.sin(this.angle) * this.speed;
    }

    update(elapsed) {
      this.x += this.vx * elapsed;
      this.y += this.vy * elapsed;
    }

    render(ctx) {
      ctx.fillStyle = this.color;
      ctx.beginPath();
      ctx.arc(this.x, this.y, this.size, 0, PI2);
      ctx.fill();  
    }
  }

  GameEngine.Ball = Ball;
  return GameEngine;
})(GameEngine || {})
var GameEngine = (function(GameEngine) {
  let cw;
  let ch;

  class Game {
    constructor(ctx) {
      cw = ctx.canvas.width;
      ch = ctx.canvas.height;
      this.ctx = ctx;

      let num_balls = 50;
      let radius;
      let color;
      this.balls = [];
      for (let i=0; i<num_balls; i++) {
        radius = 10 + Math.random()*20;
        color = "rgba("+ (Math.random()*255) + "," + (Math.random()*255) + "," + (Math.random()*255) +",1)";
        this.balls.push( new GameEngine.Ball(radius+Math.random()*(cw-2*radius), radius+Math.random()*(ch-2*radius), radius, color) );
      }
    }

    processInput() {
    }

    update(elapsed) {
      for (let i=0; i<this.balls.length; i++) {
        this.balls[i].update(elapsed);

        this.checkBorder(this.balls[i]);
      }
    }

    checkBorder(ball) {
      if (ball.x < ball.size) {
        ball.vx *= -1;
        ball.x = ball.size;
      }
      if (ball.x > cw-ball.size) {
        ball.vx *= -1;
        ball.x = cw-ball.size;
      }
      if (ball.y < ball.size) {
        ball.vy *= -1;
        ball.y = ball.size;
      }
      if (ball.y > ch-ball.size) {
        ball.vy *= -1;
        ball.y = ch-ball.size;
      }
    }

    render() {
      this.ctx.clearRect(0, 0, cw, ch);

      for (let i=0; i<this.balls.length; i++) {
        this.balls[i].render(this.ctx);
      }
    }
  }

  GameEngine.Game = Game;
  return GameEngine;
})(GameEngine || {})
window.addEventListener("load", function() {
  let theCanvas = document.getElementById("theCanvas");
  let engine = new BABYLON.Engine(theCanvas, true);

  function createScene() {
    let scene = new BABYLON.Scene(engine);
    scene.clearColor = new BABYLON.Color3.White();

    let camera = new BABYLON.UniversalCamera("camera", new BABYLON.Vector3(0, 10, -10), scene);
    camera.setTarget(BABYLON.Vector3.Zero());
    camera.attachControl(theCanvas, true);

    // https://doc.babylonjs.com/api/classes/babylon.spotlight
    let light = new BABYLON.SpotLight("spotLight",
                                      new BABYLON.Vector3(0, 10, 0),
                                      new BABYLON.Vector3(0, -1, 0),
                                      BABYLON.Tools.ToRadians(45),
                                      0.1,
                                      scene);
    light.diffuse = new BABYLON.Color3(0, 1, 0);

    let box = BABYLON.Mesh.CreateBox("Box", 4, scene);

    return scene;
  }

  let scene = createScene();

  engine.runRenderLoop(function() {
    let light = scene.getLightByName("spotLight");
    light.position.y -= 0.01;

    scene.render();
  });

  window.addEventListener("resize", function() { 
    engine.resize();
  });  
});
window.addEventListener("load", function init() {
	let theCanvas = document.getElementById("theCanvas");
	let ctx = theCanvas.getContext("2d");

	ctx.fillStyle = "#ff0066";
	ctx.beginPath();
	ctx.arc(50, 50, 30, 0, 2*Math.PI);
	ctx.fill();
});
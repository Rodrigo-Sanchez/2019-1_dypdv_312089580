var GameEngine = (function(GameEngine) {
  let cw;
  let ch;

  let KEY = GameEngine.KEY;

  let aPool;
  let bPool;

  class Game {
    constructor(ctx) {
      cw = ctx.canvas.width;
      ch = ctx.canvas.height;
      this.ctx = ctx;

      this.ship = new GameEngine.Ship(cw/2, ch/2, 50);

      this.asteroidPool = new GameEngine.AsteroidPool(cw, ch);
      this.asteroidPool.addAsteroid();


      window.addEventListener("keydown", function(evt) {
        KEY.onKeyDown(evt.keyCode);
      });
      window.addEventListener("keyup", function(evt) {
        KEY.onKeyUp(evt.keyCode);
      });
    }

    processInput() {
      this.ship.processInput();
    }

    update(elapsed) {
      aPool = this.asteroidPool.asteroids;
      bPool = this.ship.weapon.bullets;

      this.ship.update(elapsed);
      this.asteroidPool.update(elapsed);

      this.checkBorders(this.ship);

      for (let i=0; i<aPool.length; i++) {
        if (aPool[i].isAlive) {
          this.checkBorders(aPool[i]);
        }
      }

      for (let i=0; i<bPool.length; i++) {
        if (bPool[i].isAlive) {
          this.checkBorders(bPool[i]);
        }
      }


      for (let i=0; i<aPool.length; i++) {
        this.checkCircleCollision(this.ship, aPool[i], "nave-asteroide");
      }

      for (let bullet_i=0; bullet_i<bPool.length; bullet_i++) {
        if (bPool[bullet_i].isAlive) {
          for (let asteroid_i=0; asteroid_i<aPool.length; asteroid_i++) {
            if ( (aPool[asteroid_i].isAlive) && (this.checkCircleCollision(bPool[bullet_i], aPool[asteroid_i], "bala-asteroide")) ) {
              bPool[bullet_i].isAlive = false;
              this.asteroidPool.split(asteroid_i);
            }
          }
        }
      }
    }

    checkCircleCollision(obj1, obj2, tmpmsg) {
      let dist = Math.sqrt( (obj1.x - obj2.x)*(obj1.x - obj2.x) + (obj1.y - obj2.y)*(obj1.y - obj2.y) );
      if (dist < obj1.radius + obj2.radius) {
        console.log("colision", tmpmsg);
        return true;
      }
      return false;
    }

    checkBorders(gameObject) {
      if (gameObject.x < -gameObject.radius) {
        gameObject.x = cw + gameObject.radius;
      }
      if (gameObject.x > cw+gameObject.radius) {
        gameObject.x = -gameObject.radius;
      }
      if (gameObject.y < -gameObject.radius) {
        gameObject.y = ch + gameObject.radius;
      }
      if (gameObject.y > ch+gameObject.radius) {
        gameObject.y = -gameObject.radius;
      }
    }

    render() {
      this.ctx.fillStyle = "rgba(0,0,0,1)";
      this.ctx.fillRect(0, 0, cw, ch);
      this.ship.render(this.ctx);
      this.asteroidPool.render(this.ctx);
    }
  }

  GameEngine.Game = Game;
  return GameEngine;
})(GameEngine || {})
var GameEngine = (function(GameEngine) {
  let cw;
  let ch;

  let KEY = GameEngine.KEY;

  class Game {
    constructor(ctx) {
      cw = ctx.canvas.width;
      ch = ctx.canvas.height;
      this.ctx = ctx;

      this.player = new GameEngine.Player(cw/2, ch-25, 25, 50);
      this.platforms = [];
      this.platforms.push(new GameEngine.Platform(500, 400, 100, 25));
      this.platforms.push(new GameEngine.Platform(320, 320, 100, 25));
      this.platforms.push(new GameEngine.Platform(140, 240, 100, 25));
      this.platforms.push(new GameEngine.Platform(320, 180, 100, 25));
      this.platforms.push(new GameEngine.Platform(500, 100, 100, 25));

      window.addEventListener("keydown", function(evt) {
        KEY.onKeyDown(evt.keyCode);
      });
      window.addEventListener("keyup", function(evt) {
        KEY.onKeyUp(evt.keyCode);
      });
    }

    processInput() {
      this.player.processInput();
    }

    update(elapsed) {
      this.player.update(elapsed);

      this.checkCollisionPlatforms();

      this.checkCollisionWalls();
    }

    checkCollisionWalls() {
      if (this.player.x < this.player.w_2) {
        this.player.x = this.player.w_2;
      }
      if (this.player.x > cw-this.player.w_2) {
        this.player.x = cw-this.player.w_2;
      }
      if (this.player.y < this.player.h_2) {
        this.player.y = this.player.h_2;
      }
      if (this.player.y > ch-this.player.h_2) {
        this.player.y = ch-this.player.h_2;
        this.player.inFloor = true;
      }
    }

    checkCollisionPlatforms() {
      let player = this.player;
      let platform;
      for (let i=0, l=this.platforms.length; i<l; i++) {
        platform = this.platforms[i];
        if (
          (player.x - player.w_2 < platform.x + platform.w_2) &&
          (player.x + player.w_2 > platform.x - platform.w_2) && 
          (player.y - player.h_2 < platform.y + platform.h_2) &&
          (player.y + player.h_2 > platform.y - platform.h_2)
        ) {
          console.log("Colision con el rectangulo ", i)
        }
      }
    }

    render() {
      this.ctx.clearRect(0, 0, cw, ch);

      this.player.render(this.ctx);

      for (let i=0, l=this.platforms.length; i<l; i++) {
        this.platforms[i].render(this.ctx);
      }
    }
  }

  GameEngine.Game = Game;
  return GameEngine;
})(GameEngine || {})
var GameEngine = (function(GameEngine) {

  class AsteroidPool {
    constructor(scene, id, cw, ch) {
      this.game_w = cw;
      this.game_h = ch;

      this.initialSize = 30;

      this.numAsteroids = 10;
      this.asteroids = [];

      for (let i=0; i<this.numAsteroids; i++) {
        this.asteroids.push(new GameEngine.Asteroid(scene, id, cw, ch, 0, 0, this.initialSize));
      }
    }

    addAsteroid() {
      let x, y;

      switch (parseInt(Math.random() * 2)) {
        case 0:
          x = this.game_w/2 + this.initialSize;
          y = -this.game_h/2 + Math.random()*this.game_h;
          break;
        case 1:
          x = -this.game_w/2 + Math.random()*this.game_w;
          y = this.game_h/2 + this.initialSize;
          break;
      }

      for (let i=0; i<this.numAsteroids; i++) {
        if (!this.asteroids[i].isAlive) {
          this.asteroids[i].activate(x, y);
          this.asteroids[i].size = this.initialSize;
          return this.asteroids[i];
        }
      }
    }

    split(old_asteroid) {
      old_asteroid = this.asteroids[old_asteroid];
      let new_asteroid = this.addAsteroid();

      if (new_asteroid) {
        new_asteroid.x = old_asteroid.x;
        new_asteroid.y = old_asteroid.y;
        new_asteroid.vx = -old_asteroid.vx;
        new_asteroid.vy = -old_asteroid.vy;
        new_asteroid.size = new_asteroid.radius = old_asteroid.size;
        new_asteroid.hp = old_asteroid.hp;
        new_asteroid.hit();
      }

      old_asteroid.hit();
    }

    update(elapsed) {
      for (let i=0; i<this.numAsteroids; i++) {
        if (this.asteroids[i].isAlive) {
          this.asteroids[i].update(elapsed);
        }
      }
    }
  
  }

  GameEngine.AsteroidPool = AsteroidPool;
  return GameEngine;
})(GameEngine || {})

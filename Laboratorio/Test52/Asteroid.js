var GameEngine = (function(GameEngine) {

  class Asteroid {
    constructor(scene, id, cw, ch, x, y, size) {
      this.isAlive = false;
      this.x = x;
      this.y = y;
      this.cw = cw;
      this.ch = ch;
      this.speed = 100;
      
      let angle = (360 * Math.random()) * Math.PI/180;
      this.vx = Math.cos(angle) * this.speed;
      this.vy = Math.sin(angle) * this.speed;
      this.size = this.radius = size;
      this.hp = 4;

      this.mesh = BABYLON.Mesh.CreateIcoSphere("bulletMesh", {subdivisions:1}, scene);
      this.mesh.material = new BABYLON.StandardMaterial("bulletMesh_material", scene);
      this.mesh.material.diffuseColor = new BABYLON.Color3(1, 0, 1);

      this.mesh.scaling.x = this.mesh.scaling.y = this.mesh.scaling.z = size;

      this.mesh.isVisible = this.isAlive;
    }

    hit() {
      this.hp--;

      if (this.hp > 0) {
        this.radius = this.size = this.size*0.75;
        this.mesh.scaling.x = this.mesh.scaling.y = this.mesh.scaling.z = this.size;
      }
      else {
        this.isAlive = false;
      }

      this.mesh.isVisible = this.isAlive;
    }

    activate(x, y) {
      this.x = x;
      this.y = y;
      this.isAlive = true;

      this.mesh.scaling.x = this.mesh.scaling.y = this.mesh.scaling.z = this.size;
      this.mesh.position.x = this.x;
      this.mesh.position.y = this.y;

      this.mesh.isVisible = this.isAlive;
    }

    update(elapsed) {
      this.x += this.vx *elapsed;
      this.y += this.vy *elapsed;

      this.checkBorders();

      this.mesh.position.x = this.x;
      this.mesh.position.y = this.y;
    }

    checkBorders() {
      if (this.x -this.size/2 > this.cw/2) {
        this.x = -this.cw/2 -this.size/2;
      }
      if (this.x +this.size/2 < -this.cw/2) {
        this.x = this.cw/2 +this.size/2;
      }
      if (this.y -this.size/2 > this.ch/2) {
        this.y = -this.ch/2 -this.size/2;
      }
      if (this.y +this.size/2 < -this.ch/2) {
        this.y = this.ch/2 +this.size/2;
      }
    }

  }

  GameEngine.Asteroid = Asteroid;
  return GameEngine;
})(GameEngine || {})
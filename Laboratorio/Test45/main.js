window.addEventListener("load", function() {
  let theCanvas = document.getElementById("theCanvas");
  let engine = new BABYLON.Engine(theCanvas, true);

  function createScene() {
    let scene = new BABYLON.Scene(engine);
    scene.clearColor = new BABYLON.Color3(1, 1, 1);
    
    let camera = new BABYLON.UniversalCamera("camera", new BABYLON.Vector3(5, 10, -10), scene);
    camera.setTarget(BABYLON.Vector3.Zero());
    camera.attachControl(theCanvas, true);

    let box = BABYLON.Mesh.CreateBox("Box", 4, scene);

    let material = new BABYLON.StandardMaterial("material", scene);
    material.diffuseColor = new BABYLON.Color3(0, 0, 1);
    material.emissiveColor = new BABYLON.Color3(1, 0, 0);
    material.specularColor = new BABYLON.Color3(1, 1, 1);
    // material.ambientColor = new BABYLON.Color3(0.25, 0.25, 0.25);
    material.specularPower = 3;
    material.alpha = 1;
    box.material = material;

    let light = new BABYLON.SpotLight("spotLight",
                                      new BABYLON.Vector3(0, 10, 0),
                                      new BABYLON.Vector3(0, -1, 0),
                                      BABYLON.Tools.ToRadians(45),
                                      0.1,
                                      scene);
    light.diffuse = new BABYLON.Color3(1, 1, 1);

    // scene.ambientColor = new BABYLON.Color3(0.5, 0.5, 0.5);

    return scene;
  }

  let scene = createScene();
  
  engine.runRenderLoop(function() {
    scene.render();
  });
  
  window.addEventListener("resize", function() { 
    engine.resize();
  }); 
});
window.addEventListener("load", function() {
  console.log("init");
  let theCanvas = document.getElementById("theCanvas");
  let ctx = theCanvas.getContext("2d");

  let lastTime = Date.now();
  let current = 0;
  let elapsed = 0;

  let game = new GameEngine.Game(ctx);

  (function gameLoop() {
    current = Date.now();
    elapsed = (current - lastTime) / 1000;

    game.processInput();
    game.update(elapsed);
    game.render();

    lastTime = current;

    window.requestAnimationFrame(gameLoop);
  })();



  // reescalado del canvas
  let canvas_w = theCanvas.width;
  let canvas_h = theCanvas.height;
  let scale;

  window.addEventListener("resize", resize);
  function resize() {
    scale = Math.min( window.innerWidth/canvas_w, window.innerHeight/canvas_h );
    theCanvas.style.transform = "translate(" + ((window.innerWidth - scale*canvas_w)/2) + "px," + ((window.innerHeight - scale*canvas_h)/2) + "px) scale(" + scale + ")";
  }

  resize();
});
var GameEngine = (function(GameEngine) {
  let PI2 = 2*Math.PI;
  let gravity = 10;

  class Ball {
    constructor(x, y, size) {
      this.x = x;
      this.y = y;
      this.size = size;
      this.friction = 0.2;
      this.bounce = -0.75;

      this.speed = 200;
      this.angle = (360 * Math.random()) * Math.PI/180;
      this.vx = Math.cos(this.angle) * this.speed;
      this.vy = Math.sin(this.angle) * this.speed;
    }

    update(elapsed) {
      this.speed = Math.sqrt(this.vx*this.vx + this.vy*this.vy);
      this.angle = Math.atan2(this.vy, this.vx);
      
      this.speed = (this.speed > this.friction) ? this.speed - this.friction : 0;

      this.vx = Math.cos(this.angle) * this.speed;
      this.vy = Math.sin(this.angle) * this.speed;
      
      this.vy += gravity;
      this.x += this.vx * elapsed;
      this.y += this.vy * elapsed;
    }

    render(ctx) {
      ctx.fillStyle = "#ff0000";
      ctx.beginPath();
      ctx.arc(this.x, this.y, this.size, 0, PI2);
      ctx.fill();  
    }
  }

  GameEngine.Ball = Ball;
  return GameEngine;
})(GameEngine || {})
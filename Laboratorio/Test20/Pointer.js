var GameEngine = (function(GameEngine) {

  let rect;
  let x_aux;
  let y_aux;

  Pointer = {
    x: 0,
    y: 0,
    
    DOWN: false,
    UP: false,
    MOVE: false,

    pointerDown: function(evt, cw, ch) {
      this.DOWN = true;
      this.UP = false;
      this.MOVE = false;

      this._getPos(evt, cw, ch);
    },
    pointerUp: function(evt, cw, ch) {
      this.DOWN = false;
      this.UP = true;
      this.MOVE = false;

      this._getPos(evt, cw, ch);
    },
    pointerMove: function(evt, cw, ch) {
      this.MOVE = true;

      this._getPos(evt, cw, ch);
    },
    _getPos: function(evt, cw, ch) {
      evt.preventDefault();
      evt.stopPropagation();

      if (evt.touches) {
        if (evt.touches.length > 0) {
          x_aux = evt.touches[0].clientX;
          y_aux = evt.touches[0].clientY;
        }
      }
      else {
        x_aux = evt.clientX;
        y_aux = evt.clientY;
      }

      rect = evt.target.getBoundingClientRect();
      this.x = cw * (x_aux - evt.target.offsetLeft - rect.left) / rect.width;
      this.y = ch * (y_aux - evt.target.offsetTop  - rect.top ) / rect.height;
    },
  };

  GameEngine.Pointer = Pointer;
  return GameEngine;
})(GameEngine || {})
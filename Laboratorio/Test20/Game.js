var GameEngine = (function(GameEngine) {
  let cw;
  let ch;
  let dx;
  let dy;
  let old_x;
  let old_y;
  let elapsed_counter = 0;

  let pointer = GameEngine.Pointer;

  class Game {
    constructor(ctx) {
      cw = ctx.canvas.width;
      ch = ctx.canvas.height;
      this.ctx = ctx;

      let ball_size = 25;
      this.ball = new GameEngine.Ball(ball_size+Math.random()*(cw-2*ball_size), ch/2, ball_size);

      ctx.canvas.addEventListener("mousedown", function(evt) {
        pointer.pointerDown(evt, cw, ch);
      });
      ctx.canvas.addEventListener("mouseup", function(evt) {
        pointer.pointerUp(evt, cw, ch);
      });
      ctx.canvas.addEventListener("mousemove", function(evt) {
        pointer.pointerMove(evt, cw, ch);
      });

      ctx.canvas.addEventListener("touchstart", function(evt) {
        pointer.pointerDown(evt, cw, ch);
      });
      ctx.canvas.addEventListener("touchend", function(evt) {
        pointer.pointerUp(evt, cw, ch);
      });
      ctx.canvas.addEventListener("touchmove", function(evt) {
        pointer.pointerMove(evt, cw, ch);
      });
    }

    processInput() {
      this.tryToGrab = false;

      if (pointer.DOWN && !this.grabing) {
        this.tryToGrab = true;
      }
      
      if (pointer.UP && this.grabing) {
        this.release = true;
      }
    }

    update(elapsed) {
      elapsed_counter += elapsed;

      this.ball.update(elapsed);

      if (this.tryToGrab) {
        if (Math.sqrt((pointer.x-this.ball.x)*(pointer.x-this.ball.x) + (pointer.y-this.ball.y)*(pointer.y-this.ball.y)) <= this.ball.size) {
          this.grabing = true;

          dx = pointer.x - this.ball.x;
          dy = pointer.y - this.ball.y;
          old_x = this.ball.x;
          old_y = this.ball.y;
        }
      }

      if (this.grabing) {
        this.ball.x = pointer.x - dx;
        this.ball.y = pointer.y - dy;
      }

      if (this.release) {
        this.release = false;
        this.grabing = false;
        this.ball.vx = (this.ball.x - old_x)*5;
        this.ball.vy = (this.ball.y - old_y)*5;
      }

      if (elapsed_counter > 0.5) {
        old_x = this.ball.x;
        old_y = this.ball.y;
        elapsed_counter = 0;
      }

      if (this.ball.x < this.ball.size) {
        this.ball.vx *= this.ball.bounce;
        this.ball.x = this.ball.size;
      }
      if (this.ball.x > cw-this.ball.size) {
        this.ball.vx *= this.ball.bounce;
        this.ball.x = cw-this.ball.size;
      }
      if (this.ball.y < this.ball.size) {
        this.ball.vy *= this.ball.bounce;
        this.ball.y = this.ball.size;
      }
      if (this.ball.y > ch-this.ball.size) {
        this.ball.vy *= this.ball.bounce;
        this.ball.y = ch-this.ball.size;
      }
    }

    render() {
      this.ctx.clearRect(0, 0, cw, ch);
      this.ball.render(this.ctx);
    }
  }

  GameEngine.Game = Game;
  return GameEngine;
})(GameEngine || {})
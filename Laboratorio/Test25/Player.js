var GameEngine = (function(GameEngine) {
  let gravity = 16;

  let KEY = GameEngine.KEY;

  class Player extends GameEngine.Sprite {
    constructor(x, y, w, h) {
      super(x, y, w, h, "player.svg", 5, 16, 16);

      this.frameCounter = 0;
      this.framesPerChange = 4;

      this.w_2 = w/2;
      this.h_2 = h/2;

      this.speed = 120;
      this.jump_heigth = 380;
      this.inFloor = false;

      this.vx = 0;
      this.vy = 0;
    }

    processInput() {
      this.vx = 0;

      if ((KEY.isPress(KEY.SPACE)) && (this.inFloor)) {
        this.vy = -this.jump_heigth;
      }
      if (KEY.isPress(KEY.LEFT)) {
        this.vx = -this.speed;
        this.direction = -1;
      }
      if (KEY.isPress(KEY.RIGHT)) {
        this.vx = this.speed;
        this.direction = 1;
      }
    }

    update(elapsed) {
      this.inFloor = false;
      this.vy += gravity;

      super.update(elapsed);
    }
    
    render(ctx) {
      if (this.vx !== 0) {
        this.frameCounter = (this.frameCounter +1)%(this.framesPerChange*3);
        this.currentFrame = 1 + parseInt(this.frameCounter/this.framesPerChange);
      }
      else {
        if (this.inFloor) {
          this.currentFrame = 0;
        }
      }

      if (!this.inFloor) {
        this.currentFrame = 4;
      }

      super.render(ctx)
    }
  }

  GameEngine.Player = Player;
  return GameEngine;
})(GameEngine || {})
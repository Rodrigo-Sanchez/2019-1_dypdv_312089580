window.addEventListener("load", function() {
  let theCanvas = document.getElementById("theCanvas");
  let engine = new BABYLON.Engine(theCanvas, true);

  function createScene() {
    let scene = new BABYLON.Scene(engine);
    scene.clearColor = new BABYLON.Color3(1, 1, 1);
    
    // https://doc.babylonjs.com/api/classes/babylon.universalcamera
    let camera = new BABYLON.UniversalCamera("camera", new BABYLON.Vector3(0, 10, -10), scene);
    camera.setTarget(BABYLON.Vector3.Zero());

    camera.attachControl(theCanvas, true);
    camera.keysUp.push(87);    //W
    camera.keysDown.push(83)   //D
    camera.keysLeft.push(65);  //A
    camera.keysRight.push(68); //S

    let box = BABYLON.Mesh.CreateBox("Box", 4, scene);
    let material = new BABYLON.StandardMaterial("material", scene);
    material.diffuseColor = new BABYLON.Color3(1, 0, 1);
    box.material = material;

    let light = new BABYLON.SpotLight("spotLight",
                                      new BABYLON.Vector3(0, 10, 0),
                                      new BABYLON.Vector3(0, -1, 0),
                                      BABYLON.Tools.ToRadians(45),
                                      0.1,
                                      scene);
    light.diffuse = new BABYLON.Color3(1, 1, 1);

    return scene;
  }

  let scene = createScene();
  
  engine.runRenderLoop(function() {
    scene.render();
  });
  
  window.addEventListener("resize", function() { 
    engine.resize();
  });
});
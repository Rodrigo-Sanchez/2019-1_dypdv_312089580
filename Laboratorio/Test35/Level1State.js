var GameEngine = (function(GameEngine) {
  let cw;
  let ch;

  class Level1State {
    constructor(game, w, h) {
      this.game = game;
      cw = w;
      ch = h;

      this.camera = new GameEngine.Camera(cw/2, ch/2-8, cw, ch);
      this.player = new GameEngine.Player(88, ch-125, 16, 16);
      this.level = new GameEngine.Level();

      // this.background_audio = new Audio("audios/level_1.mp3");
      this.background_audio = new Audio("audios/level_1.mp3");
      this.background_audio.loop = true;
      this.background_audio.volume = 0.5;
      this.background_audio.addEventListener("canplay", function() {
        this.play();
      });
    }

    processInput() {
      this.player.processInput();
    }

    update(elapsed) {
      // update player
      this.player.update(elapsed, this.level, cw, ch, this.camera);

      let dx = this.camera.x - this.player.x;
      if (dx < 0) {
        this.camera.x = Math.min(this.level.w -cw/2, this.lerp(this.camera.x, this.player.x, 0.25));
      }

      // update enemies
      for (let i=0, l=this.level.enemies.length; i<l; i++) {
        if (Math.abs(this.camera.x - this.level.enemies[i].x) < cw/2) {
          this.level.enemies[i].active = true;
        }
        this.level.enemies[i].update(elapsed, this.level, cw, ch, this.camera, this.player);
      }
    }

    lerp(v0, v1, t) {
      return v0 + (v1 - v0)*t;
    }

    render(ctx) {
      ctx.fillStyle = "#6b8cff";
      ctx.fillRect(0, 0, cw, ch);

      this.camera.applyTransform(ctx);

      this.level.render(ctx);
      this.player.render(ctx);

      for(let i=0, l=this.level.enemies.length; i<l; i++) {
        this.level.enemies[i].render(ctx);
      }

      this.camera.releaseTransform(ctx);
    }
  }

  GameEngine.Level1State = Level1State;
  return GameEngine;
})(GameEngine || {})
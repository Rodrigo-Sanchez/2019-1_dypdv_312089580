var GameEngine = (function(GameEngine) {
  let gravity = 16;

  let KEY = GameEngine.KEY;

  class Player extends GameEngine.Sprite {
    constructor(x, y, w, h) {
      super(x, y, w, h, "images/player.svg", 5, 16, 16);

      this.frameCounter = 0;
      this.framesPerChange = 4;

      this.w_2 = w/2;
      this.h_2 = h/2;

      this.speed = 120;
      this.jump_heigth = 366;
      this.inFloor = false;

      this.vx = 0;
      this.vy = 0;
      this.canJump = true;

      // this.jump_audio = new Audio("audios/jump.mp3");
      this.jump_audio = new Audio("https://joellongi.bitbucket.io/2019-1/recursos/audios/jump.mp3");
    }

    processInput() {
      this.vx = 0;
      this.speed = 120;

      if (KEY.isReleased(KEY.SPACE)) {
        this.canJump = true;
      }
      if ((KEY.isPress(KEY.SPACE)) && (this.canJump) && (this.inFloor)) {
        this.jump_audio.pause();
        this.jump_audio.currentTime = 0;
        this.jump_audio.play();

        this.vy = -this.jump_heigth;
        this.canJump = false;
      }

      if (KEY.isPress(KEY.Z_KEY)) {
        this.speed = 240;
      }

      if (KEY.isPress(KEY.LEFT)) {
        this.vx = -this.speed;
        this.direction = -1;
      }
      if (KEY.isPress(KEY.RIGHT)) {
        this.vx = this.speed;
        this.direction = 1;
      }

    }

    update(elapsed, level, cw, ch, camera) {
      this.inFloor = false;
      this.vy += gravity;

      super.update(elapsed);

      this.checkCollisionPlatforms(level);
      this.setState();

      this.checkCollisionWalls(cw, ch, camera);
    }

    checkCollisionPlatforms(level) {
      let tile_pos = level.getTilePos(this.x, this.y);

      //center
      this.reactCollision(level.getPlatform(tile_pos.x,   tile_pos.y));
      // left
      this.reactCollision(level.getPlatform(tile_pos.x-1, tile_pos.y));
      // right
      this.reactCollision(level.getPlatform(tile_pos.x+1, tile_pos.y));

      // top
      this.reactCollision(level.getPlatform(tile_pos.x,   tile_pos.y-1));
      // bottom
      this.reactCollision(level.getPlatform(tile_pos.x,   tile_pos.y+1));

      // left top
      this.reactCollision(level.getPlatform(tile_pos.x-1, tile_pos.y-1));
      // right top
      this.reactCollision(level.getPlatform(tile_pos.x+1, tile_pos.y-1));

      // left bottom
      this.reactCollision(level.getPlatform(tile_pos.x-1, tile_pos.y+1));
      // right bottom
      this.reactCollision(level.getPlatform(tile_pos.x+1, tile_pos.y+1));
    }

    reactCollision(platform) {
      if ( platform &&
           Math.abs(this.x - platform.x) < this.w_2 + platform.w_2 && 
           Math.abs(this.y - platform.y) < this.h_2 + platform.h_2 ) {

        let overlapX = (this.w_2 + platform.w_2) - Math.abs(this.x - platform.x);
        let overlapY = (this.h_2 + platform.h_2) - Math.abs(this.y - platform.y);

        if (overlapX < overlapY) {
          if (this.x - platform.x > 0) {
            this.x += overlapX;
          }
          else {
            this.x -= overlapX;
          }
        }
        else if (overlapX > overlapY) {
          if (this.y - platform.y > 0) {
            this.y += overlapY;
            if (this.vy < 0) {
              this.vy = 0;
            }
          }
          else {
            this.y -= overlapY;
            if (this.vy > 0) {
              this.inFloor = true;
              this.vy = 0;
            }
          }
        }
      }
    }

    setState() {
      if (this.vx !== 0) {
        this.state = "walking";
      }
      else if (this.inFloor) {
        this.state = "still";
      }
      if (!this.inFloor) {
        this.state = "jumping";
      }
    }
    
    checkCollisionWalls(cw, ch, camera) {
      if (this.x < camera.x -cw/2 +this.w_2) {
        this.x = camera.x -cw/2 +this.w_2;
      }
      if (this.x > camera.x +cw/2 -this.w_2) {
        this.x = camera.x +cw/2 -this.w_2;
      }

      if (this.y > ch-this.h_2) {
        this.y = 0;
        this.vy = 0;
      }
    }
    
    render(ctx) {
      if (this.state === "walking") {
        this.frameCounter = (this.frameCounter +1)%(this.framesPerChange*3);
        this.currentFrame = 1 + parseInt(this.frameCounter/this.framesPerChange);
      }
      else if (this.state === "still") {
        this.currentFrame = 0;
      }
      else if (this.state === "jumping") {
        this.currentFrame = 4;
      }

      super.render(ctx)
    }
  }

  GameEngine.Player = Player;
  return GameEngine;
})(GameEngine || {})
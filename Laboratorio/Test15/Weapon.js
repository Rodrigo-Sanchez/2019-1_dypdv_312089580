var GameEngine = (function(GameEngine) {
  class Weapon {
    constructor(numBullets) {
      this.bullets = [];
      for (let i=0; i<numBullets; i++) {
        this.bullets.push(new GameEngine.Bullet());
      }
    }

    shot(x, y, direction) {
      for (let i=0, l=this.bullets.length; i<l; i++) {
        
        if (!this.bullets[i].isAlive) {
          this.bullets[i].activate(x, y, direction);
          return;
        }
      }
    }

    update(elapsed) {
      for (let i=0, l=this.bullets.length; i<l; i++) {
        if (this.bullets[i].isAlive) {
          this.bullets[i].update(elapsed);
        }
      }
    }

    render(ctx) {
      for (let i=0, l=this.bullets.length; i<l; i++) {
        if (this.bullets[i].isAlive) {
          this.bullets[i].render(ctx);
        }
      }
    }
  }

  GameEngine.Weapon = Weapon;
  return GameEngine;
})(GameEngine || {})
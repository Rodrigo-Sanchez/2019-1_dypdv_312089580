var GameEngine = (function(GameEngine) {
  const PI2 = 2*Math.PI;

  class Bullet {
    constructor(speed=300) {
      this.isAlive = false;
      this.speed = speed;
      this.x = 0;
      this.y = 0;
      this.size = 2;
    }

    activate(x, y, direction) {
      this.x = x;
      this.y = y;
      this.vx = this.speed * Math.cos(direction);
      this.vy = this.speed * Math.sin(direction);
      this.isAlive = true;
    }

    update(elapse) {
      this.x += this.vx * elapse;
      this.y += this.vy * elapse;
    }

    render(ctx) {
      ctx.fillStyle = "#ffffff";
      ctx.beginPath();
      ctx.arc(this.x, this.y, this.size, 0, PI2);
      ctx.fill();  
    }
  }

  GameEngine.Bullet = Bullet;
  return GameEngine;
})(GameEngine || {})
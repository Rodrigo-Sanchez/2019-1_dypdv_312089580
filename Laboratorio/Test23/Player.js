var GameEngine = (function(GameEngine) {
  let gravity = 20;

  let KEY = GameEngine.KEY;

  class Player {
    constructor(x, y, w, h) {
      this.x = x;
      this.y = y;
      this.w = w;
      this.h = h;
			this.w_2 = w/2;
      this.h_2 = h/2;

      this.speed = 200;
      this.jump_heigth = 600;
      this.inFloor = false;

      this.vx = 0;
      this.vy = 0;
    }

    processInput() {
      this.vx = 0;

      if ((KEY.isPress(KEY.SPACE)) && (this.inFloor)) {
        this.vy = -this.jump_heigth;
        this.inFloor = false;
      }
      if (KEY.isPress(KEY.LEFT)) {
        this.vx = -this.speed;
      }
      if (KEY.isPress(KEY.RIGHT)) {
        this.vx = this.speed;
      }
    }

    update(elapsed) {
      this.inFloor = false;

      this.vy += gravity;
      this.x += this.vx * elapsed;
      this.y += this.vy * elapsed;
    }
    
    render(ctx) {
      ctx.fillStyle = "#222222";
      ctx.beginPath();
			ctx.fillRect(this.x - this.w_2, this.y - this.h_2, this.w, this.h);
    }
  }

  GameEngine.Player = Player;
  return GameEngine;
})(GameEngine || {})
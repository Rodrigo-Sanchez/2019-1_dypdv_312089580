var GameEngine = (function(GameEngine) {
  const PI_180 = Math.PI/180;

  class Character {
    constructor(scene) {
      this.scene = scene;
      this.mesh = scene.getMeshByName("Monito");
      this.mesh.checkCollisions = true;
      this.mesh.ellipsoid = new BABYLON.Vector3(35, 90, 20);
      this.mesh.ellipsoidOffset = new BABYLON.Vector3(0, 90, 0);

      // let debug_ellipsoid = BABYLON.MeshBuilder.CreateSphere("tmp_sphere", {diameterX: 70, diameterY: 180, diameterZ: 40}, scene);
      // debug_ellipsoid.position.y = 90;
      // debug_ellipsoid.parent = this.mesh;
      // debug_ellipsoid.visibility = 0.25;

      this.cameraTarget = BABYLON.MeshBuilder.CreateBox("CameraTarget", {width: 1, height: 1, depth: 1}, scene);
      this.cameraTarget.visibility = 0;
      this.cameraTarget.parent = this.mesh;
      this.cameraTarget.position.y = 150;

      this.skeleton = scene.getSkeletonByName("Armature");
      this.skeleton.needInitialSkinMatrix = false;

      this.idle_range = this.skeleton.getAnimationRange("Idle");
      this.walk_range = this.skeleton.getAnimationRange("Walk");
      
      this.state = "Idle";

      scene.beginAnimation(this.skeleton, this.idle_range.from, this.idle_range.to, true);

      this.velocity = new BABYLON.Vector3();
      this.vr = 0;
      this.rotation = -Math.PI/2;
      this.speed = 0;
    }

    processInput(keys) {
      this.vr = 0;

      if (keys["ArrowUp"]) {
        if (this.state === "Idle") {
          this.scene.stopAnimation(this.skeleton);
          this.scene.beginAnimation(this.skeleton, this.walk_range.from, this.walk_range.to, true);
          this.state = "Walk";
        }
        this.speed = 2;
      }
      else {
        if (this.state === "Walk") {
          this.scene.stopAnimation(this.skeleton);
          this.scene.beginAnimation(this.skeleton, this.idle_range.from, this.idle_range.to, true);
          this.state = "Idle";
        }
        this.speed = 0;
        this.velocity.x = 0;
        this.velocity.y = 0;
        this.velocity.z = 0;
      }

      if (keys["ArrowLeft"]) {
        this.vr = -2;
        this.velocity.x = 0;
        this.velocity.y = 0;
        this.velocity.z = 0;
      }
      if (keys["ArrowRight"]) {
        this.vr = 2;
        this.velocity.x = 0;
        this.velocity.y = 0;
        this.velocity.z = 0;
      }
    }

    update(elapsed) {
      this.rotation += this.vr * PI_180;
      this.velocity.x += -Math.cos(this.rotation) * this.speed * elapsed;
      this.velocity.y += this.scene.gravity.y;
      this.velocity.z +=  Math.sin(this.rotation) * this.speed * elapsed;

      this.mesh.rotation.y = this.cameraTarget.rotation.y = this.rotation +Math.PI/2;

      this.mesh.moveWithCollisions(this.velocity);
    }

  }

  GameEngine.Character = Character;
  return GameEngine;
})(GameEngine || {})
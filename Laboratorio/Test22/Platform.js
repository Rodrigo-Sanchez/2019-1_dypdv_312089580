var GameEngine = (function(GameEngine) {

  class Platform {
    constructor(x, y, w, h) {
      this.x = x;
      this.y = y;
      this.w = w;
      this.h = h;
      this.w_2 = w/2;
      this.h_2 = h/2;
    }

    update(elapse) {
    }

    render(ctx) {
      ctx.fillStyle = "#777777";
      ctx.strokeStyle = "#333333";
      ctx.lineWidth = 2;
      ctx.beginPath();
      ctx.rect(this.x - this.w_2, this.y - this.h_2, this.w, this.h);
      ctx.fill();
      ctx.stroke();
    }
  }

  GameEngine.Platform = Platform;
  return GameEngine;
})(GameEngine || {})
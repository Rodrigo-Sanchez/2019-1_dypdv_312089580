window.addEventListener("load", function() {
  let theCanvas = document.getElementById("theCanvas");
  let engine = new BABYLON.Engine(theCanvas, true);

  function createScene() {
    let scene = new BABYLON.Scene(engine);
    scene.clearColor = new BABYLON.Color3.White();

    let camera = new BABYLON.UniversalCamera("camera", new BABYLON.Vector3(0, 10, -10), scene);
    camera.setTarget(BABYLON.Vector3.Zero());
    camera.attachControl(theCanvas, true);

    // https://doc.babylonjs.com/api/classes/babylon.pointlight
    let light = new BABYLON.PointLight("pointLight",
                                       new BABYLON.Vector3(0, 10, 0),
                                       scene);
    light.diffuse = new BABYLON.Color3(1, 0, 0);

    let box = BABYLON.Mesh.CreateBox("Box", 4, scene);

    return scene;
  }

  let scene = createScene();

  engine.runRenderLoop(function() {
    scene.render();
  });

  window.addEventListener("resize", function() { 
    engine.resize();
  });  
});
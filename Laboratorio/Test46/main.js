window.addEventListener("load", function() {
  let theCanvas = document.getElementById("theCanvas");
  let engine = new BABYLON.Engine(theCanvas, true);

  function createScene() {
    let scene = new BABYLON.Scene(engine);
    scene.clearColor = new BABYLON.Color3(1, 1, 1);
    
    let camera = new BABYLON.UniversalCamera("camera", new BABYLON.Vector3(5, 10, -10), scene);
    camera.setTarget(BABYLON.Vector3.Zero());
    camera.attachControl(theCanvas, true);

    let box = BABYLON.Mesh.CreateBox("Box", 4, scene);

    let material = new BABYLON.StandardMaterial("material", scene);
    material.diffuseTexture = new BABYLON.Texture("grass.svg", scene);
    // material.specularTexture = new BABYLON.Texture("grass.svg", scene);
    // material.emissiveTexture = new BABYLON.Texture("grass.svg", scene);
    // material.ambientTexture = new BABYLON.Texture("grass.svg", scene);
    material.bumpTexture = new BABYLON.Texture("grass_normal_map.svg", scene);
    box.material = material;

    let light = new BABYLON.SpotLight("spotLight",
                                      new BABYLON.Vector3(0, 10, 0),
                                      new BABYLON.Vector3(0, -1, 0),
                                      BABYLON.Tools.ToRadians(45),
                                      0.1,
                                      scene);
    light.diffuse = new BABYLON.Color3(1, 1, 1);

    return scene;
  }

  let scene = createScene();
  
  engine.runRenderLoop(function() {
    scene.render();
  });
  
  window.addEventListener("resize", function() { 
    engine.resize();
  }); 
});
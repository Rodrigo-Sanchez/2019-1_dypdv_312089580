var GameEngine = (function(GameEngine) {
  let map_w = 20;
  let map_h = 15;
  let tile_w = 16;
  let tile_h = 16;
  let mapData = [
    1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,
    1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,8,1,
    1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,8,1,
    1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,8,1,
    1,0,0,0,0,1,2,1,0,0,0,1,2,1,0,0,0,1,1,1,
    4,5,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,
    6,7,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,
    6,7,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,
    6,7,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,
    1,1,1,2,1,0,0,0,1,2,1,0,0,0,1,2,1,0,0,1,
    1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,4,5,
    1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,6,7,
    1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,6,7,
    1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,6,7,
    1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
  ];

  class Level {
    constructor() {
      this.platforms = [];

      this.tileAtlas = new GameEngine.TileAtlas(16, 16, "tiles.svg", 8, 16, 16);

      let platform;

      for (let i=0, l=map_w*map_h; i<l; i++) {
        if (mapData[i] > 0) {
          platform = new GameEngine.Platform( (i%map_w)*tile_w + tile_w/2, 
                                              parseInt(i/map_w)*tile_h + tile_h/2, 
                                              tile_w, 
                                              tile_h );
          platform.tileAtlas = this.tileAtlas;
          platform.currentFrame = mapData[i]-1;

          this.platforms.push(platform);
        }
      }
    }
  }

  GameEngine.Level = Level;
  return GameEngine;
})(GameEngine || {})
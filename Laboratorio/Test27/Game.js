var GameEngine = (function(GameEngine) {
  let cw;
  let ch;
  let platformCol;

  let KEY = GameEngine.KEY;

  class Game {
    constructor(ctx) {
      cw = ctx.canvas.width;
      ch = ctx.canvas.height;
      this.ctx = ctx;

      this.player = new GameEngine.Player(cw/2, ch-25, 16, 16);
      this.level = new GameEngine.Level();
      this.platforms = this.level.platforms;

      window.addEventListener("keydown", function(evt) {
        KEY.onKeyDown(evt.keyCode);
      });
      window.addEventListener("keyup", function(evt) {
        KEY.onKeyUp(evt.keyCode);
      });
    }

    processInput() {
      this.player.processInput();
    }

    update(elapsed) {
      this.player.update(elapsed);

      this.checkCollisionPlatforms(this.player, this.platforms);

      this.checkCollisionWalls();

      this.player.setState();
    }

    checkCollisionWalls() {
      if (this.player.x < this.player.w_2) {
        this.player.x = this.player.w_2;
        this.player.vx = 0;
      }
      if (this.player.x > cw-this.player.w_2) {
        this.player.x = cw-this.player.w_2;
        this.player.vx = 0;
      }
      if (this.player.y < this.player.h_2) {
        this.player.y = this.player.h_2;
        this.player.vy = 0;
      }
      if (this.player.y > ch-this.player.h_2) {
        this.player.y = ch-this.player.h_2;
        this.player.vy = 0;
        this.player.inFloor = true;
      }
    }

    checkCollisionPlatforms(player, platforms) {
      platformCol = [];
      let platform;

      // encontrar las plataformas con las que colisiona el jugador
      for (let i=0, l=platforms.length; i<l; i++) {
        platform = platforms[i];

        if ( Math.abs(player.x - platform.x) < player.w_2 + platform.w_2 && 
             Math.abs(player.y - platform.y) < player.h_2 + platform.h_2 ) {
          platformCol.push(platform);
        }
      }

      // resolver las colisiones con las plataformas encontradas
      if (platformCol.length > 0) {
        let overlapX, overlapY, platform;
      
        // resolver las colisiones en x
        for (let i=0, l=platformCol.length; i<l; i++) {
          platform = platformCol[i];
          overlapX = (player.w_2 + platform.w_2) - Math.abs(player.x - platform.x);
          overlapY = (player.h_2 + platform.h_2) - Math.abs(player.y - platform.y);
          
         if (overlapX < overlapY) {
            if (player.x - platform.x > 0) {
              player.x += overlapX;
            }
            else {
              player.x -= overlapX;
            }
          }
        }
  
        // resolver las colisiones en y
        for (let i=0, l=platformCol.length; i<l; i++) {
          platform = platformCol[i];
          overlapX = (player.w_2 + platform.w_2) - Math.abs(player.x - platform.x);
          overlapY = (player.h_2 + platform.h_2) - Math.abs(player.y - platform.y);
          
          if (overlapX > overlapY) {
            if (player.y - platform.y > 0) {
              player.y += overlapY;
            }
            else {
              player.y -= overlapY;
              player.inFloor = true;
            }
            player.vy = 0;
          }
        }
      }
    }

    render() {
      this.ctx.fillStyle = "#6b8cff";
      this.ctx.fillRect(0, 0, cw, ch);

      this.player.render(this.ctx);

      for (let i=0, l=this.platforms.length; i<l; i++) {
        this.platforms[i].render(this.ctx);
      }
    }
  }

  GameEngine.Game = Game;
  return GameEngine;
})(GameEngine || {})
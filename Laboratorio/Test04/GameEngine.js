var GameEngine = (function(GameEngine) {
  let cw;
  let ch;

  let pos_x = 50;
  let pos_y = 50;

  class Game {
    constructor(ctx) {
        cw = ctx.canvas.width;
        ch = ctx.canvas.height;
        this.ctx = ctx;
    }

  processInput() {}

  update() {
        pos_x = pos_x +1;
  }

  render() {
        this.ctx.clearRect(0, 0, cw, ch);
        this.ctx.fillStyle = "#ff0066";
        this.ctx.beginPath();
        this.ctx.arc(pos_x, pos_y, 30, 0, 2*Math.PI);
        this.ctx.fill();  
    }
}

    GameEngine.Game = Game;
    return GameEngine;
})(GameEngine || {})
window.addEventListener("load", function() {
    console.log("init");
    let theCanvas = document.getElementById("theCanvas");
    let ctx = theCanvas.getContext("2d");

    let game = new GameEngine.Game(ctx);

    (function gameLoop() {
        game.processInput();
        game.update();
        game.render();

    window.requestAnimationFrame(gameLoop);
  })();
});
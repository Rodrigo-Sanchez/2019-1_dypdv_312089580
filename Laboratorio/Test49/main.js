window.addEventListener("load", function() {
  let theCanvas = document.getElementById("theCanvas");
  let engine = new BABYLON.Engine(theCanvas, true);

  function createScene() {
    let scene = new BABYLON.Scene(engine);
    scene.clearColor = new BABYLON.Color3(1, 1, 1);

    let light = new BABYLON.DirectionalLight("directionalLight",
                                             new BABYLON.Vector3(-0.25, -1, -0.5),
                                             scene);
    light.diffuse = new BABYLON.Color3(1, 1, 1);

    let box1 = BABYLON.Mesh.CreateBox("Box1", 4, scene);
    let material1 = new BABYLON.StandardMaterial("material1", scene);
    material1.diffuseColor = new BABYLON.Color3(1, 0, 1);
    box1.material = material1;
    
    let box2 = BABYLON.Mesh.CreateBox("Box2", 4, scene);
    let material2 = new BABYLON.StandardMaterial("material2", scene);
    material2.wireframe = true;
    box2.material = material2;
    box2.position = new BABYLON.Vector3(0, 5, 0);

    // https://doc.babylonjs.com/api/classes/babylon.followcamera
    let camera = new BABYLON.FollowCamera("camera",
                                          new BABYLON.Vector3(0, 0, 0),
                                          scene,
                                          box1);
    camera.attachControl(theCanvas, true);
    camera.radius = 60;
    camera.heightOffset = 0;

    return scene;
  }

  let scene = createScene();

  engine.runRenderLoop(function() {
    scene.getMeshByName("Box1").position.x += 0.1;
    scene.getMeshByName("Box1").position.y += 0.1;
    scene.render();
  });
  
  window.addEventListener("resize", function() { 
    engine.resize();
  });
});
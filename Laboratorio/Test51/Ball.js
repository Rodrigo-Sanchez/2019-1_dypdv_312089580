var GameEngine = (function(GameEngine) {
  let PI_180 = Math.PI/180;

  class Ball {
    constructor(scene, id, cw, ch, x, y, size) {
      this.cw = cw;
      this.ch = ch;
      this.init_x = x;
      this.init_y = y;

      this.size = size;

      this.mesh = BABYLON.Mesh.CreateBox(id, size, scene);

      this.mesh.material = new BABYLON.StandardMaterial(id + "_material", this.scene);
      this.mesh.material.diffuseColor = new BABYLON.Color3(0, 0, 1);

      this.speed = 300;
      this.init();
    }

    init() {
      this.mesh.position.x = this.init_x;
      this.mesh.position.y = this.init_y;

      this.angle = ((-30 + Math.random()*60) + ((Math.random() < 0.5) ? 180 : 0)) * PI_180;
      this.vx = Math.cos(this.angle) * this.speed;
      this.vy = Math.sin(this.angle) * this.speed;
    }

    setX(x) {
      this.mesh.position.x = x;
    }
    setY(y) {
      this.mesh.position.y = y;
    }
    getX() {
      return this.mesh.position.x;
    }
    getY() {
      return this.mesh.position.y;
    }

    update(elapse) {
      this.mesh.position.x += this.vx * elapse;
      this.mesh.position.y += this.vy * elapse;
    }
  }

  GameEngine.Ball = Ball;
  return GameEngine;
})(GameEngine || {})
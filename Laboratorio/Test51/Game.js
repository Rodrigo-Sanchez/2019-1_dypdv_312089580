var GameEngine = (function(GameEngine) {
  let keys = {};
  let cw = 800;
  let ch = 600;

  class Game {
    constructor(engine) {
      this.engine = engine;

      this.scene = new BABYLON.Scene(engine);
      this.scene.clearColor = new BABYLON.Color3(1, 1, 1);

      this.camera = new BABYLON.UniversalCamera("camera", new BABYLON.Vector3(0, 0, -710), this.scene);
      this.camera.setTarget(BABYLON.Vector3.Zero());
      
      this.light = new BABYLON.DirectionalLight("directionalLight",
                                                new BABYLON.Vector3(0, 0, 1),
                                                this.scene);
      this.light.diffuse = new BABYLON.Color3(1, 1, 1);

      this.background = new BABYLON.Mesh.CreatePlane("background", 1, this.scene);
      this.background.material = new BABYLON.StandardMaterial("background_material", this.scene);
      this.background.material.diffuseColor = new BABYLON.Color3(0, 0.25, 0);
      this.background.material.specularColor = new BABYLON.Color3(0, 0, 0);
      this.background.scaling.x = cw;
      this.background.scaling.y = ch;

      this.ball = new GameEngine.Ball(this.scene, "ball", cw, ch, 0, 0, 15);

      this.paddle_left  = new GameEngine.Paddle(this.scene, "paddle_left", cw, ch, -390, 0, 15, 80);
      this.paddle_right = new GameEngine.Paddle(this.scene, "paddle_right", cw, ch, 390, 0, 15, 80);

      this.game_initialized = false;

      // GUI
      this.init_text = new BABYLON.GUI.TextBlock();
      this.init_text.top = "20%";
      this.init_text.text = "Presiona espacio para comenzar";
      this.init_text.color = "white";
      this.init_text.resizeToFit = true;
      this.init_text.fontSize = "5%";

      this.score_left = new BABYLON.GUI.TextBlock();
      this.score_left.left = "-16%";
      this.score_left.top = "-40%";
      this.score_left.text = "0";
      this.score_left.color = "white";
      this.score_left.fontSize = "6%";

      this.score_right = new BABYLON.GUI.TextBlock();
      this.score_right.left = "16%";
      this.score_right.top = "-40%";
      this.score_right.text = "0";
      this.score_right.color = "white";
      this.score_right.fontSize = "6%";

      var GUITexture = BABYLON.GUI.AdvancedDynamicTexture.CreateFullscreenUI("UI");
      GUITexture.addControl(this.init_text);    
      GUITexture.addControl(this.score_left);    
      GUITexture.addControl(this.score_right);   

      // register keyboard input
      this.scene.actionManager = new BABYLON.ActionManager(this.scene);
      this.scene.actionManager.registerAction(
        new BABYLON.ExecuteCodeAction(BABYLON.ActionManager.OnKeyDownTrigger, 
          function(evt) {
            keys[evt.sourceEvent.key] = true;
          }
        )
      );
      this.scene.actionManager.registerAction(
        new BABYLON.ExecuteCodeAction(BABYLON.ActionManager.OnKeyUpTrigger, 
          function(evt) {
            keys[evt.sourceEvent.key] = false;
          }
        )
      );

    }

    processInput() {
      this.paddle_left.processInput(keys["w"]||keys["W"], keys["s"]||keys["S"]);
      this.paddle_right.processInput(keys["ArrowUp"], keys["ArrowDown"]);

      this.space_pressed = keys[" "];
    }

    update(elapsed) {
      if ((this.game_initialized === false) && this.space_pressed) {
        this.game_initialized = true;
        this.score_left.text = "0";
        this.score_right.text = "0";
      }

      if (this.game_initialized) {
        this.init_text.isVisible = false;
        
        this.paddle_left.update(elapsed);
        this.paddle_right.update(elapsed);
        this.ball.update(elapsed);

        if (this.ball.getX() > (cw - this.ball.size)/2) {
          this.score_left.text = (parseInt(this.score_left.text)+1).toString();
          this.ball.init();
        }
        if (this.ball.getX() < (-cw + this.ball.size)/2) {
          this.score_right.text = (parseInt(this.score_right.text)+1).toString();
          this.ball.init();
        }

        if ((this.score_left.text == "5") || (this.score_right.text == "5")) {
          this.game_initialized = false;
          this.init_text.isVisible = true;
          this.paddle_left.init();
          this.paddle_right.init();
        }

        if (this.ball.getY() > (ch - this.ball.size)/2) {
          this.ball.setY( (ch - this.ball.size)/2 );
          this.ball.vy *= -1;
        }
        if (this.ball.getY() < (-ch + this.ball.size)/2) {
          this.ball.setY( (-ch + this.ball.size)/2 );
          this.ball.vy *= -1;
        }
      }

      this.collissionBallPaddle(this.paddle_left, this.ball);
      this.collissionBallPaddle(this.paddle_right, this.ball);
    }

    collissionBallPaddle(paddle, ball) {
      let px = paddle.getX();
      let py = paddle.getY();

      let bx = ball.getX();
      let by = ball.getY();

      if (Math.abs(px - bx) < (paddle.w + ball.size)/2) {
        if ((by < py + paddle.h/2) && (by > py - paddle.h/2)) {
          ball.vx *= -1.1;
          ball.setX( px + Math.sign(ball.vx)*(paddle.w + ball.size)/2 );
        }
      }
    }

    render() {
      this.scene.render();
    }
  }

  GameEngine.Game = Game;
  return GameEngine;
})(GameEngine || {})
var GameEngine = (function(GameEngine) {

  class Paddle {
    constructor(scene, id, cw, ch, x, y, w, h) {
      this.cw = cw;
      this.ch = ch;
      this.init_x = x;
      this.init_y = y;
      this.w = w;
      this.h = h;

      this.mesh = BABYLON.Mesh.CreateBox(id, 1, scene);
      this.mesh.scaling.x = w;
      this.mesh.scaling.y = h;

      this.vy = 0;
      this.speed_y = 300;

      this.mesh.material = new BABYLON.StandardMaterial(id + "_material", this.scene);
      this.mesh.material.diffuseColor = new BABYLON.Color3(0.9, 0.75, 0.3);

      this.init();
    }

    processInput(up, down) {
      this.vy = 0;

      if (up) {
        this.vy = this.speed_y;
      }
      if (down) {
        this.vy = -this.speed_y;
      }
    }

    init() {
      this.mesh.position.x = this.init_x;
      this.mesh.position.y = this.init_y;
    }

    setX(x) {
      this.mesh.position.x = x;
    }
    setY(y) {
      this.mesh.position.y = y;
    }
    getX() {
      return this.mesh.position.x;
    }
    getY() {
      return this.mesh.position.y;
    }

    update(elapse) {
      this.mesh.position.y += this.vy * elapse;
      if (this.mesh.position.y > (this.ch - this.h)/2) {
        this.mesh.position.y = (this.ch - this.h)/2;
      }
      if (this.mesh.position.y < (-this.ch + this.h)/2) {
        this.mesh.position.y = (-this.ch + this.h)/2;
      }
    }
  }

  GameEngine.Paddle = Paddle;
  return GameEngine;
})(GameEngine || {})
var GameEngine = (function(GameEngine) {

  class Camera {
    constructor(x, y, cw, ch) {
      this.x = x;
      this.y = y;
      this.stage_w = cw;
      this.stage_h = ch;
    }

    applyTransform(ctx) {
      ctx.save();
      ctx.translate(parseInt(this.stage_w/2 -this.x), parseInt(this.stage_h/2 -this.y));
    }
    releaseTransform(ctx) {
      ctx.restore();
    }

    render(ctx) {
      ctx.lineWidth = 2;
      ctx.strokeStyle = "white";
      ctx.beginPath();
      ctx.moveTo(parseInt(this.x - 20), parseInt(this.y));
      ctx.lineTo(parseInt(this.x + 20), parseInt(this.y));
      ctx.moveTo(parseInt(this.x), parseInt(this.y - 20));
      ctx.lineTo(parseInt(this.x), parseInt(this.y + 20));
      ctx.stroke();
    }
  }

  GameEngine.Camera = Camera;
  return GameEngine;
})(GameEngine || {})
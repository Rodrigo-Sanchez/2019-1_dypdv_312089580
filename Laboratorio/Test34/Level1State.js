var GameEngine = (function(GameEngine) {
  let cw;
  let ch;

  class Level1State {
    constructor(game, w, h) {
      this.game = game;
      cw = w;
      ch = h;

      this.camera = new GameEngine.Camera(cw/2, ch/2-8, cw, ch);

      this.player = new GameEngine.Player(88, ch-125, 16, 16);
      this.level = new GameEngine.Level();

      // this.background_audio = new Audio("audios/level_1.mp3");
      this.background_audio = new Audio("https://joellongi.bitbucket.io/2019-1/recursos/audios/level_1.mp3");
      this.background_audio.loop = true;
      this.background_audio.volume = 0.5;
      this.background_audio.addEventListener("canplay", function() {
        this.play();
      });
    }

    processInput() {
      this.player.processInput();
    }

    update(elapsed) {
      this.player.update(elapsed);

      this.checkCollisionPlatforms(this.player, this.level);

      this.player.setState();

      let dx = this.camera.x - this.player.x;
      if (dx < 0) {
        this.camera.x = Math.min(this.level.w -cw/2, this.lerp(this.camera.x, this.player.x, 0.25));
      }

      this.checkCollisionWalls();
    }

    checkCollisionWalls() {
      if (this.player.x < this.camera.x -cw/2 +this.player.w_2) {
        this.player.x = this.camera.x -cw/2 +this.player.w_2;
      }
      if (this.player.x > this.camera.x +cw/2 -this.player.w_2) {
        this.player.x = this.camera.x +cw/2 -this.player.w_2;
      }

      if (this.player.y > ch-this.player.h_2) {
        this.player.y = 0;
        this.player.vy = 0;
      }
    }

    checkCollisionPlatforms(player, level) {
      let player_tile_pos = level.getTilePos(player.x, player.y);

      //center
      this.reactCollision(player, level.getPlatform(player_tile_pos.x,   player_tile_pos.y));
      // left
      this.reactCollision(player, level.getPlatform(player_tile_pos.x-1, player_tile_pos.y));
      // right
      this.reactCollision(player, level.getPlatform(player_tile_pos.x+1, player_tile_pos.y));

      // top
      this.reactCollision(player, level.getPlatform(player_tile_pos.x,   player_tile_pos.y-1));
      // bottom
      this.reactCollision(player, level.getPlatform(player_tile_pos.x,   player_tile_pos.y+1));

      // left top
      this.reactCollision(player, level.getPlatform(player_tile_pos.x-1, player_tile_pos.y-1));
      // right top
      this.reactCollision(player, level.getPlatform(player_tile_pos.x+1, player_tile_pos.y-1));

      // left bottom
      this.reactCollision(player, level.getPlatform(player_tile_pos.x-1, player_tile_pos.y+1));
      // right bottom
      this.reactCollision(player, level.getPlatform(player_tile_pos.x+1, player_tile_pos.y+1));
    }

    reactCollision(player, platform) {
      if ( platform &&
           Math.abs(player.x - platform.x) < player.w_2 + platform.w_2 && 
           Math.abs(player.y - platform.y) < player.h_2 + platform.h_2 ) {

        let overlapX = (player.w_2 + platform.w_2) - Math.abs(player.x - platform.x);
        let overlapY = (player.h_2 + platform.h_2) - Math.abs(player.y - platform.y);

        if (overlapX < overlapY) {
          if (player.x - platform.x > 0) {
            player.x += overlapX;
          }
          else {
            player.x -= overlapX;
          }
        }
        else if (overlapX > overlapY) {
          if (player.y - platform.y > 0) {
            player.y += overlapY;
            if (player.vy < 0) {
              player.vy = 0;
            }
          }
          else {
            player.y -= overlapY;
            if (player.vy > 0) {
              player.inFloor = true;
              player.vy = 0;
            }
          }
        }
      }
    }

    lerp(v0, v1, t) {
      return v0 + (v1 - v0)*t;
    }

    render(ctx) {
      ctx.fillStyle = "#6b8cff";
      ctx.fillRect(0, 0, cw, ch);

      this.camera.applyTransform(ctx);

      this.level.render(ctx);
      this.player.render(ctx);

      this.camera.releaseTransform(ctx);
    }
  }

  GameEngine.Level1State = Level1State;
  return GameEngine;
})(GameEngine || {})
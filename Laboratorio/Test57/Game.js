var GameEngine = (function(GameEngine) {
  let keys = {};

  class Game {
    constructor(engine, scene) {
      this.engine = engine;
      this.scene = scene;
      this.scene.gravity = new BABYLON.Vector3(0, -9.81, 0);
      this.scene.collisionsEnabled = true;

      BABYLON.Animation.AllowMatricesInterpolation = false;

      scene.ambientColor = new BABYLON.Color3(0.75, 0.75, 0.75);

      this.camera = new BABYLON.FollowCamera("FollowCam", new BABYLON.Vector3(0, 20, 10), this.scene);
      this.camera.radius = 300;
      this.camera.heightOffset = 100;
      this.camera.cameraAcceleration = 0.05
      this.camera.maxCameraSpeed = 10;


      this.light = new BABYLON.DirectionalLight("directionalLight", 
                                                new BABYLON.Vector3(-1, -2, -1), 
                                                this.scene);
      this.light.position = new BABYLON.Vector3(0, 150, 20);


      let shadowGenerator = new BABYLON.ShadowGenerator(1024, this.light);
   
      
      let ground = BABYLON.Mesh.CreateGround("Ground", 4600, 4600, 2, this.scene);
      ground.material = new BABYLON.StandardMaterial("GroundMat", this.scene);
      ground.material.diffuseColor = new BABYLON.Color3(0.3, 0.2, 0);
      ground.material.specularColor = new BABYLON.Color3(0, 0, 0);
      ground.receiveShadows = true;
      ground.checkCollisions = true;
     
      let wall1 = BABYLON.MeshBuilder.CreateBox("Wall1", {width: 1000, height: 350, depth: 10}, this.scene);
      wall1.position = new BABYLON.Vector3(0, -30, -500);
      wall1.checkCollisions = true;

      this.ball = BABYLON.MeshBuilder.CreateSphere("Ball", {diameter: 100}, this.scene);
      this.ball.material = new BABYLON.StandardMaterial("BallMat", this.scene);
      this.ball.emissiveColor = new BABYLON.Color3(0, 0, 1);
      this.ball.position.x = -500;
      this.ball.position.y = 50;

      this.rug = BABYLON.MeshBuilder.CreateBox("Wall1", {width: 100, height: 4, depth: 100}, this.scene);
      this.rug.position.x = 300;
      this.rug.position.z = 300;


      this.character = new GameEngine.Character(this.scene);
      shadowGenerator.getShadowMap().renderList.push(this.character.mesh);
      this.camera.lockedTarget = this.character.cameraTarget;

      let ramp = scene.getMeshByName("Rampa");
      ramp.checkCollisions = true;

      let weapon = BABYLON.MeshBuilder.CreateSphere("Ball", {diameter: 1}, this.scene);
      weapon.position.x = 10;
      weapon.position.y = 10;
      weapon.position.z = 50;
      weapon.parent = this.camera;



      // register keyboard input
      this.scene.actionManager = new BABYLON.ActionManager(this.scene);
      this.scene.actionManager.registerAction(
        new BABYLON.ExecuteCodeAction(BABYLON.ActionManager.OnKeyDownTrigger, 
          function(evt) {
            keys[evt.sourceEvent.key] = true;
          }
        )
      );
      this.scene.actionManager.registerAction(
        new BABYLON.ExecuteCodeAction(BABYLON.ActionManager.OnKeyUpTrigger, 
          function(evt) {
            delete keys[evt.sourceEvent.key];
          }
        )
      );

    }

    processInput() {
      this.character.processInput(keys);
    }

    update(elapsed) {
      this.character.update(elapsed);

      if (this.character.mesh.intersectsMesh(this.ball, false)) {
        this.ball.material.emissiveColor = new BABYLON.Color3(1, 0, 0);
      }
      else {
        this.ball.material.emissiveColor = new BABYLON.Color3(0, 0, 1);
      }

      if (this.rug.intersectsPoint(this.character.mesh.position)) {
        this.rug.visibility = 0;
      }
      else {
        this.rug.visibility = 1;
      }
    }

    render() {
      this.scene.render();
    }
  }

  GameEngine.Game = Game;
  return GameEngine;
})(GameEngine || {})
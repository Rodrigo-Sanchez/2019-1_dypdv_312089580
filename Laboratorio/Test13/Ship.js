var GameEngine = (function(GameEngine) {
  let PI2 = 2*Math.PI;
  let PI_180 = Math.PI/180;

  let KEY = GameEngine.KEY;

  class Ship {
    constructor(x, y, size) {
      this.x = x;
      this.y = y;
      this.vx = 0;
      this.vy = 0;
      this.vr = 0;
      this.ax = 0;
      this.ay = 0;
      this.trust = 0;
      this.friction = 0.97;

      this.size = size;
      this.rotation = 0;
      this.showFlame = false;
    }

    processInput() {
      this.vr = 0;
      this.showFlame = false;
      this.trust = 0;

      if (KEY.isPress(KEY.LEFT)) {
        this.vr = -3;
      }
      if (KEY.isPress(KEY.RIGHT)) {
        this.vr = 3;
      }
      if (KEY.isPress(KEY.UP)) {
        this.trust = 20;
        this.showFlame = true;
      }
      if (KEY.isPress(KEY.DOWN)) {
        this.trust = -2;
      }
    }

    update(elapsed) {
      this.rotation += this.vr * PI_180;
      this.ax = Math.cos(this.rotation) * this.trust;
      this.ay = Math.sin(this.rotation) * this.trust;

      this.vx += this.ax;
      this.vy += this.ay;

      this.vx *= this.friction;
      this.vy *= this.friction;

      this.x += this.vx * elapsed;
      this.y += this.vy * elapsed;
    }
    
    render(ctx) {
      let tmp_angle = 125;
      let cos_back = Math.cos(tmp_angle * PI_180);
      let sin_back = Math.sin(tmp_angle * PI_180);
      let size_2 = this.size/2;
      let size_3 = this.size/3;
      let size_8 = this.size/8;

      ctx.save();
      ctx.translate(this.x, this.y);
      ctx.rotate(this.rotation);

      if (this.showFlame) {
        ctx.fillStyle = "#ff0000";
        ctx.beginPath();
        ctx.moveTo(-size_2, 0)
        ctx.lineTo(-size_8,  size_3 * sin_back)
        ctx.lineTo(-size_8, -size_3 * sin_back)
        ctx.closePath();
        ctx.fill();
      }

      ctx.fillStyle = "#ffffff";
      ctx.beginPath();
      ctx.moveTo(size_2, 0);
      ctx.lineTo(size_2 * cos_back, size_2 * sin_back);
      ctx.lineTo(-size_8, 0);
      ctx.lineTo(size_2 * cos_back, -size_2 * sin_back);
      ctx.closePath();
      ctx.fill();

      ctx.restore();
    }
  }

  GameEngine.Ship = Ship;
  return GameEngine;
})(GameEngine || {})
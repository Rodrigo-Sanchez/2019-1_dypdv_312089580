var GameEngine = (function(GameEngine) {
  let map_w = 16;
  let map_h = 15;
  let tile_w = 16;
  let tile_h = 16;
  let mapData = [
    14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,
    14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,
    14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,
    14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,
    14,14,14,14,14,14,14,14,3,4,14,14,14,14,14,14,
    14,14,14,14,14,14,14,14,5,6,14,14,14,14,14,14,
    7,7,7,7,7,13,7,7,7,7,13,7,14,14,7,7,
    18,18,18,18,18,13,18,18,14,14,13,14,14,14,14,14,
    22,22,22,14,14,13,21,19,14,14,3,4,14,14,14,14,
    23,23,23,14,14,13,19,19,14,14,5,6,14,14,14,14,
    21,19,16,14,14,13,19,21,3,4,3,4,14,14,14,14,
    19,19,19,19,19,13,19,19,5,6,5,6,14,14,14,14,
    7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,
    1,2,1,2,1,2,1,2,1,2,1,2,1,2,1,2,
    2,1,2,1,2,1,2,1,2,1,2,1,2,1,2,1
  ];

  class Level {
    constructor() {
      this.platforms = {};

      this.w = map_w * tile_w;
      this.h = map_h * tile_h;

      this.tileAtlas = new GameEngine.TileAtlas(16, 16, "images/tileset.svg", 8, 16, 16);

      let platform;
      let x, y;

      for (let i=0, l=map_w*map_h; i<l; i++) {
        if ((mapData[i] > 0) && (mapData[i] < 14)) {
          x = (i%map_w);
          y = parseInt(i/map_w);

          if (mapData[i] !== 13) {
            platform = new GameEngine.Platform( x*tile_w + tile_w/2, 
                                                y*tile_h + tile_h/2, 
                                                tile_w, 
                                                tile_h );
          }
          else {
            platform = new GameEngine.Ladder( x*tile_w + tile_w/2, 
                                              y*tile_h + tile_h/2, 
                                              tile_w, 
                                              tile_h );
          }

          platform.tileAtlas = this.tileAtlas;
          platform.currentFrame = mapData[i]-1;
          this.platforms["tile_"+x+"_"+y] = platform;
        }
      }
    }

    getTilePos(x, y) {
      return {
        x: parseInt(x/tile_w), 
        y: parseInt(y/tile_h)
      };
    }

    getPlatform(x, y) {
      return this.platforms["tile_"+x+"_"+y];
    }

    render(ctx) {
      for (let i=0, l=map_w*map_h; i<l; i++) {
        if (mapData[i] > 0) {
          this.tileAtlas.render(ctx, mapData[i]-1, (i%map_w)*tile_w + tile_w/2, parseInt(i/map_w)*tile_h + tile_h/2);
        }
      }
    }
  }

  GameEngine.Level = Level;
  return GameEngine;
})(GameEngine || {})
var GameEngine = (function(GameEngine) {
  let gravity = 16;

  let KEY = GameEngine.KEY;

  class Player extends GameEngine.Sprite {
    constructor(x, y, w, h) {
      super(x, y, w, h, "images/player.svg", 14, 32, 32);

      this.frameCounter = 0;
      this.framesPerChange = 8;

      this.ladderCounter = 0;
      this.ladderFramesPerChange = 12;

      this.w_2 = w/2;
      this.h_2 = h/2;

      this.jump_heigth = 295;
      this.inFloor = false;

      this.speed = 95;
      this.vx = 0;
      this.vy = 0;
      this.canJump = true;
    }

    processInput() {
      this.vx = 0;
      this.ladderVy = 0;
      this.tryGrabLadder = false;

      if (KEY.isReleased(KEY.SPACE)) {
        this.canJump = true;
      }
      if ((KEY.isPress(KEY.SPACE)) && (this.canJump) && (this.inFloor)) {
        this.vy = -this.jump_heigth;
        this.canJump = false;
      }

      if ((KEY.isPress(KEY.SPACE)) && (this.inLadder)) {
        this.inLadder = false;
        this.w = 32;
        this.w_2 = 16;
      }

      if (KEY.isPress(KEY.LEFT)) {
        this.vx = -this.speed;
        this.direction = -1;
      }
      if (KEY.isPress(KEY.RIGHT)) {
        this.vx = this.speed;
        this.direction = 1;
      }

      if (KEY.isPress(KEY.UP)) {
        this.tryGrabLadder = true;
        this.ladderVy = -this.speed/2;
        if (this.inLadder) {
          if (((this.ladderCounter++)%this.ladderFramesPerChange) === 0) {
            this.direction *= -1;
          }
        }
      }
      if (KEY.isPress(KEY.DOWN)) {
        this.tryGrabLadder = true;
        this.ladderVy = this.speed/2;
        if (this.inLadder) {
          if (((this.ladderCounter++)%this.ladderFramesPerChange) === 0) {
            this.direction *= -1;
          }
        }
      }
    }

    setState() {
      if (!this.inLadder) {
        if (this.vx !== 0) {
          this.state = "walking";
        }
        else if (this.inFloor) {
          this.state = "still";
        }
        if (!this.inFloor) {
          this.state = "jumping";
        }
      }
      else {
        this.state = "ladder";
      }
    }

    update(elapsed) {
      this.inFloor = false;

      if (!this.inLadder) {
        this.vy += gravity;

        super.update(elapsed);
      }
      else {
        this.vx = 0;
        this.vy = this.ladderVy;
        super.update(elapsed);
      }
    }
    
    render(ctx) {
      if (this.state === "walking") {
        this.frameCounter = (this.frameCounter +1)%(this.framesPerChange*4);
        let theFrame = parseInt(this.frameCounter/this.framesPerChange);
        if (theFrame === 3) {
          theFrame = 1;
        }
        this.currentFrame = 1 + theFrame;
      }
      else if (this.state === "still") {
        this.currentFrame = 0;
      }
      else if (this.state === "jumping") {
        this.currentFrame = 4;
      }
      else if (this.state === "ladder") {
        this.currentFrame = 5;
      }

      ctx.fillStyle = "rgba(100, 50, 25, 0.5)";
      ctx.beginPath();
      ctx.moveTo(this.x - this.w_2, this.y - this.h_2);
      ctx.lineTo(this.x - this.w_2, this.y + this.h_2);
      ctx.lineTo(this.x + this.w_2, this.y + this.h_2);
      ctx.lineTo(this.x + this.w_2, this.y - this.h_2);
      ctx.lineTo(this.x - this.w_2, this.y - this.h_2);
      ctx.fill();

      super.render(ctx)
    }
  }

  GameEngine.Player = Player;
  return GameEngine;
})(GameEngine || {})
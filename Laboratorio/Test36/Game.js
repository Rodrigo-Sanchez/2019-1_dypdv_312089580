var GameEngine = (function(GameEngine) {
  let cw;
  let ch;

  let KEY = GameEngine.KEY;

  class Game {
    constructor(ctx) {
      cw = ctx.canvas.width;
      ch = ctx.canvas.height;
      this.ctx = ctx;

      this.camera = new GameEngine.Camera(cw/2, ch/2, cw, ch);

      this.player = new GameEngine.Player(cw/2, ch-90, 32, 32);
      this.level = new GameEngine.Level();

      window.addEventListener("keydown", function(evt) {
        KEY.onKeyDown(evt.keyCode);
      });
      window.addEventListener("keyup", function(evt) {
        KEY.onKeyUp(evt.keyCode);
      });
    }

    processInput() {
      this.player.processInput();
    }

    update(elapsed) {
      if ((this.player.tryGrabLadder) && (!this.player.inLadder)) {
        this.checkCollisionLadders(this.player, this.level);
      }

      this.player.update(elapsed);

      if (this.player.inLadder) {
        this.checkInLadders(this.player, this.level);
      }

      this.checkCollisionPlatforms(this.player, this.level);

      if (this.player.inFloor) {
        this.player.inLadder = false;
      }

      this.player.setState();

      this.camera.update(this.player, this.level);

      this.checkCollisionWalls();
    }

    checkCollisionLadders(player, level) {
      let player_tile_pos = level.getTilePos(player.x, player.y);
      let ladder;

      // top
      ladder = level.getPlatform(player_tile_pos.x, player_tile_pos.y-1);
      if (ladder && (ladder.type === "Ladder") && (player.ladderVy < 0)) {
        this.player.x = ladder.x;
        this.player.inLadder = true;
        return;
      }

      //center
      ladder = level.getPlatform(player_tile_pos.x, player_tile_pos.y);
      if (ladder && (ladder.type === "Ladder")) {
        this.player.x = ladder.x;
        this.player.inLadder = true;
        return;
      }

      // bottom
      ladder = level.getPlatform(player_tile_pos.x, player_tile_pos.y+1);
      if (ladder && (ladder.type === "Ladder") && (player.ladderVy > 0)) {
        this.player.x = ladder.x;
        this.player.inLadder = true;
        return;
      }
    }

    checkInLadders(player, level) {
      let player_tile_pos = level.getTilePos(player.x, player.y + player.h_2);
      let ladder;
      //center
      ladder = level.getPlatform(player_tile_pos.x, player_tile_pos.y);
      if (ladder && (ladder.type === "Ladder")) {
        this.player.inLadder = true;
        this.player.w = 16;
        this.player.w_2 = 8;
      }
      else {
        this.player.inLadder = false;
        this.player.w = 32;
        this.player.w_2 = 16;
      }
    }

    checkCollisionWalls() {
      if (this.player.x < this.camera.x -cw/2 +this.player.w_2) {
        this.player.x = this.camera.x -cw/2 +this.player.w_2;
      }
      if (this.player.x > this.camera.x +cw/2 -this.player.w_2) {
        this.player.x = this.camera.x +cw/2 -this.player.w_2;
      }

      if (this.player.y > ch-this.player.h_2) {
        this.player.y = 0;
        this.player.vy = 0;
      }
    }

    checkCollisionPlatforms(player, level) {
      let player_tile_pos = level.getTilePos(player.x, player.y);

      //center
      this.reactCollision(player, level.getPlatform(player_tile_pos.x,   player_tile_pos.y));
      // left
      this.reactCollision(player, level.getPlatform(player_tile_pos.x-1, player_tile_pos.y));
      // right
      this.reactCollision(player, level.getPlatform(player_tile_pos.x+1, player_tile_pos.y));

      // top
      this.reactCollision(player, level.getPlatform(player_tile_pos.x,   player_tile_pos.y-1));
      // bottom
      this.reactCollision(player, level.getPlatform(player_tile_pos.x,   player_tile_pos.y+1));

      // left top
      this.reactCollision(player, level.getPlatform(player_tile_pos.x-1, player_tile_pos.y-1));
      // right top
      this.reactCollision(player, level.getPlatform(player_tile_pos.x+1, player_tile_pos.y-1));

      // left bottom
      this.reactCollision(player, level.getPlatform(player_tile_pos.x-1, player_tile_pos.y+1));
      // right bottom
      this.reactCollision(player, level.getPlatform(player_tile_pos.x+1, player_tile_pos.y+1));
    }

    reactCollision(player, platform, type) {
      if ( platform &&
           platform.type === "Platform" &&
           Math.abs(player.x - platform.x) < player.w_2 + platform.w_2 && 
           Math.abs(player.y - platform.y) < player.h_2 + platform.h_2 ) {

        let overlapX = (player.w_2 + platform.w_2) - Math.abs(player.x - platform.x);
        let overlapY = (player.h_2 + platform.h_2) - Math.abs(player.y - platform.y);

        if (overlapX < overlapY) {
          if (player.x - platform.x > 0) {
            player.x += overlapX;
          }
          else {
            player.x -= overlapX;
          }
        }
        else if (overlapX > overlapY) {
          if (player.y - platform.y > 0) {
            player.y += overlapY;
            if (player.vy < 0) {
              player.vy = 0;
            }
          }
          else {
            player.y -= overlapY;
            if (player.vy > 0) {
              player.inFloor = true;
              player.vy = 0;
            }
          }
        }
      }
    }

    render() {
      this.ctx.fillStyle = "#5ad0e1";
      this.ctx.fillRect(0, 0, cw, ch);

      this.camera.applyTransform(this.ctx);

      this.level.render(this.ctx);
      this.player.render(this.ctx);

      this.camera.releaseTransform(this.ctx);
    }
  }

  GameEngine.Game = Game;
  return GameEngine;
})(GameEngine || {})
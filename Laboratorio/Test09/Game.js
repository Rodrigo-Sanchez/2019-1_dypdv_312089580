var GameEngine = (function(GameEngine) {
  let cw;
  let ch;

  class Game {
    constructor(ctx) {
      cw = ctx.canvas.width;
      ch = ctx.canvas.height;
      this.ctx = ctx;

      let num_balls = 150;
      let radius;
      let color;
      this.balls = [];
      for (let i=0; i<num_balls; i++) {
        radius = 10 + Math.random()*20;
        color = "rgba("+ (Math.random()*255) + "," + (Math.random()*255) + "," + (Math.random()*255) +",1)";
        this.balls.push( new GameEngine.Ball(radius+Math.random()*(cw-2*radius), radius+Math.random()*(ch-2*radius), radius, color) );
      }
    }

    processInput() {
    }

    update(elapsed) {
      for (let i=0; i<this.balls.length; i++) {
        this.balls[i].update(elapsed);
        this.checkBorder(this.balls[i]);
      }
      for (let i=0; i<this.balls.length-1; i++) {
        for (let j=i+1; j<this.balls.length; j++) {
          this.checkCollision(this.balls[i], this.balls[j], elapsed);
        }
      }
    }

    checkCollision(ball0, ball1, elapsed) {
      let dx = ball1.x - ball0.x,
          dy = ball1.y - ball0.y,
          dist = Math.sqrt(dx * dx + dy * dy);

      if (dist < ball0.size + ball1.size) {
        let angle = Math.atan2(dy, dx),
            sin = Math.sin(angle),
            cos = Math.cos(angle),

            x0 = 0,
            y0 = 0,

            x1 = dx * cos + dy * sin,
            y1 = dy * cos - dx * sin,

            vx0 = ball0.vx * cos + ball0.vy * sin,
            vy0 = ball0.vy * cos - ball0.vx * sin,

            vx1 = ball1.vx * cos + ball1.vy * sin,
            vy1 = ball1.vy * cos - ball1.vx * sin,

            vxTotal = vx0 - vx1;

        let tmpvx0 = ((ball0.mass - ball1.mass) * vx0 + 2 * ball1.mass * vx1) / (ball0.mass + ball1.mass);
        vx1 = ((ball1.mass - ball0.mass) * vx1 + 2 * ball0.mass * vx0) / (ball0.mass + ball1.mass);
        vx0 = tmpvx0;

        let absV = Math.abs(vx0 * elapsed) + Math.abs(vx1 * elapsed),
            overlap = (ball0.size + ball1.size) - Math.abs(x0 - x1);
        x0 += (vx0 / absV * overlap)* elapsed;
        x1 += (vx1 / absV * overlap)* elapsed;

        let x0Final = x0 * cos - y0 * sin,
            y0Final = y0 * cos + x0 * sin,
            x1Final = x1 * cos - y1 * sin,
            y1Final = y1 * cos + x1 * sin;

        ball1.x = ball0.x + x1Final;
        ball1.y = ball0.y + y1Final;
        ball0.x = ball0.x + x0Final;
        ball0.y = ball0.y + y0Final;

        ball0.vx = vx0 * cos - vy0 * sin;
        ball0.vy = vy0 * cos + vx0 * sin;
        ball1.vx = vx1 * cos - vy1 * sin;
        ball1.vy = vy1 * cos + vx1 * sin;
      }
    }

    checkBorder(ball) {
      if (ball.x < ball.size) {
        ball.vx *= -1;
        ball.x = ball.size;
      }
      if (ball.x > cw-ball.size) {
        ball.vx *= -1;
        ball.x = cw-ball.size;
      }
      if (ball.y < ball.size) {
        ball.vy *= -1;
        ball.y = ball.size;
      }
      if (ball.y > ch-ball.size) {
        ball.vy *= -1;
        ball.y = ch-ball.size;
      }
    }

    render() {
      this.ctx.clearRect(0, 0, cw, ch);

      for (let i=0; i<this.balls.length; i++) {
        this.balls[i].render(this.ctx, i);
      }
    }
  }

  GameEngine.Game = Game;
  return GameEngine;
})(GameEngine || {})
var GameEngine = (function(GameEngine) {
  let cw;
  let ch;
  let grid_size = 16;

  let directions = ["N", "S", "E", "W"];

  function randomSort(a, b) { 
    return (Math.random() < 0.5) ? -1 : 1; 
  }

  class Game {
    constructor(ctx) {
      this.ctx = ctx;
      cw = 40;
      ch = 30;

      this.grid = [];
      for (let i=0; i<cw*ch; i++) {
        this.grid.push(new GameEngine.Cell());
      }

      this.carve_passages_from(0, 0, this.grid);
    }

    carve_passages_from(cx, cy, grid) {
      grid[cx + cy*cw].visited = true;

      let dir = Array.from(directions.sort(randomSort));

      for (let i=0; i<dir.length; i++) {
        let nx = cx;
        let ny = cy;
        if (dir[i] === "E") {
          nx++;
        }
        if (dir[i] === "W") {
          nx--;
        }
        if (dir[i] === "N") {
          ny++;
        }
        if (dir[i] === "S") {
          ny--;
        }

        if ( (0 <= nx) && (nx < cw) && (0 <= ny) && (ny < ch) && (!grid[nx + ny*cw].visited) ) {
          switch (dir[i]) {
            case "N":
              grid[cx + cy*cw].north = false;
              grid[nx + ny*cw].south = false;
              break;
            case "S":
              grid[cx + cy*cw].south = false;
              grid[nx + ny*cw].north = false;
              break;
            case "E":
              grid[cx + cy*cw].east  = false;
              grid[nx + ny*cw].weast = false;
              break;
            case "W":
              grid[cx + cy*cw].weast = false;
              grid[nx + ny*cw].east  = false;
              break;
          }

          this.carve_passages_from(nx, ny, grid);
        }
      }
    }

    processInput() {
    }

    update(elapsed) {
    }

    render() {
      let ctx = this.ctx;

      for (let i=0; i<cw*ch; i++) {
        let x = i%cw;
        let y = parseInt(i/cw);

        ctx.strokeStyle = "#000000";
        ctx.beginPath();

        if (this.grid[i].south) {
          ctx.moveTo(x*grid_size, y*grid_size);
          ctx.lineTo((x+1)*grid_size, y*grid_size);
        }
        if (this.grid[i].east) {
          ctx.moveTo((x+1)*grid_size, y*grid_size);
          ctx.lineTo((x+1)*grid_size, (y+1)*grid_size);
        }
        if (this.grid[i].north) {
          ctx.moveTo((x+1)*grid_size, (y+1)*grid_size);
          ctx.lineTo(x*grid_size, (y+1)*grid_size);
        }
        if (this.grid[i].weast) {
          ctx.moveTo(x*grid_size, (y+1)*grid_size);
          ctx.lineTo(x*grid_size, y*grid_size);
        }
        ctx.stroke();
      }
    }
  }

  GameEngine.Game = Game;
  return GameEngine;
})(GameEngine || {})
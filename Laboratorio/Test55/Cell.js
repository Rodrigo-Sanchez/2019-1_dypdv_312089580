var GameEngine = (function(GameEngine) {

  class Cell {
    constructor() {
      this.north = true;
      this.south = true;
      this.east  = true;
      this.weast = true;

      this.visited = false;
    }
  }

  GameEngine.Cell = Cell;
  return GameEngine;
})(GameEngine || {})
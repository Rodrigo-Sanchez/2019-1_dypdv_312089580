var GameEngine = (function(GameEngine) {
  let cw;
  let ch;

  let KEY = GameEngine.KEY;

  class Game {
    constructor(ctx) {
      cw = ctx.canvas.width;
      ch = ctx.canvas.height;
      this.ctx = ctx;

      this.ship = new GameEngine.Ship(cw/2, ch/2, 30);

      window.addEventListener("keydown", function(evt) {
        KEY.onKeyDown(evt.keyCode);
      });
      window.addEventListener("keyup", function(evt) {
        KEY.onKeyUp(evt.keyCode);
      });
    }

    processInput() {
      this.ship.processInput();
    }

    update(elapsed) {
      this.ship.update(elapsed);

      this.checkCollisionWalls();
    }

    checkCollisionWalls() {
      if (this.ship.x < -this.ship.size/2) {
        this.ship.x = cw + this.ship.size/2;
      }
      if (this.ship.x > cw+this.ship.size/2) {
        this.ship.x = -this.ship.size/2;
      }
      if (this.ship.y < -this.ship.size/2) {
        this.ship.y = ch + this.ship.size/2;
      }
      if (this.ship.y > ch+this.ship.size/2) {
        this.ship.y = -this.ship.size/2;
      }
    }

    render() {
      this.ctx.fillStyle = "rgba(0,0,0,1)";
      this.ctx.fillRect(0, 0, cw, ch);
      this.ship.render(this.ctx);
    }
  }

  GameEngine.Game = Game;
  return GameEngine;
})(GameEngine || {})
var GameEngine = (function(GameEngine) {
  let keys = {};

  class Level1State {
    constructor(engine) {
      this.ready = false;

      this.engine = engine;
      this.scene = new BABYLON.Scene(engine);

      this.scene.gravity = new BABYLON.Vector3(0, -9.81, 0);
      this.scene.collisionsEnabled = true;
      this.scene.clearColor = new BABYLON.Color3(0.46484375, 0.6171875, 0.79296875);

      this.assetsManager = new BABYLON.AssetsManager(this.scene);
      this.assetsManager.addMeshTask("mesh_task", "", "Level1Models/", "monito.babylon");

      this.rampa = this.assetsManager.addMeshTask("rampa", "", "Level1Models/", "rampa.obj");
      this.rampa.onSuccess = function(task) {
        let loadedMeshes = task.loadedMeshes;
        for (let i=0; i<loadedMeshes.length; i++) {
          console.log(loadedMeshes[i].name)
          loadedMeshes[i].position = new BABYLON.Vector3(0, 0, -250);
          loadedMeshes[i].scaling = new BABYLON.Vector3(50, 50, 50);
        }
      }

      let self = this;
      this.assetsManager.onFinish = function () {
        document.getElementById("theCanvas").focus();
        self.init();
      }
      this.assetsManager.load();
    }

    init() {
      this.ready = true;
      document.getElementById("theCanvas").focus();

      BABYLON.Animation.AllowMatricesInterpolation = false;

      this.scene.ambientColor = new BABYLON.Color3(0.75, 0.75, 0.75);

      this.camera = new BABYLON.FollowCamera("FollowCam", new BABYLON.Vector3(0, 20, 10), this.scene);
      this.camera.radius = 300;
      this.camera.heightOffset = 100;
      this.camera.cameraAcceleration = 0.05
      this.camera.maxCameraSpeed = 10;

      // https://doc.babylonjs.com/how_to/skybox
      var skybox = BABYLON.MeshBuilder.CreateBox("SkyBox", {size:4600}, this.scene);
      var skyboxMaterial = new BABYLON.StandardMaterial("SkyBoxMat", this.scene);
      skyboxMaterial.backFaceCulling = false;
      // skyboxMaterial.reflectionTexture = new BABYLON.CubeTexture("models/skybox/skybox", this.scene);
      skyboxMaterial.reflectionTexture = new BABYLON.CubeTexture("https://www.babylonjs-playground.com/textures/TropicalSunnyDay", this.scene);
      skyboxMaterial.reflectionTexture.coordinatesMode = BABYLON.Texture.SKYBOX_MODE;
      skyboxMaterial.diffuseColor = new BABYLON.Color3(0, 0, 0);
      skyboxMaterial.specularColor = new BABYLON.Color3(0, 0, 0);
      skybox.material = skyboxMaterial;


      this.light = new BABYLON.DirectionalLight("directionalLight", 
                                                new BABYLON.Vector3(-1, -2, -1), 
                                                this.scene);
      this.light.position = new BABYLON.Vector3(0, 150, 20);


      let shadowGenerator = new BABYLON.ShadowGenerator(1024, this.light);
   
      
      this.ground = BABYLON.Mesh.CreateGround("Ground", 4600, 4600, 2, this.scene);
      this.ground.material = new BABYLON.StandardMaterial("GroundMat", this.scene);
      this.ground.material.diffuseColor = new BABYLON.Color3(0.3, 0.2, 0);
      this.ground.material.specularColor = new BABYLON.Color3(0, 0, 0);
      this.ground.receiveShadows = true;
      this.ground.checkCollisions = true;
     
      this.character = new GameEngine.Character(this.scene);
      shadowGenerator.getShadowMap().renderList.push(this.character.mesh);
      this.camera.lockedTarget = this.character.cameraTarget;

      let ramp = this.scene.getMeshByName("Rampa");
      ramp.checkCollisions = true;


      // register keyboard input
      this.scene.actionManager = new BABYLON.ActionManager(this.scene);
      this.scene.actionManager.registerAction(
        new BABYLON.ExecuteCodeAction(BABYLON.ActionManager.OnKeyDownTrigger, 
          function(evt) {
            keys[evt.sourceEvent.key] = true;
          }
        )
      );
      this.scene.actionManager.registerAction(
        new BABYLON.ExecuteCodeAction(BABYLON.ActionManager.OnKeyUpTrigger, 
          function(evt) {
            delete keys[evt.sourceEvent.key];
          }
        )
      );
    }

    processInput() {
      if (this.ready) {
        this.character.processInput(keys);
      }
    }

    update(elapsed) {
      if (this.ready) {
        this.character.update(elapsed);

        if (this.ground.intersectsMesh(this.character.mesh, false)) {
          this.character.inFloor = true;
        }
      }
    }

    render() {
      if (this.ready) {
        this.scene.render();
      }
    }
  }

  GameEngine.Level1State = Level1State;
  return GameEngine;
})(GameEngine || {})
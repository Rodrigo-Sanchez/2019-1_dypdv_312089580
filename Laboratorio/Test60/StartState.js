var GameEngine = (function(GameEngine) {
  
  class StartState {
    constructor(engine, game) {
      let self = this;

      this.engine = engine;
      this.game = game;

      this.scene = new BABYLON.Scene(engine);

      this.camera = new BABYLON.UniversalCamera("camera", new BABYLON.Vector3(0, 0, -10), this.scene);
      this.camera.setTarget(BABYLON.Vector3.Zero());

      // https://doc.babylonjs.com/how_to/gui
      this.advancedTexture = BABYLON.GUI.AdvancedDynamicTexture.CreateFullscreenUI("GUI");

      let rectPanel = new BABYLON.GUI.Rectangle();
      rectPanel.width = 0.65;
      rectPanel.height = "50px";
      rectPanel.top = 20;
      rectPanel.left = 20;
      rectPanel.cornerRadius = 0;
      rectPanel.color = "black";
      rectPanel.thickness = 0;
      rectPanel.background = "purple";
      rectPanel.verticalAlignment = BABYLON.GUI.Control.VERTICAL_ALIGNMENT_TOP;
      rectPanel.horizontalAlignment = BABYLON.GUI.Control.HORIZONTAL_ALIGNMENT_LEFT;
      this.advancedTexture.addControl(rectPanel);    

      let text1 = new BABYLON.GUI.TextBlock();
      text1.text = "Pantalla de inicio, selecciona una escena para empezar.";
      text1.color = "white";
      text1.fontSize = 24;
      rectPanel.addControl(text1);


      let button1 = BABYLON.GUI.Button.CreateSimpleButton("Button_1", "Escena 1");
      button1.width = 0.25;
      button1.height = "40px";
      button1.top = -200;
      button1.color = "white";
      button1.background = "green";
      this.advancedTexture.addControl(button1);
      button1.onPointerDownObservable.add(function(info) {
        console.log("button1:", info);
        self.changeState("scene_1");
      });

      let button2 = BABYLON.GUI.Button.CreateSimpleButton("Button_2", "Escena 2");
      button2.width = 0.25;
      button2.height = "40px";
      button2.top = -150;
      button2.color = "white";
      button2.background = "green";
      this.advancedTexture.addControl(button2);
      button2.onPointerDownObservable.add(function(info) {
        console.log("button2:", info);
        self.changeState("scene_2");
      });

      let button3 = BABYLON.GUI.Button.CreateSimpleButton("Button_3", "Escena 3");
      button3.width = 0.25;
      button3.height = "40px";
      button3.top = -100;
      button3.color = "white";
      button3.background = "green";
      this.advancedTexture.addControl(button3);
      button3.onPointerDownObservable.add(function(info) {
        console.log("button3:", info);
        self.changeState("scene_3");
      });


      let checkboxContainer = new BABYLON.GUI.Rectangle();
      this.advancedTexture.addControl(checkboxContainer);
      checkboxContainer.top = -50;
      checkboxContainer.left = 0;
      checkboxContainer.width = 0.25;
      checkboxContainer.height = "40px";
      checkboxContainer.cornerRadius = 0;
      checkboxContainer.color = "black";
      checkboxContainer.thickness = 0;
      checkboxContainer.background = "purple";

      let checkbox = new BABYLON.GUI.Checkbox();
      checkbox.width = "20px";
      checkbox.height = "20px";
      checkbox.isChecked = true;
      checkbox.color = "gray";
      checkbox.left = "-80px";
      checkbox.onIsCheckedChangedObservable.add(function(value) {
        console.log("checkbox:", value);
        self.image.isVisible = value;
      });
      let checkboxLabel = BABYLON.GUI.Control.AddHeader(checkbox, "Un checkbox", "120px", { isHorizontal: true, controlFirst: true });
      checkboxLabel.height = "30px";
      checkboxLabel.color = "white";
      checkboxContainer.addControl(checkboxLabel);


      let radioContainer = new BABYLON.GUI.Rectangle();
      this.advancedTexture.addControl(radioContainer);
      radioContainer.top = 0;
      radioContainer.left = 0;
      radioContainer.width = 0.25;
      radioContainer.height = "40px";
      radioContainer.cornerRadius = 0;
      radioContainer.color = "black";
      radioContainer.thickness = 0;
      radioContainer.background = "yellow";

      let radio1 = new BABYLON.GUI.RadioButton();
      radio1.width = "20px";
      radio1.height = "20px";
      radio1.color = "white";
      radio1.background = "green";
      let radio1Label = BABYLON.GUI.Control.AddHeader(radio1, "Op1", "100px", { isHorizontal: true, controlFirst: true });
      radio1Label.height = "30px";
      radio1Label.left = "-50px";
      radioContainer.addControl(radio1Label);
      radio1.onIsCheckedChangedObservable.add(function(state) {
        console.log("radio1:", state);
      });
      radio1.isChecked = true;

      let radio2 = new BABYLON.GUI.RadioButton();
      radio2.width = "20px";
      radio2.height = "20px";
      radio2.color = "white";
      radio2.background = "green";
      let radio2Label = BABYLON.GUI.Control.AddHeader(radio2, "Op2", "100px", { isHorizontal: true, controlFirst: true });
      radio2Label.height = "30px";
      radio2Label.left = "30px";
      radioContainer.addControl(radio2Label);
      radio2.onIsCheckedChangedObservable.add(function(state) {
        console.log("radio2:", state);
      });

      let radio3 = new BABYLON.GUI.RadioButton();
      radio3.width = "20px";
      radio3.height = "20px";
      radio3.color = "white";
      radio3.background = "green";
      let radio3Label = BABYLON.GUI.Control.AddHeader(radio3, "Op3", "100px", { isHorizontal: true, controlFirst: true });
      radio3Label.height = "30px";
      radio3Label.left = "110px";
      radioContainer.addControl(radio3Label);
      radio3.onIsCheckedChangedObservable.add(function(state) {
        console.log("radio3:", state);
      });

      this.image = new BABYLON.GUI.Image("img", "StartStateModels/startscreen.png");
      this.image.left = "300px";
      this.image.top = "170px";
      this.image.width = "300px";
      this.image.height = "300px";
      this.image.stretch = BABYLON.GUI.Image.STRETCH_UNIFORM;
      this.advancedTexture.addControl(this.image);
      this.image.isVisible = true;
    }

    changeState(state_name) {
      this.game.changeState(state_name);
    }

    processInput(keys) {
    }

    update(elapsed) {
    }

    render() {
      this.scene.render();
    }
  }

  GameEngine.StartState = StartState;
  return GameEngine;
})(GameEngine || {})
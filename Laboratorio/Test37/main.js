window.addEventListener("load", function() {
  let theCanvas = document.getElementById("theCanvas");
  let engine = new BABYLON.Engine(theCanvas, true);

  function createScene() {
    let scene = new BABYLON.Scene(engine);
    scene.clearColor = new BABYLON.Color3(1, 1, 1);
    
    let camera = new BABYLON.UniversalCamera("camera", new BABYLON.Vector3(10, 10, -10), scene);
    camera.setTarget(BABYLON.Vector3.Zero());

    let box = BABYLON.Mesh.CreateBox("Box", 4, scene);

    return scene;
  }

  let scene = createScene();
  
  engine.runRenderLoop(function() {
    scene.render();
  });
  
});
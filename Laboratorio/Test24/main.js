window.addEventListener("load", function() {
  let theCanvas = document.getElementById("theCanvas");
  let ctx = theCanvas.getContext("2d");

  let lastTime = Date.now();
  let current = 0;
  let elapsed = 0;
  let max_elapsed_wait = 30/1000;

  let game = new GameEngine.Game(ctx);

  (function gameLoop() {
    current = Date.now();
    elapsed = (current - lastTime) / 1000;

    if (elapsed > max_elapsed_wait) {
      elapsed = max_elapsed_wait;
    }

    game.processInput();
    game.update(elapsed);
    game.render();

    lastTime = current;

    window.requestAnimationFrame(gameLoop);
  })();
});
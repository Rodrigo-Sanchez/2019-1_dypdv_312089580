var GameEngine = (function(GameEngine) {
  let keys = {};

  class Level2State {
    constructor(engine) {
      this.ready = false;

      this.engine = engine;
      this.scene = new BABYLON.Scene(engine);

      this.scene.gravity = new BABYLON.Vector3(0, -0.5, 0);
      this.scene.collisionsEnabled = true;
      this.scene.clearColor = new BABYLON.Color3(0.46484375, 0.6171875, 0.79296875);

      this.assetsManager = new BABYLON.AssetsManager(this.scene);
      this.assetsManager.addMeshTask("mesh_task", "", "Level2Models/", "monito.babylon");
      this.assetsManager.addMeshTask("ground_task", "", "Level2Models/", "ground.babylon");

      let self = this;
      this.assetsManager.onFinish = function () {
        document.getElementById("theCanvas").focus();
        self.init();
      }
      this.assetsManager.load();
    }

    init() {
      this.ready = true;

      BABYLON.Animation.AllowMatricesInterpolation = false;

      this.scene.ambientColor = new BABYLON.Color3(0.5, 0.5, 0.5);

      this.camera = new BABYLON.FollowCamera("FollowCam", new BABYLON.Vector3(0, 1, 0), this.scene);
      this.camera.radius = 7;
      this.camera.heightOffset = 0.7;
      this.camera.cameraAcceleration = 0.05;
      this.camera.maxCameraSpeed = 100;

      // https://doc.babylonjs.com/how_to/skybox
      var skybox = BABYLON.MeshBuilder.CreateBox("SkyBox", {size:4600}, this.scene);
      var skyboxMaterial = new BABYLON.StandardMaterial("SkyBoxMat", this.scene);
      skyboxMaterial.backFaceCulling = false;
      // skyboxMaterial.reflectionTexture = new BABYLON.CubeTexture("models/skybox/skybox", this.scene);
      skyboxMaterial.reflectionTexture = new BABYLON.CubeTexture("https://www.babylonjs-playground.com/textures/TropicalSunnyDay", this.scene);
      skyboxMaterial.reflectionTexture.coordinatesMode = BABYLON.Texture.SKYBOX_MODE;
      skyboxMaterial.diffuseColor = new BABYLON.Color3(0, 0, 0);
      skyboxMaterial.specularColor = new BABYLON.Color3(0, 0, 0);
      skybox.material = skyboxMaterial;


      this.light = new BABYLON.DirectionalLight("directionalLight", 
                                                new BABYLON.Vector3(-1, -5, -1), 
                                                this.scene);
      this.light.position = new BABYLON.Vector3(0, 15, 20);


      let shadowGenerator = new BABYLON.ShadowGenerator(1024, this.light);
   
      this.ground = this.scene.getMeshByName("Ground");
      this.ground.receiveShadows = true;
      this.ground.checkCollisions = true;
     
      this.character = new GameEngine.Character2(this.scene);
      shadowGenerator.getShadowMap().renderList.push(this.character.mesh);
      this.camera.lockedTarget = this.character.cameraTarget;

      this.enemy = new BABYLON.MeshBuilder.CreateBox("Enemy", {size:1.9}, this.scene);
      this.enemy.position.z = -5;
      this.enemy.position.y = 6;
      this.enemy.checkCollisions = true;

      // https://doc.babylonjs.com/how_to/gui
      this.advancedTexture = BABYLON.GUI.AdvancedDynamicTexture.CreateFullscreenUI("GUI");

      let rectPanel = new BABYLON.GUI.Rectangle();
      rectPanel.width = 0.9;
      rectPanel.height = "20px";
      rectPanel.top = 20;
      rectPanel.left = 20;
      rectPanel.cornerRadius = 0;
      rectPanel.color = "black";
      rectPanel.thickness = 0;
      rectPanel.background = "purple";
      rectPanel.verticalAlignment = BABYLON.GUI.Control.VERTICAL_ALIGNMENT_TOP;
      rectPanel.horizontalAlignment = BABYLON.GUI.Control.HORIZONTAL_ALIGNMENT_LEFT;
      this.advancedTexture.addControl(rectPanel);    

      this.text1 = new BABYLON.GUI.TextBlock();
      this.text1.text = "";
      this.text1.color = "white";
      this.text1.fontSize = 12;
      rectPanel.addControl(this.text1);


      // register keyboard input
      this.scene.actionManager = new BABYLON.ActionManager(this.scene);
      this.scene.actionManager.registerAction(
        new BABYLON.ExecuteCodeAction(BABYLON.ActionManager.OnKeyDownTrigger, 
          function(evt) {
            keys[evt.sourceEvent.key] = true;
          }
        )
      );
      this.scene.actionManager.registerAction(
        new BABYLON.ExecuteCodeAction(BABYLON.ActionManager.OnKeyUpTrigger, 
          function(evt) {
            delete keys[evt.sourceEvent.key];
          }
        )
      );
    }

    processInput() {
      if (this.ready) {
        this.character.processInput(keys);
      }
    }

    update(elapsed) {
      if (this.ready) {
        this.character.update(elapsed);

        if (this.ground.intersectsMesh(this.character.mesh, false)) {
          this.character.inFloor = true;
        }

        if (this.character.state === "Atack") {
          if (this.character.weapon.intersectsMesh(this.enemy, true)) {
            this.text1.text = "enemigo golpeado";
          }
          else {
            this.text1.text = "";
          }
        }
      }
    }

    render() {
      if (this.ready) {
        this.scene.render();
      }
    }
  }

  GameEngine.Level2State = Level2State;
  return GameEngine;
})(GameEngine || {})
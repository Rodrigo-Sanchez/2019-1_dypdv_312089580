var GameEngine = (function(GameEngine) {

  class Game {
    constructor(engine) {
      this.engine = engine;
      this.state = new GameEngine.StartState(engine, this);
    }

    changeState(state_name) {
      if (state_name === "scene_1") {
        this.state.scene.detachControl();
        this.state = new GameEngine.Level1State(this.engine, this);
      }
      if (state_name === "scene_2") {
        this.state.scene.detachControl();
        this.state = new GameEngine.Level2State(this.engine, this);
      }
    }

    processInput() {
      this.state.processInput();
    }

    update(elapsed) {
      this.state.update(elapsed);
    }

    render() {
      this.state.render();
    }
  }

  GameEngine.Game = Game;
  return GameEngine;
})(GameEngine || {})
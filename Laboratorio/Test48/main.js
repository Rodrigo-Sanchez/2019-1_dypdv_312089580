window.addEventListener("load", function() {
  let theCanvas = document.getElementById("theCanvas");
  let engine = new BABYLON.Engine(theCanvas, true);

  function createScene() {
    let scene = new BABYLON.Scene(engine);
    scene.clearColor = new BABYLON.Color3(1, 1, 1);
    
    let box = BABYLON.Mesh.CreateBox("Box", 4, scene);
    let material = new BABYLON.StandardMaterial("material", scene);
    material.diffuseColor = new BABYLON.Color3(1, 0, 1);
    box.material = material;

    let light = new BABYLON.DirectionalLight("directionalLight",
                                             new BABYLON.Vector3(-0.25, -1, -0.5),
                                             scene);
    light.diffuse = new BABYLON.Color3(1, 1, 1);

    // https://doc.babylonjs.com/api/classes/babylon.arcrotatecamera
    let camera = new BABYLON.ArcRotateCamera("camera",
                                             BABYLON.Tools.ToRadians(45),
                                             BABYLON.Tools.ToRadians(45),
                                             30,
                                             box.position,
                                             scene);
    camera.attachControl(theCanvas, true);

    return scene;
  }

  let scene = createScene();
  
  engine.runRenderLoop(function() {
    scene.render();
  });
  
  window.addEventListener("resize", function() { 
    engine.resize();
  }); 
});
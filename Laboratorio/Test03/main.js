window.addEventListener("load", function init() {
	console.log("init");

	let theCanvas = document.getElementById("theCanvas");
	let ctx = theCanvas.getContext("2d");

	let cw = theCanvas.width;
	let ch = theCanvas.height;

	let pos_x = 50;
	let pos_y = 50;

	function gameLoop() {
		processInput();
		update();
		render();

		// sleep
		window.requestAnimationFrame(gameLoop);
	}
	gameLoop();

	function processInput() {}

	function update() {
		pos_x = pos_x + 1;
	}

	function render () {
		ctx.clearRect(0, 0, cw, ch);
		ctx.fillStyle = "#ff0066";
		ctx.beginPath();
		ctx.arc(pos_x, pos_y, 30, 0, 2*Math.PI);
		ctx.fill();
	}
});
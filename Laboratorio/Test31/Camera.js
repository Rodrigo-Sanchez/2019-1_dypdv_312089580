var GameEngine = (function(GameEngine) {

  class Camera {
    constructor(x, y, cw, ch) {
      this.x = x;
      this.y = y;
      this.stage_w = cw;
      this.stage_h = ch;
    }

    applyTransform(ctx) {
      ctx.save();
      ctx.translate(this.stage_w/2 -this.x, this.stage_h/2 -this.y);
    }
    releaseTransform(ctx) {
      ctx.restore();
    }

    render(ctx) {
      ctx.lineWidth = 2;
      ctx.strokeStyle = "white";
      ctx.beginPath();
      ctx.moveTo(this.x - 20, this.y);
      ctx.lineTo(this.x + 20, this.y);
      ctx.moveTo(this.x, this.y - 20);
      ctx.lineTo(this.x, this.y + 20);
      ctx.stroke();
    }
  }

  GameEngine.Camera = Camera;
  return GameEngine;
})(GameEngine || {})
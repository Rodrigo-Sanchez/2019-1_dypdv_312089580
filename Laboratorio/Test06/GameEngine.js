var GameEngine = (function(GameEngine) {
  let cw;
  let ch;

  class Game {
    constructor(ctx) {
      cw = ctx.canvas.width;
      ch = ctx.canvas.height;
      this.ctx = ctx;

      let radius = 25;
      this.ball = new GameEngine.Ball(radius+Math.random()*(cw-2*radius), radius+Math.random()*(ch-2*radius), radius);
    }

    processInput() {
    }

    update(elapsed) {
      if (this.ball.x < this.ball.size) {
        this.ball.vx *= -1;
        this.ball.x = this.ball.size;
      }
      if (this.ball.x > cw-this.ball.size) {
        this.ball.vx *= -1;
        this.ball.x = cw-this.ball.size;
      }
      if (this.ball.y < this.ball.size) {
        this.ball.vy *= -1;
        this.ball.y = this.ball.size;
      }
      if (this.ball.y > ch-this.ball.size) {
        this.ball.vy *= -1;
        this.ball.y = ch-this.ball.size;
      }

      this.ball.update(elapsed);
    }

    render() {
      this.ctx.clearRect(0, 0, cw, ch);
      this.ball.render(this.ctx);
    }
  }

  GameEngine.Game = Game;
  return GameEngine;
})(GameEngine || {})
var GameEngine = (function(GameEngine) {
  let cw;
  let ch;

  class Game {
    constructor(ctx) {
      cw = ctx.canvas.width;
      ch = ctx.canvas.height;
      this.ctx = ctx;

      let ball_size = 25;
      this.ball = new GameEngine.Ball(ball_size+Math.random()*(cw-2*ball_size), ch/2, ball_size);
    }

    processInput() {
    }

    update(elapsed) {
      if (this.ball.x < this.ball.size) {
        this.ball.vx *= this.ball.bounce;
        this.ball.x = this.ball.size;
      }
      if (this.ball.x > cw-this.ball.size) {
        this.ball.vx *= this.ball.bounce;
        this.ball.x = cw-this.ball.size;
      }
      if (this.ball.y < this.ball.size) {
        this.ball.vy *= this.ball.bounce;
        this.ball.y = this.ball.size;
      }
      if (this.ball.y > ch-this.ball.size) {
        this.ball.vy *= this.ball.bounce;
        this.ball.y = ch-this.ball.size;
      }

      this.ball.update(elapsed);
    }

    render() {
      this.ctx.clearRect(0, 0, cw, ch);
      this.ball.render(this.ctx);
    }
  }

  GameEngine.Game = Game;
  return GameEngine;
})(GameEngine || {})
var GameEngine = (function(GameEngine) {

  class Game {
    constructor(engine, scene) {
      this.engine = engine;
      this.scene = scene;

      BABYLON.Animation.AllowMatricesInterpolation = false;

      scene.ambientColor = new BABYLON.Color3(1, 1, 1);

      this.camera = new BABYLON.ArcRotateCamera("camera", -Math.PI/2, 0, 500, new BABYLON.Vector3(0, 1, 0), this.scene);
      this.camera.setTarget(BABYLON.Vector3.Zero());
      this.camera.attachControl(document.getElementById("theCanvas"), true);

      this.light = new BABYLON.DirectionalLight("directionalLight", 
                                                new BABYLON.Vector3(0, 0, 1.0), 
                                                scene);
      this.light.position = new BABYLON.Vector3(0, 0, -1);

      let skeleton = this.scene.getSkeletonByName("Armature");
      skeleton.needInitialSkinMatrix = false;

      let idle_range = skeleton.getAnimationRange("Idle");
      let walk_range = skeleton.getAnimationRange("Walk");
      let injuried_range = skeleton.getAnimationRange("Injuried");
      
      this.scene.actionManager = new BABYLON.ActionManager(this.scene);
      this.scene.actionManager.registerAction(
        new BABYLON.ExecuteCodeAction(BABYLON.ActionManager.OnKeyDownTrigger, 
          function(evt) {
            if (evt.sourceEvent.key === "0") {
              scene.stopAnimation(skeleton);
              scene.beginAnimation(skeleton, idle_range.from, idle_range.to, true);
            }
            else if (evt.sourceEvent.key === "1") {
              scene.stopAnimation(skeleton);
              scene.beginAnimation(skeleton, walk_range.from, walk_range.to, true);
            }
            else if (evt.sourceEvent.key === "2") {
              scene.stopAnimation(skeleton);
              scene.beginAnimation(skeleton, injuried_range.from, injuried_range.to, true);
            }
          }
        )
      );

      scene.beginAnimation(skeleton, idle_range.from, idle_range.to, true);

    }

    update() {
    }

    render() {
      this.scene.render();
    }
  }

  GameEngine.Game = Game;
  return GameEngine;
})(GameEngine || {})
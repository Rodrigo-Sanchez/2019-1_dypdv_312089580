window.addEventListener("load", function() {
  let theCanvas = document.getElementById("theCanvas");
  let engine = new BABYLON.Engine(theCanvas, true);

  let scene = new BABYLON.Scene(engine);
  scene.clearColor = new BABYLON.Color3(0, 0, 0);

  let assetsManager = new BABYLON.AssetsManager(scene);
  assetsManager.addMeshTask("mesh_task", "", "", "monito.babylon");

  assetsManager.onFinish = function () {
    let game = new GameEngine.Game(engine, scene);

    engine.runRenderLoop(function() {
      game.update(engine.getDeltaTime()/1000);
      game.render();
    });
  }

  assetsManager.load();


  
  let canvas_w = theCanvas.width;
  let canvas_h = theCanvas.height;
  let scale;
  window.addEventListener("resize", resize);
  function resize() {
    scale = Math.min( window.innerWidth/canvas_w, window.innerHeight/canvas_h );
    theCanvas.style.transform = "translate(" + ((window.innerWidth - scale*canvas_w)/2) + "px," + ((window.innerHeight - scale*canvas_h)/2) + "px) scale(" + scale + ")";
  }

  resize();
});
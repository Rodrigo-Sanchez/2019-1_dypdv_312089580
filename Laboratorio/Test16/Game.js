var GameEngine = (function(GameEngine) {
  let cw;
  let ch;

  let KEY = GameEngine.KEY;

  class Game {
    constructor(ctx) {
      cw = ctx.canvas.width;
      ch = ctx.canvas.height;
      this.ctx = ctx;

      this.ship = new GameEngine.Ship(cw/2, ch/2, 50);

      window.addEventListener("keydown", function(evt) {
        KEY.onKeyDown(evt.keyCode);
      });
      window.addEventListener("keyup", function(evt) {
        KEY.onKeyUp(evt.keyCode);
      });
    }

    processInput() {
      this.ship.processInput();
    }

    update(elapsed) {
      this.ship.update(elapsed);

      this.checkBorders(this.ship);
      for (let i=0, l=this.ship.weapon.bullets.length; i<l; i++) {
        if (this.ship.weapon.bullets[i].isAlive) {
          this.checkBorders(this.ship.weapon.bullets[i]);
        }
      }
    }

    checkBorders(gameObject) {
      if (gameObject.x < -gameObject.size/2) {
        gameObject.x = cw + gameObject.size/2;
      }
      if (gameObject.x > cw+gameObject.size/2) {
        gameObject.x = -gameObject.size/2;
      }
      if (gameObject.y < -gameObject.size/2) {
        gameObject.y = ch + gameObject.size/2;
      }
      if (gameObject.y > ch+gameObject.size/2) {
        gameObject.y = -gameObject.size/2;
      }
    }

    render() {
      this.ctx.fillStyle = "rgba(0,0,0,1)";
      this.ctx.fillRect(0, 0, cw, ch);
      this.ship.render(this.ctx);
    }
  }

  GameEngine.Game = Game;
  return GameEngine;
})(GameEngine || {})
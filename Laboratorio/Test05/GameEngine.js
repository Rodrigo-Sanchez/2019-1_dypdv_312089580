var GameEngine = (function(GameEngine) {
  let cw;
  let ch;
  let rect_x;
  let rect_y;
  let rect_w;
  let rect_h;

  let text_keycode = "";

  let Key = {
    _pressed : {},
    
    LEFT: 37,
    UP: 38,
    RIGHT: 39,
    DOWN: 40,

    isPress: function(keyCode) {
      return this._pressed[keyCode];
    },
    onKeyDown: function(keyCode) {
      this._pressed[keyCode] = true;
    },
    onKeyUp: function(keyCode) {
      delete this._pressed[keyCode];
    }
  }

  class Game {
    constructor(ctx) {
      cw = ctx.canvas.width;
      ch = ctx.canvas.height;
      this.ctx = ctx;

      rect_x = cw/2;
      rect_y = ch/2;
      rect_w = 30;
      rect_h = 90;

      window.addEventListener("keydown", function(evt) {
        Key.onKeyDown(evt.keyCode);
        text_keycode = "keyCode = " + evt.keyCode;
      });
      window.addEventListener("keyup", function(evt) {
        Key.onKeyUp(evt.keyCode);
      });
    }

    processInput() {
      if (Key.isPress(Key.LEFT)) {
        rect_x -= 5;
      }
      if (Key.isPress(Key.RIGHT)) {
        rect_x += 5;
      }
      if (Key.isPress(Key.UP)) {
        rect_y -= 5;
      }
      if (Key.isPress(Key.DOWN)) {
        rect_y += 5;
      }
    }

    update(elapsed) {
      if (rect_x < -rect_w/2) {
        rect_x = cw + rect_w/2;
      }
      if (rect_x > cw+rect_w/2) {
        rect_x = -rect_w/2;
      }
      if (rect_y < -rect_h/2) {
        rect_y = ch + rect_h/2;
      }
      if (rect_y > ch+rect_h/2) {
        rect_y = -rect_h/2;
      }
    }

    render() {
      this.ctx.clearRect(0, 0, cw, ch);

      this.ctx.fillStyle = "blue";
      this.ctx.beginPath();
      this.ctx.fillRect(rect_x - rect_w/2, rect_y-rect_h/2, rect_w, rect_h);

      this.ctx.fillStyle = "yellow";
      this.ctx.strokeStyle = "black";
      this.ctx.lineWidth = 5;
      this.ctx.lineJoin = "round";
      this.ctx.font = "bold 23px Monospace";
      this.ctx.beginPath();
      this.ctx.strokeText(text_keycode, 10, 25);
      this.ctx.fillText(text_keycode, 10, 25);
    }
  }

  GameEngine.Game = Game;
  return GameEngine;
})(GameEngine || {})
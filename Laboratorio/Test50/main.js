window.addEventListener("load", function() {
  let theCanvas = document.getElementById("theCanvas");
  let engine = new BABYLON.Engine(theCanvas, true);

  function createScene() {
    let scene = new BABYLON.Scene(engine);
    scene.clearColor = new BABYLON.Color3(1, 1, 1);
    
    let box = BABYLON.Mesh.CreateBox("Box", 4, scene);

    let material = new BABYLON.StandardMaterial("material", scene);
    material.diffuseColor = new BABYLON.Color3(0.7, 0.3, .8);
    box.material = material;

    let light = new BABYLON.DirectionalLight("directionalLight",
                                             new BABYLON.Vector3(-0.5, -1, -0.25),
                                             scene);
    light.diffuse = new BABYLON.Color3(1, 1, 1);

    // https://doc.babylonjs.com/api/classes/babylon.anaglypharcrotatecamera
    var camera = new BABYLON.AnaglyphArcRotateCamera("camera",
                                                      BABYLON.Tools.ToRadians(45),
                                                      BABYLON.Tools.ToRadians(45),
                                                      30,
                                                      box.position,
                                                      0.33,
                                                      scene);
    camera.attachControl(theCanvas, true);

    return scene;
  }

  let scene = createScene();
  
  engine.runRenderLoop(function() {
    scene.render();
  });
  
  window.addEventListener("resize", function() { 
    engine.resize();
  }); 
});
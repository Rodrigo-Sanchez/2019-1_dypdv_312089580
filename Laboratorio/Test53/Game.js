var GameEngine = (function(GameEngine) {
  let keys = {};
  let cw = 800;
  let ch = 600;
  let aPool;
  let bPool;

  class Game {
    constructor(engine, scene) {
      this.engine = engine;
      this.scene = scene;

      scene.ambientColor = new BABYLON.Color3(0.2, 0.2, 0.2);

      this.camera = new BABYLON.UniversalCamera("camera", new BABYLON.Vector3(0, 0, -710), this.scene);
      this.camera.setTarget(BABYLON.Vector3.Zero());
      
   
      this.light = new BABYLON.DirectionalLight("directionalLight", 
                                                new BABYLON.Vector3(0, 0, 1.0), 
                                                scene);
      this.light.position = new BABYLON.Vector3(0, 0, -1);
    
      this.background = new BABYLON.Mesh.CreatePlane("background", 1, this.scene);
      this.background.material = new BABYLON.StandardMaterial("background_material", this.scene);
      this.background.material.diffuseColor = new BABYLON.Color3(0, 0.25, 0);
      this.background.material.specularColor = new BABYLON.Color3(0, 0, 0);
      this.background.scaling.x = cw;
      this.background.scaling.y = ch;
      this.background.position.z = 10;

      this.ship = new GameEngine.Ship(this.scene, "ship", cw, ch, 0, 0, 30);
      this.asteroidPool = new GameEngine.AsteroidPool(this.scene, "asteroids", cw, ch);
      this.asteroidPool.addAsteroid();

      this.game_initialized = false;

      // GUI
      this.init_text = new BABYLON.GUI.TextBlock();
      this.init_text.top = "20%";
      this.init_text.text = "Presiona espacio para comenzar";
      this.init_text.color = "white";
      this.init_text.resizeToFit = true;
      this.init_text.fontSize = "5%";

      var GUITexture = BABYLON.GUI.AdvancedDynamicTexture.CreateFullscreenUI("UI");
      GUITexture.addControl(this.init_text);
      
      // register keyboard input
      this.scene.actionManager = new BABYLON.ActionManager(this.scene);
      this.scene.actionManager.registerAction(
        new BABYLON.ExecuteCodeAction(BABYLON.ActionManager.OnKeyDownTrigger, 
          function(evt) {
            keys[evt.sourceEvent.key] = true;
          }
        )
      );
      this.scene.actionManager.registerAction(
        new BABYLON.ExecuteCodeAction(BABYLON.ActionManager.OnKeyUpTrigger, 
          function(evt) {
            delete keys[evt.sourceEvent.key];
          }
        )
      );

    }

    processInput() {
      this.ship.processInput(keys);
      this.space_pressed = keys[" "];
    }

    update(elapsed) {
      if ((this.game_initialized === false) && this.space_pressed) {
        this.game_initialized = true;
      }

      if (this.game_initialized) {
        this.init_text.isVisible = false;
        this.ship.update(elapsed);
        this.asteroidPool.update(elapsed);

        aPool = this.asteroidPool.asteroids;
        bPool = this.ship.weapon.bullets;

        for (let i=0; i<aPool.length; i++) {
          if (aPool[i].isAlive) {
            this.checkCircleCollision(this.ship, aPool[i], "nave-asteroide");
          }
        }
  
        for (let bullet_i=0; bullet_i<bPool.length; bullet_i++) {
          if (bPool[bullet_i].isAlive) {
            for (let asteroid_i=0; asteroid_i<aPool.length; asteroid_i++) {
              if ( (aPool[asteroid_i].isAlive) && (this.checkCircleCollision(bPool[bullet_i], aPool[asteroid_i], "bala-asteroide")) ) {
                bPool[bullet_i].setAlive(false);
                this.asteroidPool.split(asteroid_i);
              }
            }
          }
        }
      }
    }

    checkCircleCollision(obj1, obj2, tmpmsg) {
      let dist = Math.sqrt( (obj1.x - obj2.x)*(obj1.x - obj2.x) + (obj1.y - obj2.y)*(obj1.y - obj2.y) );
      if (dist < obj1.radius + obj2.radius) {
        console.log("colision", tmpmsg);
        return true;
      }
      return false;
    }

    render() {
      this.scene.render();
    }
  }

  GameEngine.Game = Game;
  return GameEngine;
})(GameEngine || {})
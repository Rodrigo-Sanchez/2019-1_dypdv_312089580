var GameEngine = (function(GameEngine) {
  const PI_180 = Math.PI/180;

  class Ship {
    constructor(scene, id, cw, ch, x, y, size) {
      this.cw = cw;
      this.ch = ch;
      this.init_x = x;
      this.init_y = y;

      this.size = size;
      this.radius = this.size;

      this.mesh = scene.getMeshByName("SpaceShip");
      this.mesh.isVisible = true;
      this.mesh.scaling.x = this.mesh.scaling.y = this.mesh.scaling.z = size/2;
      this.mesh.material.ambientColor = new BABYLON.Color3(1, 1, 1);

      this.x = 0;
      this.y = 0;
      this.vx = 0;
      this.vy = 0;
      this.ax = 0;
      this.ay = 0;
      this.rotation = 0;
      this.trust = 0;
      this.friction = 0.95;
      this.shooting = false;
      this.weapon = new GameEngine.Weapon(scene, id, 20, 0.25);
      
      this.init();
    }

    init() {
      this.mesh.position.x = this.init_x;
      this.mesh.position.y = this.init_y;
    }

    processInput(keys) {
      this.vr = 0;
      this.trust = 0;
      this.showFlame = false;
      this.shooting = false;

      if (keys["ArrowLeft"]) {
        this.vr = 2;
      }
      if (keys["ArrowRight"]) {
        this.vr = -2;
      }
      if (keys["ArrowUp"]) {
        this.showFlame = true;
        this.trust = 10;
      }
      if (keys["ArrowDown"]) {
        this.trust = -2;
      }
      if (keys["z"] || keys["Z"]) {
        this.shooting = true;
      }
    }

    update(elapsed) {
      this.rotation += this.vr * PI_180;
      this.ax = Math.cos(this.rotation) * this.trust;
      this.ay = Math.sin(this.rotation) * this.trust;
      this.vx += this.ax;
      this.vy += this.ay;

      this.vx *= this.friction;
      this.vy *= this.friction;

      this.x += this.vx * elapsed;
      this.y += this.vy * elapsed;

      this.checkBorders();


      this.mesh.rotation.z = this.rotation;

      this.mesh.position.x = this.x;
      this.mesh.position.y = this.y;
      
      this.weapon.auxDelayTime += elapsed;
      if (this.shooting) {
        if (this.weapon.delayActivation < this.weapon.auxDelayTime) {
          // this.weapon.shot(this.x, this.y, 5, -22, this.vx, this.vy, this.rotation);
          // this.weapon.shot(this.x, this.y, 5,  22, this.vx, this.vy, this.rotation);
          this.weapon.shot(this.x, this.y, 0,  0, this.vx, this.vy, this.rotation);
          this.weapon.auxDelayTime = 0;
        }
      }
      this.weapon.update(elapsed);
    }

    checkBorders() {
      if (this.x -this.size/2 > this.cw/2) {
        this.x = -this.cw/2 -this.size/2;
      }
      if (this.x +this.size/2 < -this.cw/2) {
        this.x = this.cw/2 +this.size/2;
      }
      if (this.y -this.size/2 > this.ch/2) {
        this.y = -this.ch/2 -this.size/2;
      }
      if (this.y +this.size/2 < -this.ch/2) {
        this.y = this.ch/2 +this.size/2;
      }
    }
  }

  GameEngine.Ship = Ship;
  return GameEngine;
})(GameEngine || {})
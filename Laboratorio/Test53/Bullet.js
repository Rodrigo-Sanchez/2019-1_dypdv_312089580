var GameEngine = (function(GameEngine) {
  let bulletMesh = null;

  class Bullet {
    constructor(scene, id, speed=300, activeTime=2) {
      this.isAlive = false;
      this.speed = speed;
      this.activeTime = activeTime;
      this.auxActiveTime = 0;
      this.x = 0;
      this.y = 0;
      this.size = 5;
      this.radius = this.size;

      if ((bulletMesh === null) || (bulletMesh && (bulletMesh.getScene() != scene))) {
        bulletMesh = BABYLON.Mesh.CreateBox("bulletMesh", this.size, scene);
        bulletMesh.material = new BABYLON.StandardMaterial("bulletMesh_material", scene);
        bulletMesh.material.diffuseColor = new BABYLON.Color3(0, 0, 1);
        bulletMesh.isVisible = false;
      }

      this.mesh = bulletMesh.createInstance(id);

      //////
      // this.mesh = bulletMesh.clone();
      // this.mesh.material = new BABYLON.StandardMaterial(id + "_material", scene);
      // this.mesh.material.diffuseColor = new BABYLON.Color3(0, 0, 1);
      //////

      this.mesh.isVisible = this.isAlive;
    }

    activate(ox, oy, x, y, vx, vy, rotation) {
      this.x = ox + x * Math.cos(rotation) - y * Math.sin(rotation);
      this.y = oy + y * Math.cos(rotation) + x * Math.sin(rotation);

      this.vx = vx + this.speed * Math.cos(rotation);
      this.vy = vy + this.speed * Math.sin(rotation);
      this.isAlive = true;

      this.mesh.isVisible = this.isAlive;
      this.mesh.rotation.z = rotation;
    }

    setAlive(alive) {
      this.mesh.isVisible = this.isAlive = alive;
    }

    update(elapse) {
      this.x += this.vx * elapse;
      this.y += this.vy * elapse;

      this.mesh.position.x = this.x;
      this.mesh.position.y = this.y;

      this.auxActiveTime += elapse;
      if (this.auxActiveTime > this.activeTime) {
        this.isAlive = false;
        this.auxActiveTime = 0;
        this.mesh.isVisible = this.isAlive;
      }
    }
  }

  GameEngine.Bullet = Bullet;
  return GameEngine;
})(GameEngine || {})
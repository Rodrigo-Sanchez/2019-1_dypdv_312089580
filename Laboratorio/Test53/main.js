window.addEventListener("load", function() {
  let theCanvas = document.getElementById("theCanvas");
  let engine = new BABYLON.Engine(theCanvas, true);

  let scene = new BABYLON.Scene(engine);
  scene.clearColor = new BABYLON.Color3(0, 0, 0);

  let assetsManager = new BABYLON.AssetsManager(scene);
  assetsManager.addMeshTask("spaceship_task", "", "", "spaceship.babylon");
  assetsManager.addMeshTask("spaceship_task", "", "", "asteroid.babylon");
  assetsManager.onTaskSuccess = function(task) {
    task.loadedMeshes[0].isVisible = false;
  }
  // assetsManager.onTaskError = function(task) {
  //   console.log("Error")
  // }

  assetsManager.onFinish = function () {
    let asteroidGame = new GameEngine.Game(engine, scene);
    
    engine.runRenderLoop(function() {
      asteroidGame.processInput();
      asteroidGame.update(engine.getDeltaTime()/1000);
      asteroidGame.render();
    });
  }

  assetsManager.load();


  
  let canvas_w = theCanvas.width;
  let canvas_h = theCanvas.height;
  let scale;
  window.addEventListener("resize", resize);
  function resize() {
    scale = Math.min( window.innerWidth/canvas_w, window.innerHeight/canvas_h );
    theCanvas.style.transform = "translate(" + ((window.innerWidth - scale*canvas_w)/2) + "px," + ((window.innerHeight - scale*canvas_h)/2) + "px) scale(" + scale + ")";
  }

  resize();
});
window.addEventListener("load", function() {
  let theCanvas = document.getElementById("theCanvas");
  let engine = new BABYLON.Engine(theCanvas, true);

  function createScene() {
    let scene = new BABYLON.Scene(engine);
    scene.clearColor = new BABYLON.Color3(1, 1, 1);
    
    let camera = new BABYLON.UniversalCamera("camera", new BABYLON.Vector3(5, 10, -10), scene);
    camera.setTarget(BABYLON.Vector3.Zero());
    camera.attachControl(theCanvas, true);

    let box = BABYLON.Mesh.CreateBox("Box", 4, scene);

    let material = new BABYLON.StandardMaterial("material", scene);
    material.wireframe = true;
    box.material = material;

    return scene;
  }

  let scene = createScene();
  
  engine.runRenderLoop(function() {
    scene.render();
  });
  
  window.addEventListener("resize", function() { 
    engine.resize();
  }); 
});
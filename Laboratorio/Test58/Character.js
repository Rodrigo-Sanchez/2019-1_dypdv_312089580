var GameEngine = (function(GameEngine) {
  const PI_180 = Math.PI/180;

  class Character {
    constructor(scene) {
      this.scene = scene;
      this.mesh = scene.getMeshByName("Monito");
      this.mesh.checkCollisions = true;
      this.mesh.ellipsoid = new BABYLON.Vector3(35, 90, 20);
      this.mesh.ellipsoidOffset = new BABYLON.Vector3(0, 90, 0);
      this.mesh.material.diffuseColor = new BABYLON.Color3(0.5, 0.5, 0.5);

      // let debug_ellipsoid = BABYLON.MeshBuilder.CreateSphere("tmp_sphere", {diameterX: 70, diameterY: 180, diameterZ: 40}, scene);
      // debug_ellipsoid.position.y = 90;
      // debug_ellipsoid.parent = this.mesh;
      // debug_ellipsoid.visibility = 0.25;

      this.cameraTarget = BABYLON.MeshBuilder.CreateBox("CameraTarget", {width: 10, height: 10, depth: 10}, scene);
      this.cameraTarget.visibility = 0;
      this.cameraTarget.parent = this.mesh;
      this.cameraTarget.position.y = 150;

      this.skeleton = scene.getSkeletonByName("Armature");
      this.skeleton.needInitialSkinMatrix = false;

      this.idle_range = this.skeleton.getAnimationRange("Idle");
      this.walk_range = this.skeleton.getAnimationRange("Walk");
      
      this.state = "Idle";

      scene.beginAnimation(this.skeleton, this.idle_range.from, this.idle_range.to, true);

      this.gravity = new BABYLON.Vector3();
      this.velocity = new BABYLON.Vector3();
      this.vr = 0;
      this.rotation = -Math.PI/2;
      this.speed = 0;
      this.jump_heigth = 6;
      this.canJump = true;
      this.inFloor = false;
    }

    processInput(keys) {
      this.vr = 0;

      if (keys["ArrowUp"]) {
        if (this.state === "Idle") {
          this.scene.stopAnimation(this.skeleton);
          this.scene.beginAnimation(this.skeleton, this.walk_range.from, this.walk_range.to, true);
          this.state = "Walk";
        }
        this.speed = 150;
      }
      else {
        if (this.state === "Walk") {
          this.scene.stopAnimation(this.skeleton);
          this.scene.beginAnimation(this.skeleton, this.idle_range.from, this.idle_range.to, true);
          this.state = "Idle";
        }
        this.speed = 0;
        this.velocity.x = 0;
        this.velocity.z = 0;
      }

      if (keys["ArrowLeft"]) {
        this.vr = -2;
        this.velocity.x = 0;
        this.velocity.z = 0;
      }
      if (keys["ArrowRight"]) {
        this.vr = 2;
        this.velocity.x = 0;
        this.velocity.z = 0;
      }

      if (!keys["z"] && !keys["Z"]) {
        this.canJump = true;
      }
      if (this.canJump && this.inFloor && (keys["z"] || keys["Z"])) {
        this.velocity.y = this.jump_heigth;
        this.canJump = false;
      }
    }

    update(elapsed) {
      this.rotation += this.vr * PI_180;
      this.velocity.x = -Math.cos(this.rotation) * this.speed * elapsed;
      this.velocity.y += this.scene.gravity.y * elapsed;
      this.velocity.z =  Math.sin(this.rotation) * this.speed * elapsed;

      this.mesh.rotation.y = this.rotation +Math.PI/2;
      // this.cameraTarget.rotation.y = this.mesh.rotation.y;

      this.mesh.moveWithCollisions(this.velocity);

      if (this.inFloor) {
        this.velocity.y = Math.max(this.scene.gravity.y, this.velocity.y);
      }

      this.inFloor = false;
    }

  }

  GameEngine.Character = Character;
  return GameEngine;
})(GameEngine || {})
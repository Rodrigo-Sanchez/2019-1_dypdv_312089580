window.addEventListener("load", function() {
  let theCanvas = document.getElementById("theCanvas");
  let engine = new BABYLON.Engine(theCanvas, true);

  let scene = new BABYLON.Scene(engine);
  scene.clearColor = new BABYLON.Color3(0.46484375, 0.6171875, 0.79296875);

  let assetsManager = new BABYLON.AssetsManager(scene);
  assetsManager.addMeshTask("mesh_task", "", "models/", "monito.babylon");

  let rampa = assetsManager.addMeshTask("rampa", "", "models/", "rampa.obj");
  rampa.onSuccess = function(task) {
    let loadedMeshes = task.loadedMeshes;
    for (let i=0; i<loadedMeshes.length; i++) {
      console.log(loadedMeshes[i].name)
      loadedMeshes[i].position = new BABYLON.Vector3(0, 0, -250);
      loadedMeshes[i].scaling = new BABYLON.Vector3(50, 50, 50);
    }
  }

  assetsManager.onFinish = function () {
    let game = new GameEngine.Game(engine, scene);
    theCanvas.focus();

    engine.runRenderLoop(function() {
      game.processInput();
      game.update(engine.getDeltaTime()/1000);
      game.render();
    });
  }

  assetsManager.load();


  
  let canvas_w = theCanvas.width;
  let canvas_h = theCanvas.height;
  let scale;
  window.addEventListener("resize", resize);
  function resize() {
    scale = Math.min( window.innerWidth/canvas_w, window.innerHeight/canvas_h );
    theCanvas.style.transform = "translate(" + ((window.innerWidth - scale*canvas_w)/2) + "px," + ((window.innerHeight - scale*canvas_h)/2) + "px) scale(" + scale + ")";
  }

  resize();
});
var GameEngine = (function(GameEngine) {
  let cw;
  let ch;

  let KEY = GameEngine.KEY;

  class Game {
    constructor(ctx) {
      cw = ctx.canvas.width;
      ch = ctx.canvas.height;
      this.ctx = ctx;

      let ball_size = 25;
      this.ball = new GameEngine.Ship(cw/2, ch-ball_size, ball_size);

      window.addEventListener("keydown", function(evt) {
        KEY.onKeyDown(evt.keyCode);
      });
      window.addEventListener("keyup", function(evt) {
        KEY.onKeyUp(evt.keyCode);
      });
    }

    processInput() {
      this.ball.processInput();
    }

    update(elapsed) {
      this.ball.update(elapsed);

      this.checkCollisionWalls();
    }

    checkCollisionWalls() {
      if (this.ball.x < this.ball.size) {
        this.ball.x = this.ball.size;
      }
      if (this.ball.x > cw-this.ball.size) {
        this.ball.x = cw-this.ball.size;
      }
      if (this.ball.y < this.ball.size) {
        this.ball.y = this.ball.size;
        this.ball.inFloor = true;
      }
      if (this.ball.y > ch-this.ball.size) {
        this.ball.y = ch-this.ball.size;
        this.ball.inFloor = true;
      }
    }

    render() {
      this.ctx.clearRect(0, 0, cw, ch);
      this.ball.render(this.ctx);
    }
  }

  GameEngine.Game = Game;
  return GameEngine;
})(GameEngine || {})

var GameEngine = (function(GameEngine) {
  let PI2 = 2*Math.PI;
  let gravityValue = 10
  let gravity = gravityValue;

  let KEY = GameEngine.KEY;

  class Ship {
    constructor(x, y, size) {
      this.x = x;
      this.y = y;
      this.size = size;
      this.jump_heigth = 500;
      this.inFloor = false;
      this.upDirection = 1;

      this.speed = 100;
      this.vx = 0;
      this.vy = 0;
    }

    processInput() {
      if ((KEY.isPress(KEY.SPACE)) && (this.inFloor)) {
        this.vy = -this.upDirection*this.jump_heigth;
        this.inFloor = false;
      }
      if (KEY.isPress(KEY.LEFT)) {
        this.vx = -this.speed;
      }
      if (KEY.isPress(KEY.RIGHT)) {
        this.vx = this.speed;
      }
      if (!KEY.isPress(KEY.LEFT) && !KEY.isPress(KEY.RIGHT)) {
        this.vx = 0;
      }
      if (KEY.isPress(KEY.UP)) {
        this.upDirection = -1;
        this.vy = this.upDirection * 200;
      }
      if (KEY.isPress(KEY.DOWN)) {
        this.upDirection = 1;
        this.vy = this.upDirection * 200;
      }
    }

    update(elapsed) {
      this.vy += gravity*this.upDirection;
      this.x += this.vx * elapsed;
      this.y += this.vy * elapsed;
    }
    
    render(ctx) {
      ctx.fillStyle = "#0000ff";
      ctx.beginPath();
      ctx.fillRect(this.x - this.size, this.y - this.size, 2*this.size, 2*this.size);
    }
  }

  GameEngine.Ship = Ship;
  return GameEngine;
})(GameEngine || {})
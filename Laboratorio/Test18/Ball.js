var GameEngine = (function(GameEngine) {
  let PI2 = 2*Math.PI;
  let gravity = 10;

  let KEY = GameEngine.KEY;

  class Ball {
    constructor(x, y, size) {
      this.x = x;
      this.y = y;
      this.size = size;
      this.jump_heigth = 500;
      this.inFloor = false;

      this.speed = 100;
      this.vx = 0;
      this.vy = 0;
    }

    processInput() {
      if ((KEY.isPress(KEY.SPACE)) && (this.inFloor)) {
        this.vy = -this.jump_heigth;
        this.inFloor = false;
      }
      if (KEY.isPress(KEY.LEFT)) {
        this.vx = -this.speed;
      }
      if (KEY.isPress(KEY.RIGHT)) {
        this.vx = this.speed;
      }
      if (!KEY.isPress(KEY.LEFT) && !KEY.isPress(KEY.RIGHT)) {
        this.vx = 0;
      }
    }

    update(elapsed) {
      this.vy += gravity;
      this.x += this.vx * elapsed;
      this.y += this.vy * elapsed;
    }
    
    render(ctx) {
      ctx.fillStyle = "#0000ff";
      ctx.beginPath();
      ctx.fillRect(this.x - this.size, this.y - this.size, 2*this.size, 2*this.size);
    }
  }

  GameEngine.Ball = Ball;
  return GameEngine;
})(GameEngine || {})
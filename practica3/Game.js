var GameEngine =(function(GameEngine) {
  let cx;
  let cy;
  let aPool;
  let bPool;
  let KEY = GameEngine.KEY;
  let game_state = false;

  class Game {
    constructor(ctx) {
      cx = ctx.canvas.width;
      cy = ctx.canvas.height;
      this.ctx = ctx;

      this.ship = new GameEngine.Ship(cx/2, cy/2, 50);
      this.asteroidPool = new GameEngine.AsteroidPool(cx, cy, 20);
      this.asteroidPool.addAsteroid();
      this.powerUp = new GameEngine.PowerUp(cx, cy, 25);
      this.score = new GameEngine.Score(7*cx/10, cy/12, 0);
      this.life = new GameEngine.Life(0, cy/12, 3);
      this.play = new GameEngine.Play(cx/4, cy/2, "Presiona espacio para comenzar");

      window.addEventListener("keydown", function(evt) {
        KEY.onKeyDown(evt.keyCode);
      });
      window.addEventListener("keyup", function(evt) {
        KEY.onKeyUp(evt.keyCode);
      });
    }

    processInput() {
      if(KEY.isPress(KEY.SPACE)) {
        game_state = true;
        this.play.text_play = "";
        this.life.text_life = 3;
        this.score.text_score = 0;
      } 
      if(this.life.text_life == 0) {
        this.play.x = cx/6;
        this.play.text_play = "Presiona espacio para volver a jugar";
      }
      if(game_state)
        this.ship.processInput(); 
    }

    update(elapsed) {
      if(game_state && this.life.text_life > 0) {
        aPool = this.asteroidPool.asteroids;
        bPool = this.ship.weapon.bullets;
        
        this.ship.update(elapsed);
        this.asteroidPool.update(elapsed);
        if(this.powerUp.isAlive)
          this.powerUp.update(elapsed);

        this.checkBorders(this.ship);
        this.checkBorders(this.powerUp);

        for(let i=0; i<aPool.length; i++) {
          if(aPool[i].isAlive) {
            this.checkBorders(aPool[i]);
          }
        }

        for(let i=0; i<bPool.length; i++) {
          if(bPool[i].isAlive) {
            this.checkBorders(bPool[i]);
          }
        }

        if(this.checkCircleCollision(this.ship, this.powerUp, "nave-powerup")) {
          this.ship.invincible = true;
          this.powerUp.isAlive = false;
        }

        if(!this.ship.invincible) {
          for(let i=0; i<aPool.length; i++)
            if(this.checkCircleCollision(this.ship, aPool[i], "nave-asteroide")) {
              this.life.text_life--;
              this.ship.x = cx/2;
              this.ship.y = cy/2;
            }
        }

        for(let bullet_i=0; bullet_i<bPool.length; bullet_i++) {
          if(bPool[bullet_i].isAlive) {
            for(let asteroid_i=0; asteroid_i<aPool.length; asteroid_i++) {
              if((aPool[asteroid_i].isAlive)&&(this.checkCircleCollision(bPool[bullet_i], aPool[asteroid_i], "bala-asteroide"))) {
                bPool[bullet_i].isAlive = false;
                this.asteroidPool.split(asteroid_i);
                this.score.text_score += aPool[asteroid_i].value;
              }
            }
          }
        }
      }
    }

    checkCircleCollision(obj1, obj2, tmpmsg) {
      let dist = Math.sqrt((obj1.x - obj2.x)*(obj1.x - obj2.x) +(obj1.y - obj2.y)*(obj1.y - obj2.y));
      if(dist < obj1.radius + obj2.radius) {
        console.log("colision", tmpmsg);
        return true;
      }
      return false;
    }

    checkBorders(gameObject) {
      if(gameObject.x < -gameObject.radius)
        gameObject.x = cx + gameObject.radius;
      if(gameObject.x > cx+gameObject.radius)
        gameObject.x = -gameObject.radius;
      if(gameObject.y < -gameObject.radius)
        gameObject.y = cy + gameObject.radius;
      if(gameObject.y > cy+gameObject.radius)
        gameObject.y = -gameObject.radius;
    }

    render() {
      this.ctx.fillStyle = "rgba(0,0,0,1)";
      this.ctx.fillRect(0, 0, cx, cy);
      if(this.powerUp.isAlive)
        this.powerUp.render(this.ctx);
      this.asteroidPool.render(this.ctx);
      this.ship.render(this.ctx);
      this.score.render(this.ctx);
      this.life.render(this.ctx);
      this.play.render(this.ctx);
    }
  }

  GameEngine.Game = Game;
  return GameEngine;
})(GameEngine || {})
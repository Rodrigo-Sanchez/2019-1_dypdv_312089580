var GameEngine = (function(GameEngine) {

    class PowerUp extends GameEngine.Sprite {
        constructor(x, y, size) {
            super(x, y, size, size, "sprites/power_up.svg");

            this.isAlive = true;
            this.x = x;
            this.y = y;
            this.speed = 100;
            let angle = (360 * Math.random()) * Math.PI/180;
            this.vx = Math.cos(angle) * this.speed;
            this.vy = Math.sin(angle) * this.speed;

            this.size = size;
            this.radius = size/2;
        }
  
        update(elapsed) {
            this.x += this.vx*elapsed;
            this.y += this.vy*elapsed;
        }
    
        render(ctx) {
            /*ctx.fillStyle = "purple";
            ctx.beginPath();
            ctx.arc(this.x, this.y, this.radius, 0, 2*Math.PI);
            ctx.fill();*/
            super.render(ctx);
        }
    }
  
    GameEngine.PowerUp = PowerUp;
    return GameEngine;
  })(GameEngine || {})
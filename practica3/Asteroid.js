var GameEngine = (function(GameEngine) {
  const PI2 = 2*Math.PI;

  class Asteroid extends GameEngine.Sprite {
    constructor(x, y, size) {
      super(x, y, 2*size, 2*size, "sprites/asteroid.svg");

      this.isAlive = false;
      this.x = x;
      this.y = y;
      this.value;
      this.n = 1;
      this.speed = 100;
      
      let angle = (360 * Math.random()) * Math.PI/180;
      this.vx = Math.cos(angle) * this.speed;
      this.vy = Math.sin(angle) * this.speed;
      this.size = this.radius = size;
      this.hp = 3;
    }

    hit() {
      switch(this.hp) {
        case 3:
          this.value = 20;
          break;
        case 2:
          this.value = 50;
          break;
        case 1:
          this.value = 100;        
          break;
      }
      this.hp--;
      if(this.hp > 0) {
        this.radius = this.size = this.size/2;
        this.n-=0.25;
      } else {
        this.isAlive = false;
      }
    }

    activate(x, y) {
      this.x = x;
      this.y = y;
      this.isAlive = true;
    }

    update(elapsed) {
      this.x += this.vx*elapsed;
      this.y += this.vy*elapsed;
    }

    render(ctx) {
      if(this.isAlive) {
        /*ctx.fillStyle = "pink";
        ctx.beginPath();
        ctx.arc(this.x, this.y, this.radius, 0, 2*Math.PI);
        ctx.fill();*/
        super.renderSize(ctx, this.n);
      }
    }
  }

  GameEngine.Asteroid = Asteroid;
  return GameEngine;
})(GameEngine || {})
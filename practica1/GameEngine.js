var GameEngine = (function(GameEngine) {
    let canvas_x;             // Tamaño del canvas en el eje x.
    let canvas_y;             // Tamaño del canvas en el eje y.
    let paddle_x = 10;        // Tamaño en el eje x de la paleta.
    let paddle_y = 100;       // Tamaño en el eje y de la paleta.
    let player_1;             // Debería ser a la mitad de la altura menos
    let player_2;             // la mitad de la altura del jugador.
    let score_p1 = 0;         // Puntuación del player_1
    let score_p2 = 0;         // Puntuación del player_2
    let start_text = "Presiona espacio para comenzar.";
    let again_text = "Presiona espacio para jugar de nuevo.";
    let winner_text = "";
    let game_state = false;
    let Key = {
        _pressed : {},
        UP_P1: 87,
        DOWN_P1: 83,
        UP_P2: 38,
        DOWN_P2: 40,
        SPACE_BAR: 32,
        isPress: function(keyCode) {
          return this._pressed[keyCode];
        },
        onKeyDown: function(keyCode) {
          this._pressed[keyCode] = true;
        },
        onKeyUp: function(keyCode) {
          delete this._pressed[keyCode];
        }
    };
    
    /**
    * Regresa un número aleatorio entre min (inclusivo) y max (exclusivo).
    */
    function getRandomArbitrary(min, max) {
        return Math.random()*(max-min)+min;
    }

    /**
    * Regresa un número aleatorio entre los ángulos válidos.
    */
    function validAngle() {
        return Math.random() > 0.5 
                    ? getRandomArbitrary(135, 226)*Math.PI/180
                    : Math.random() > 0.5
                        ? getRandomArbitrary(0, 46)*Math.PI/180
                        : getRandomArbitrary(315, 361)*Math.PI/180;
    }

    class Game {
        constructor(ctx) {
            canvas_x = ctx.canvas.width;
            canvas_y = ctx.canvas.height;
            this.ctx = ctx;

            let radius = 10;
            this.ball = new GameEngine.Ball(canvas_x/2, canvas_y/2, radius);
            this.ball.x = canvas_x/2;
            this.ball.y = canvas_y/2;
            player_1 = canvas_y/2-paddle_y/2;
            player_2 = canvas_y/2-paddle_y/2;

            window.addEventListener("keydown", function(evt) {
                Key.onKeyDown(evt.keyCode);
            });

            window.addEventListener("keyup", function(evt) {
                Key.onKeyUp(evt.keyCode);
            });
        }

    processInput() {
        // Si se presiona la barra espaciadora iniciar/reiniciar el juego.
        if(Key.isPress(Key.SPACE_BAR)) {
            start_text = "";
            winner_text = "";
            game_state = true;
            score_p1 = 0;
            score_p2 = 0;
            this.ball.x = canvas_x/2;
            this.ball.y = canvas_y/2;
            player_1 = canvas_y/2-paddle_y/2;
            player_2 = canvas_y/2-paddle_y/2;
        }
        // player_1 no debe salirse del juego por arriba.
        if(Key.isPress(Key.UP_P1) && player_1 > 0)
            player_1 -= 5;
        // player_1 no debe salirse del juego por abajo.
        if(Key.isPress(Key.DOWN_P1) && player_1 < canvas_y-paddle_y) 
            player_1 += 5;
        // player_2 no debe salirse del juego por arriba.
        if(Key.isPress(Key.UP_P2) && player_2 > 0)
            player_2 -= 5;
        // player_2 no debe salirse del juego por abajo.
        if(Key.isPress(Key.DOWN_P2) && player_2 < canvas_y-paddle_y) 
            player_2 += 5;
    }

    update(elapsed) {
        this.ball.x += this.ball.vx; // Actualiza la posición de la bola en x.
        this.ball.y += this.ball.vy; // Actualiza la posición de la bola en y.

        // Rebote en el límite superior.
        if(this.ball.y < this.ball.size)
            this.ball.vy *= -1;
        // Rebote en el límite inferior.
        if(this.ball.y > canvas_y-this.ball.size) 
            this.ball.vy *= -1;
        // Cambia dirección en x cuando colisiona con el player_1
        if(this.ball.x < paddle_x+this.ball.size &&
            this.ball.y < canvas_y-(canvas_y-player_1-paddle_y-this.ball.size)
                && !(this.ball.y+this.ball.size < player_1))
            this.ball.vx *= this.ball.vx > -5 ? -1.5 : -1;
        // Cambia dirección en x cuando colisiona con el player_2
        if(this.ball.x > canvas_x-paddle_x-this.ball.size && 
            (this.ball.y < canvas_y-(canvas_y-player_2-paddle_y-this.ball.size))
                && !(this.ball.y+this.ball.size < player_2))
            this.ball.vx *= this.ball.vx < 5 ? -1.5 : -1;
        // Cuando entra un punto a player_1
        if(this.ball.x < this.ball.size) {
            this.ball.x = canvas_x/2;
            this.ball.y = canvas_y/2;
            this.ball.speed = 1;
            this.ball.angle = validAngle();
            this.ball.vx = Math.cos(this.ball.angle)*this.ball.speed;
            this.ball.vy = Math.sin(this.ball.angle)*this.ball.speed;
            if(game_state){
                if(score_p1 < 5)
                    score_p1++;
                if(score_p1 == 5) {
                    winner_text = "Ganó el jugador 2.";
                    start_text = "Presiona espacio para jugar de nuevo.";
                    game_state = false;
                }
            }
        }
        // Cuando entra un punto a player_2
        if(this.ball.x > canvas_x-this.ball.size) {
            this.ball.x = canvas_x/2;
            this.ball.y = canvas_y/2;
            this.ball.speed = 1;
            this.ball.angle = validAngle();
            this.ball.vx = Math.cos(this.ball.angle)*this.ball.speed;
            this.ball.vy = Math.sin(this.ball.angle)*this.ball.speed;
            if(game_state){
                if(score_p2 < 5)
                    score_p2++;
                if(score_p2 == 5) {
                    winner_text = "Ganó el jugador 1.";
                    start_text = "Presiona espacio para jugar de nuevo.";
                    game_state = false;
                }
            }
        }
        this.ball.update(elapsed);
    }

    render() {
        this.ctx.clearRect(0, 0, canvas_x, canvas_y);
        this.ctx.beginPath();

        this.ball.render(this.ctx);
        this.ctx.fillRect(0, player_1, paddle_x, paddle_y);
        this.ctx.strokeRect(0, player_1, paddle_x, paddle_y);
        this.ctx.fillRect(canvas_x-paddle_x+1, player_2, paddle_x, paddle_y);
        this.ctx.strokeRect(canvas_x-paddle_x+1, player_2, paddle_x, paddle_y);
        //this.ctx.fillRect(0, 0, canvas_x, canvas_y); //Para ver el degradado.

        var gradient = this.ctx.createLinearGradient(0, 0, canvas_x/2, canvas_y);
        gradient.addColorStop("0","magenta");
        gradient.addColorStop("0.5","blue");
        gradient.addColorStop("1.0","red");

        this.ctx.fillStyle = gradient;
        this.ctx.strokeStyle = "#000";
        this.ctx.fill();
        this.ctx.stroke();

        this.ctx.lineJoin = "round";
        this.ctx.font = "bold 75px Monospace";
        this.ctx.strokeText(score_p1+"|"+score_p2, canvas_x/2-canvas_x/9, canvas_y/8);
        this.ctx.fillText(score_p1+"|"+score_p2, canvas_x/2-canvas_x/9, canvas_y/8);
        this.ctx.font = "bold 20px Monospace";
        this.ctx.strokeText(winner_text, canvas_x/2-canvas_x/7, 8*canvas_y/10);
        this.ctx.fillText(winner_text, canvas_x/2-canvas_x/7, 8*canvas_y/10);
        this.ctx.strokeText(start_text, canvas_x/2-canvas_x/4, 9*canvas_y/10);
        this.ctx.fillText(start_text, canvas_x/2-canvas_x/4, 9*canvas_y/10);
    }
}
    GameEngine.Game = Game;
    return GameEngine;
})(GameEngine || {})
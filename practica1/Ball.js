var GameEngine = (function(GameEngine) {
    
    /**
    * Regresa un número aleatorio entre min (inclusivo) y max (exclusivo).
    */
    function getRandomArbitrary(min, max) {
        return Math.random()*(max-min)+min;
    }

    /**
    * Regresa un número aleatorio entre los ángulos válidos.
    */
    function validAngle() {
        return Math.random() > 0.5 
                    ? getRandomArbitrary(135, 226)*Math.PI/180
                    : Math.random() > 0.5
                        ? getRandomArbitrary(0, 46)*Math.PI/180
                        : getRandomArbitrary(315, 361)*Math.PI/180;
    }

    class Ball {
        constructor(x, y, size) {
            this.x = x;
            this.y = y;
            this.size = size;

            this.speed = 1;
            this.angle = validAngle();
            this.vx = Math.cos(this.angle)*this.speed;
            this.vy = Math.sin(this.angle)*this.speed;
        }

    update(elapsed) {
        this.x += this.vx*elapsed;
        this.y += this.vy*elapsed;
    }

    render(ctx) {
        ctx.beginPath();
        ctx.arc(this.x, this.y, this.size, 0, 2*Math.PI);
        ctx.fill();  
    }
}

GameEngine.Ball = Ball;
return GameEngine;
})(GameEngine || {})
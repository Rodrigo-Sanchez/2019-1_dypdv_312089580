var GameEngine = (function(GameEngine) {

  class Trampoline {
    constructor(x, y, size) {
      this.x = x;
      this.y = y;
      this.size = size;
    }

    render(ctx) {
      ctx.fillStyle = "#ffffff";
      ctx.beginPath();
      ctx.fillRect(this.x, this.y, this.size, this.size);
    }
  }

  GameEngine.Trampoline = Trampoline;
  return GameEngine;
})(GameEngine || {})
var GameEngine = (function(GameEngine) {

  class Play {
    constructor(x, y, text_play) {
      this.x = x;
      this.y = y;
      this.text_play = text_play;
    }

    render(ctx) {
      ctx.fillStyle = "#ffffff";
      ctx.lineWidth = 1;
      ctx.lineJoin = "round";
      ctx.font = "bold 20px Monospace";
      ctx.beginPath();
      ctx.fillText(this.text_play, this.x, this.y);
      ctx.restore();
    }
  }

  GameEngine.Play = Play;
  return GameEngine;
})(GameEngine || {})
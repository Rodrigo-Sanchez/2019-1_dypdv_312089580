var GameEngine = (function(GameEngine) {
  let size = 12;

  class Balloon extends GameEngine.Sprite {
    constructor(x, y, sense, value, color, active=true) {
      super(x, y, 2*size, 2*size, "sprites/balloon.svg")

      this.x = x;
      this.y = y;
      this.size = size;
      this.sense = sense;
      this.value = value;
      this.color = color;
      this.active = active;
    }

    update(elapsed) {
      this.sense > 0 ? this.x += 1 : this.x -= 1;
    }

    render(ctx) {
      if(this.active) {
        ctx.fillStyle = this.color;
        ctx.beginPath();
        ctx.arc(this.x, this.y, this.size, 0, 2*Math.PI);
        ctx.fill();
        super.render(ctx);
      }
    }
  }

  GameEngine.Balloon = Balloon;
  return GameEngine;
})(GameEngine || {})
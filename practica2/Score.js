var GameEngine = (function(GameEngine) {

  /**
   * Función que rellena de ceros.
   */
  function leftPad(value, length) {
    return value.toString().length < length ? leftPad("0" + value, length) : value;
  }

  class Score {
    constructor(x, y, text_score) {
      this.x = x;
      this.y = y;
      this.text_score = text_score;
    }

    render(ctx) {
      ctx.fillStyle = "#ffffff";
      ctx.lineWidth = 1;
      ctx.lineJoin = "round";
      ctx.font = "bold 40px Monospace";
      ctx.beginPath();
      ctx.fillText(leftPad(this.text_score, 8), this.x, this.y);
    }
  }

  GameEngine.Score = Score;
  return GameEngine;
})(GameEngine || {})
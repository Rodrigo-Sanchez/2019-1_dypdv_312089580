var GameEngine =(function(GameEngine) {
  let cx;
  let cy;
  let KEY = GameEngine.KEY;
  let game_state = false;

  function isFalse(x) {
    return x == false;
  }

  class Game {
    constructor(ctx) {
      cx = ctx.canvas.width;
      cy = ctx.canvas.height;
      this.ctx = ctx;

      this.seesaw = new GameEngine.Seesaw(cx/2, 19*cy/20, 75, -1);
      this.clown = new GameEngine.Clown(25, 402, 30);
      let pxI = -16;
      let pxD = -16;
      let num_balloons = 20;
      this.balloons = [];
      for(let i=0; i<num_balloons; i++) {
        this.balloons.push(new GameEngine.Balloon(pxI+=32, 60, -1, 9, "#e74c3c"));
        this.balloons.push(new GameEngine.Balloon(pxD+=32, 90, 1, 5, "#338ac6"));
        this.balloons.push(new GameEngine.Balloon(pxI, 120, -1, 2, "#d0af1c"));
      }
      this.trampolineLeft = new GameEngine.Trampoline(0, 9*cy/10, cy/10);
      this.trampolineRight = new GameEngine.Trampoline(cx-cy/10, 9*cy/10, cy/10);
      this.score = new GameEngine.Score(0, cy/12, 0);
      this.life = new GameEngine.Life(9*cx/10, cy/12, 3);
      this.play = new GameEngine.Play(cx/4, cy/2, "Presiona espacio para comenzar");

      window.addEventListener("keydown", function(evt) {
        KEY.onKeyDown(evt.keyCode);
      });
      window.addEventListener("keyup", function(evt) {
        KEY.onKeyUp(evt.keyCode);
      });
    }

    processInput() {
      if(KEY.isPress(KEY.SPACE)) {
        game_state = true;
        this.play.text_play = "";
        this.life.text_life = 3;
        this.score.text_score = 0;
        this.seesaw.x = cx/2;
        this.seesaw.side = -1;
        this.clown.side = 1;
        for(let i=0; i<this.balloons.length; i++)
          this.balloons[i].active = true;
      } 
      if(this.life.text_life == 0) {
        this.play.x = cx/6;
        this.play.text_play = "Presiona espacio para volver a jugar";
      }
      if(game_state)
        this.seesaw.processInput();
    }

    update(elapsed) {
      if(game_state && this.life.text_life > 0) {
        this.seesaw.update(elapsed);
        this.limitSeesaw(this.seesaw);

        this.clown.update(elapsed);
        this.bounceBorders(this.clown);
        this.bounceSeesaw(this.clown, this.seesaw, elapsed);

        for(let i=0; i<this.balloons.length; i++) {
          this.continueBorders(this.balloons[i]);
          this.balloons[i].update(elapsed);
          if(this.balloons[i].active == true)
            // Verificación del choque entre el payaso y los globos.
            if(this.checkCollision(this.balloons[i], this.clown, elapsed)) {
              this.balloons[i].active = false;
              this.score.text_score += this.balloons[i].value;
            }
        }
        this.refillBalloons(2, 20);
        this.refillBalloons(5, 50);
        this.refillBalloons(9, 100);
      }
    }

    bounceBorders(gameObject) {
      // Caso del límite izquierdo.
      if(gameObject.x < gameObject.size) {
        gameObject.vx *= gameObject.bounce;
        gameObject.x = gameObject.size;
      }
      // Caso del límite derecho.
      if(gameObject.x > cx-gameObject.size) {
        gameObject.vx *= gameObject.bounce;
        gameObject.x = cx-gameObject.size;
      }
      // Caso del límite superior.
      if(gameObject.y < gameObject.size) {
        gameObject.vy *= gameObject.bounce;
        gameObject.y = gameObject.size;
      }
      // Caso del límite inferior.
      if(gameObject.y > cy-gameObject.size) {
        gameObject.vy = -250;
        gameObject.y = 402;
        this.life.text_life--;
        this.score.text_score += 1;
        if(gameObject.side > 0) {
            gameObject.x = gameObject.size-5;
            gameObject.side = -1;
        } else {
            gameObject.x = cx-gameObject.size-5;
            gameObject.side = 1;
        }
      }
    }

    limitSeesaw(gameObject) {
      // Caso del límite izquierdo.
      if(gameObject.x < this.trampolineLeft.size+gameObject.size)
        gameObject.x = this.trampolineLeft.size+gameObject.size;
      // Caso del límite derecho.
      if(gameObject.x > cx-this.trampolineRight.size-gameObject.size)
        gameObject.x = cx-this.trampolineRight.size-gameObject.size;
    }

    continueBorders(gameObject) {
      // Caso del límite izquierdo.
      if(gameObject.x > cx+gameObject.size/2)
        gameObject.x = gameObject.size/2;
      // Caso del límite derecho.
      if(gameObject.x < -gameObject.size/2)
        gameObject.x = cx+gameObject.size/2;
    }

    refillBalloons(value, prize) {
      var arr = [];
      for(let i=0; i<this.balloons.length; i++)
        if(this.balloons[i].value == value)
          arr.push(this.balloons[i].active);

      if(arr.every(isFalse)) {
        for(let i=0; i<this.balloons.length; i++)
          if(this.balloons[i].value == value)
            this.balloons[i].active = true;
        this.score.text_score += prize;
      }
    }

    checkCollision(balloon, clown, elapsed) {
      let dx = clown.x-balloon.x,
          dy = clown.y-balloon.y,
          dist = Math.sqrt(dx*dx+dy*dy);

      if(dist < balloon.size+clown.size) {
        let angle = Math.atan2(dy, dx),
            sin = Math.sin(angle),
            cos = Math.cos(angle),
            x0 = 0,
            x1 = dx*cos+dy*sin,
            y1 = dy*cos-dx*sin,
            vx0 = balloon.vx*cos+balloon.vy*sin,
            vx1 = clown.vx*cos+clown.vy*sin,
            vy1 = clown.vy*cos-clown.vx*sin;

        let tmpvx0 = ((balloon.size-clown.size)*vx0+2*clown.size*vx1)/(balloon.size+clown.size);
            vx1 = ((clown.size-balloon.size)*vx1+2*balloon.size*vx0)/(balloon.size+clown.size);
            vx0 = tmpvx0;

        let absV = Math.abs(vx0*elapsed)+Math.abs(vx1*elapsed),
            overlap = (balloon.size+clown.size)-Math.abs(x0-x1);
        x0 += (vx0/absV*overlap)* elapsed;
        x1 += (vx1/absV*overlap)* elapsed;

        let x1Final = x1*cos-y1*sin,
            y1Final = y1*cos+x1*sin;

        clown.x = balloon.x+x1Final;
        clown.y = balloon.y+y1Final;
        clown.vx = vx1*cos-vy1*sin;
        clown.vy = vy1*cos+vx1*sin;

        balloon.active = false;
        return true;
      }
    }

    bounceSeesaw(ball, seesaw) {
      if(seesaw.side == -1) {
        if(ball.x < seesaw.x && ball.x > seesaw.x - seesaw.size/2 && ball.y > cy-50) {
            ball.vy = -350;
            this.score.text_score++;
            ball.x = seesaw.x+seesaw.size;
            seesaw.side = 1;
        } else if(ball.x < seesaw.x && ball.x > seesaw.x - seesaw.size && ball.y > cy-50) {
            ball.vy = -500;
            this.score.text_score++;
            ball.x = seesaw.x+seesaw.size;
            seesaw.side = 1;
        }
      }
      if(seesaw.side == 1) {
        if(ball.x > seesaw.x && ball.x < seesaw.x + seesaw.size/2 && ball.y > cy-50) {
            ball.vy = -350;
            this.score.text_score++;
            ball.x = seesaw.x-seesaw.size;
            seesaw.side = -1;
        } else if(ball.x > seesaw.x && ball.x < seesaw.x + seesaw.size && ball.y > cy-50) {
            ball.vy = -500;
            this.score.text_score++;
            ball.x = seesaw.x-seesaw.size
            seesaw.side = -1;
        }
      }
    }

    render() {
      this.ctx.fillStyle = "rgba(0,0,0,1)";
      this.ctx.fillRect(0, 0, cx, cy);
      
      for(let i=0; i<this.balloons.length; i++)
        if(this.balloons[i].active)
          this.balloons[i].render(this.ctx);
      this.clown.render(this.ctx);
      this.trampolineLeft.render(this.ctx);
      this.trampolineRight.render(this.ctx);
      this.score.render(this.ctx);
      this.life.render(this.ctx);
      this.play.render(this.ctx);
      this.seesaw.render(this.ctx);
    }
  }

  GameEngine.Game = Game;
  return GameEngine;
})(GameEngine || {})
var GameEngine = (function(GameEngine) {

  class Life {
    constructor(x, y, text_life) {
      this.x = x;
      this.y = y;
      this.text_life = text_life;
    }
    
    render(ctx) {
      ctx.fillStyle = "#ffffff";
      ctx.lineWidth = 1;
      ctx.lineJoin = "round";
      ctx.font = "bold 40px Monospace";
      ctx.beginPath();
      ctx.fillText(this.text_life+"♥", this.x, this.y);
    }
  }

  GameEngine.Life = Life;
  return GameEngine;
})(GameEngine || {})
var GameEngine = (function(GameEngine) {
  let gravity = 9.81;
  
  /**
 *Regresa un número aleatorio entre min (inclusivo) y max (exclusivo).
  */
  function getRandomArbitrary(min, max) {
      return Math.random()*(max-min)+min;
  }

  class Clown extends GameEngine.Sprite {
    constructor(x, y, size, side=1) {
      super(x, y, 2*size, 2*size, "sprites/clown.svg");

      this.x = x;
      this.y = y;
      this.size = size;
      this.side = side;
      this.friction = 0.1;
      this.bounce = -0.75;

      this.speed = 300;
      this.angle = getRandomArbitrary(292, 316)*Math.PI/180;
      this.vx = Math.cos(this.angle)*this.speed;
      this.vy = Math.sin(this.angle)*this.speed;
    }

    update(elapsed) {
      this.angle = Math.atan2(this.vy, this.vx);

      this.vx = Math.cos(this.angle)*this.speed;
      this.vy = Math.sin(this.angle)*this.speed+gravity;
      this.x += this.vx*elapsed;
      this.y += this.vy*elapsed;
    }

    render(ctx) {
      /*ctx.fillStyle = "purple";
      ctx.beginPath();
      ctx.arc(this.x, this.y, this.size, 0, 2*Math.PI);
      ctx.fill();*/
      super.render(ctx); // Al final del render dibuja al payaso frente a la pelota.    
    }
  }

  GameEngine.Clown = Clown;
  return GameEngine;
})(GameEngine || {})
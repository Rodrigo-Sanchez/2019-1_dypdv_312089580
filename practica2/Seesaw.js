var GameEngine =(function(GameEngine) {
  let KEY = GameEngine.KEY;

  class Seesaw {
    constructor(x, y, size, side, delayActivation=0.1) {
      this.x = x;
      this.y = y;
      this.size = size;
      this.side = side;
      this.delayActivation = delayActivation;
      this.auxDelayTime = delayActivation;
      this.friction = 0.97;
      this.changing = false;
      this.clownStatic = new GameEngine.Clown(390, 450, 30, -1);
    }

    processInput() {
      if(KEY.isPress(KEY.LEFT))
        this.leftMove = true;
      if(KEY.isPress(KEY.RIGHT))
        this.rightMove = true;
      if(this.delayActivation < this.auxDelayTime) {
        if(KEY.isPress(KEY.Z)) {
          this.changing = true;
          this.auxDelayTime = 0;
        }
      }
    }

    update(elapsed) {
      if(this.leftMove) {
        this.x -= 5;
        this.leftMove = false;
      }
      if(this.rightMove) {
        this.x += 5;
        this.rightMove = false;
      }
      this.auxDelayTime += elapsed;
      if(this.changing)
        if(this.delayActivation < this.auxDelayTime) {
          this.side *= -1;
          this.auxDelayTime = 0;
          this.changing = false;
        }
      this.clownStatic.x = this.side == 1 ? this.x-70 : this.x+70;
    }

    render(ctx) {
      this.clownStatic.render(ctx);

      if(this.side == 1) {
        ctx.moveTo(this.x+this.size, this.y);
        ctx.lineTo(this.x-this.size, this.y+25);
      }
      if(this.side == -1) {
        ctx.moveTo(this.x+this.size, this.y+25);
        ctx.lineTo(this.x-this.size, this.y);
      }
      ctx.moveTo(this.x-10, this.y+25);
      ctx.lineTo(this.x, this.y+15);
      ctx.lineTo(this.x+10, this.y+25);
      ctx.strokeStyle = "#ffffff";
      ctx.lineWidth = 4;
      ctx.fill();
      ctx.stroke();
      ctx.restore();
    }
  }

  GameEngine.Seesaw = Seesaw;
  return GameEngine;
})(GameEngine || {})
var GameEngine = (function (GameEngine) {
  const PI_180 = Math.PI / 180;
  //Guarda la posición del personaje.
  var char_x = 0;
  var char_y = 0;
  var char_z = 0;

  /**
   * Primer enemigo.
   * Funcion: perseguir y tocar al estudiante
   */
  class Enemy {
    constructor(x, y, z, scene) {
      this.state = "Buscando"
      this.scene = scene;
      this.speed = 0;
      this.x = x;
      this.y = y;
      this.z = z;
      this.enemigo = BABYLON.MeshBuilder.CreateCylinder("Enemy", {
        diameter: 7,
        height: 20,
        depth: 1
      }, this.scene);
      this.enemigo.position = new BABYLON.Vector3(this.x, this.y, this.z);
      //this.arma = new GameEngine.Weapon_enemy(scene,x,y,z);
      this.velocity = new BABYLON.Vector3();
      this.rotacion = 0;
      this.angle = .05;
      this.Lifepoints = 2;
    }
    //Actualiza la posición del personaje.
    setPosChar(x, y, z) {

      char_x = x;
      char_y = y;
      char_z = z;
    }
    detectaColision(posicion) {
      if (BABYLON.Vector3.Distance(posicion, this.enemigo.position) < 10) {
        return true;
      } else {
        return false;
      }
    }
    randomWalk() {
      this.speed = 20;

      if (Math.random() < 0.02) {
        this.rotation = (Math.random() < 0.5) ? Math.PI / 4 : -Math.PI / 4;
      }

    }
    processInput(keys) {

    }
    /*
     *Metodo perseguir, calcula la distancia entre el estudiante y el enemigo, si es menor a un rango, lo perseguirá
     *sino caminara aleatoriamente.
     */
    perseguir(posicion, elapsed) {
      if (this.state == "Atacando") {
        let move_vector = BABYLON.Vector3.Lerp(this.enemigo.position, posicion, 0.01).subtract(this.enemigo.position);
        this.enemigo.moveWithCollisions(move_vector);

        this.enemigo.lookAt(posicion);
      } else if (this.state == "Buscando") {
        this.randomWalk();

        this.velocity.x = -Math.cos(this.rotation) * this.speed * elapsed;
        this.velocity.z =  Math.sin(this.rotation) * this.speed * elapsed;
  
        this.enemigo.rotation.y = this.rotation +Math.PI/2;
  
        this.enemigo.moveWithCollisions(this.velocity);
      } else if (this.state = "Muerto") {
        this.enemigo.dispose();
      }


    }
    update(elapsed, posicion) {
      if (BABYLON.Vector3.Distance(this.enemigo.position, posicion) < 700) {
        this.state = "Atacando";
      } else {
        this.state = "Buscando";
      }
      if (this.Lifepoints == 0) {
        this.state = "Muerto";
      }
      this.perseguir(posicion, elapsed);
    }

  }

  GameEngine.Enemy = Enemy;
  return GameEngine;
})(GameEngine || {})
var GameEngine = (function (GameEngine) {
  /*
    Clase que modela la bala normal del jugador
  */
  class Bullet {
    constructor(game, weapon) {
      this.game = game;
      this.weapon = weapon;
      this.scene = game.scene;
      // velicodad de la bala
      this.speed = 10 + this.weapon.student.speed;
      this.vx = 0;
      this.vy = 0;
      this.vz = 0;
      // nos indica si la bala sigue viva
      this.isLive = true;
      // tiempo de vida de la bala inicial
      this.timeLife = 20;
      // tiempo de vida de la bala
      this.life = this.timeLife;
      // Modelo que representa la bala
      this.mesh = BABYLON.MeshBuilder.CreateSphere("bullet", {
        segments: 16,
        diameter: 2
      }, this.game.scene);
      // Posicion de la bala
      this.mesh.position.x = this.weapon.student.camera.position.x;
      this.mesh.position.z = this.weapon.student.camera.position.z;
      this.mesh.position.y = this.weapon.student.camera.position.y - 2;
      // Material de la bala
      var myMaterial = new BABYLON.StandardMaterial("myMaterial", this.game.scene);
      myMaterial.diffuseColor = new BABYLON.Color3(1, 0, 1);
      myMaterial.specularColor = new BABYLON.Color3(0.5, 0.6, 0.87);
      myMaterial.emissiveColor = new BABYLON.Color3(0, 1, 1);
      myMaterial.ambientColor = new BABYLON.Color3(0.23, 0.98, 0.53);

      this.mesh.material = myMaterial;
    }

    /**
     * Metodo de procesamiento de direccion de la bala
     */
    processInput(pickInfo) {
      // Calculamos la direccion en los ejes en los que se movera la bala
      var dx = pickInfo.pickedPoint.x - this.mesh.position.x;
      var dz = pickInfo.pickedPoint.z - this.mesh.position.z;
      var dy = pickInfo.pickedPoint.y - this.mesh.position.y;
      var ang = Math.atan2(dz, dx);
      var ang2 = Math.atan2(dy, dx);
      var mag = Math.sqrt((dz * dz) + (dx * dx));
      var ang2 = Math.atan2(dy, mag);
      this.vx = this.speed * Math.cos(ang);
      this.vz = this.speed * Math.sin(ang);
      this.vy = this.speed * Math.sin(ang2);
    }
    /**
    * Metodo de actualizacion de la bala
    */
    update() {
      if (this.life <= 0) {
        this.isLive = false;
        this.mesh.dispose();
      } else {
        this.life--;
        this.mesh.position.x += this.vx;
        this.mesh.position.z += this.vz;
        this.mesh.position.y += this.vy;
      }
    }

    /**
     * Metodo de animacion de la bala, Babylon se encarga de la animacion
     */
    render() {}

  }

  GameEngine.Bullet = Bullet;
  return GameEngine;
})(GameEngine || {})
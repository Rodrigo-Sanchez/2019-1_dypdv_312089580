var GameEngine = (function (GameEngine) {
  /**
  * Clase que nos permite modelar el HUD del jugador
  */
  class Life {
    constructor(x, y, width, height, lifes) {
      this.x = x + "px";
      this.y = y + "px";
      this.width = width;
      this.height = height;
      this.hits = 1;
      // Indicador de Vidas del jugador (puntos para daños)
      this.totalLifes = lifes;
      // Indicador de vidas anterirores
      this.lifesA = lifes;
      // Indicador de carga
      this.load = false;
      // Indicadores de vida del jugador
      this.rectG = null;
      this.rectO = null;
      // Puntaje del jugador
      this.points = 0;
      // Indicador de balas
      this.bullets = 0;
      // Indicador de tlacuaches disponibles
      this.tlacuaches = 0;

      this.advancedTexture = BABYLON.GUI.AdvancedDynamicTexture.CreateFullscreenUI("UI");

      //Indicador de arma del tlacuache
      var missile_image = new BABYLON.GUI.Image("missile", "img/tlacuache.png");
      missile_image.width = "125px";
      missile_image.height = "125px";
      this.rectM = new BABYLON.GUI.Button();
      this.rectM.width = "150px";
      this.rectM.height = "70px";
      this.rectM.cornerRadius = 2;
      this.rectM.color = "Transparent";
      this.rectM.thickness = 1;
      this.rectM.horizontalAlignment = BABYLON.GUI.Control.HORIZONTAL_ALIGNMENT_LEFT;
      this.rectM.verticalAlignment = BABYLON.GUI.Control.VERTICAL_ALIGNMENT_TOP;
      this.rectM.addControl(missile_image);
      this.advancedTexture.addControl(this.rectM);
      //Indicador del arma de bala normal
      var shot_image = new BABYLON.GUI.Image("shot", "img/shot.png");
      shot_image.width = "100px";
      shot_image.height = "75px";
      this.rectS = new BABYLON.GUI.Button();
      this.rectS.width = "150px";
      this.rectS.height = "70px";
      this.rectS.cornerRadius = 2;
      this.rectS.color = "Orange";
      this.rectS.thickness = 1;
      this.rectS.horizontalAlignment = BABYLON.GUI.Control.HORIZONTAL_ALIGNMENT_RIGHT;
      this.rectS.verticalAlignment = BABYLON.GUI.Control.VERTICAL_ALIGNMENT_TOP;
      this.rectS.addControl(shot_image);
      this.advancedTexture.addControl(this.rectS);
      //Indicador de puntaje en el HUD
      this.rectL = new BABYLON.GUI.Rectangle();
      this.rectL.width = "150px";
      this.rectL.height = "20px";
      this.rectL.cornerRadius = 2;
      this.rectL.color = "Orange";
      this.rectL.thickness = 1;
      this.rectL.background = "green";
      this.rectL.horizontalAlignment = BABYLON.GUI.Control.HORIZONTAL_ALIGNMENT_LEFT;
      this.advancedTexture.addControl(this.rectL);
      // Texto de puntaje
      this.label = new BABYLON.GUI.TextBlock();
      this.label.text = "Puntos: " + this.points;
      this.rectL.addControl(this.label);
      // Indicador de balas disponibles en pantalla
      this.rectR = new BABYLON.GUI.Rectangle();
      this.rectR.width = "150px";
      this.rectR.height = "20px";
      this.rectR.cornerRadius = 2;
      this.rectR.color = "Orange";
      this.rectR.thickness = 1;
      this.rectR.background = "green";
      this.rectR.horizontalAlignment = BABYLON.GUI.Control.HORIZONTAL_ALIGNMENT_RIGHT;
      this.advancedTexture.addControl(this.rectR);
      // Texto de balas disponibles en pantalla
      this.Blabel = new BABYLON.GUI.TextBlock();
      this.Blabel.text = "Balas: " + this.bullets;
      this.rectR.addControl(this.Blabel);
      //Indicador de tlacuaches disponibles al activar Power-Up
      this.rectT = new BABYLON.GUI.Rectangle();
      this.rectT.width = "150px";
      this.rectT.height = "20px";
      this.rectT.cornerRadius = 2;
      this.rectT.top = "22px";
      this.rectT.color = "Orange";
      this.rectT.thickness = 1;
      this.rectT.background = "green";
      this.rectT.isVisible = false;
      this.rectT.horizontalAlignment = BABYLON.GUI.Control.HORIZONTAL_ALIGNMENT_RIGHT;
      this.advancedTexture.addControl(this.rectT);
      // Texto de indicador de tlacuaches
      this.Tlabel = new BABYLON.GUI.TextBlock();
      this.Tlabel.text = "Tlacuaches : " + this.tlacuaches;
      this.rectT.addControl(this.Tlabel);
    }

    /**
    * Metodo que actualiza el Indicador de arma, balas, y tlacuaches disponibles
    */
    update(weapon, bullets, tlacuaches) {
      this.bullets = bullets;
      this.tlacuaches = tlacuaches;
      if (weapon) {
        this.rectS.color = "Transparent";
        this.rectM.color = "Orange";
        this.rectT.isVisible = true;
      } else {
        this.rectS.color = "Orange";
        this.rectM.color = "Transparent";
        this.rectT.isVisible = false;
      }

    }

    /**
    * Metodo que actualiza los marcadores en pantalla
    */
    render() {
      this.label.text = "Puntos: " + this.points;
      this.Blabel.text = "Balas: " + this.bullets;
      this.Tlabel.text = "Tlacuaches : " + this.tlacuaches;
      if (this.lifesA != this.totalLifes) {
        this.lifesA = this.totalLifes;
        this.load = false;
      }
      if (!this.load) {
        if (this.rectG != null)
          this.advancedTexture.removeControl(this.rectG)
        var rect1 = new BABYLON.GUI.Rectangle();
        rect1.width = (this.width * this.totalLifes) + "px";
        rect1.height = this.height + "px";
        rect1.cornerRadius = 20;
        rect1.thickness = 0;
        rect1.background = "green";
        rect1.verticalAlignment = BABYLON.GUI.Control.VERTICAL_ALIGNMENT_TOP;

        this.rectG = rect1;
        this.advancedTexture.addControl(this.rectG);

        if (this.rectO != null)
          this.advancedTexture.removeControl(this.rectO)
        var rect2 = new BABYLON.GUI.Rectangle();
        rect2.width = (this.width * this.totalLifes) + "px";
        rect2.height = this.height + "px";
        rect2.cornerRadius = 20;
        rect2.color = "Orange";
        rect2.thickness = 3;
        rect2.verticalAlignment = BABYLON.GUI.Control.VERTICAL_ALIGNMENT_TOP;
        this.rectO = rect2;
        this.advancedTexture.addControl(this.rectO);
        this.load = true;
      } else {
      }
    }
  }

  GameEngine.Life = Life;
  return GameEngine;
})(GameEngine || {})
var GameEngine = (function (GameEngine) {
  let PI_180 = Math.PI / 180;

  class Bullet_enemy {
    constructor(scene, x, y, z, angulo, eneX, eneY, eneZ) {
      this.x = x;
      this.y = y;
      this.z = z;
      this.id = "bala_enemigo";
      this.size = 2;
      this.mesh = BABYLON.MeshBuilder.CreateSphere(this.id, {
        segments: 16,
        diameter: 2
      }, scene);
      this.mesh.material = new BABYLON.StandardMaterial(this.id + "_material", scene);
      this.mesh.material.diffuseColor = new BABYLON.Color3(0.2, 0.7, 0.1);
      this.setPosition(eneX, eneY, eneZ);
      this.speed = 2;
      this.vx = this.speed * Math.cos(angulo);
      this.vz = this.speed * Math.sin(angulo);
      this.tvida = 100;
    }

    setPosition(x, y, z) {
      this.mesh.position.x = x;
      this.mesh.position.y = y;
      this.mesh.position.z = z;
    }

    getX() {
      return this.mesh.position.x;
    }

    getY() {
      return this.mesh.position.y;
    }

    getZ() {
      return this.mesh.position.z;
    }

    update(elapse) {
      this.tvida--;
      if (this.tvida <= 0) {
        this.mesh.dispose();
      }
      this.mesh.position.x += this.vx;
      this.mesh.position.z += this.vz;

      this.x = this.mesh.position.x;
      this.y = this.mesh.position.y;
      this.z = this.mesh.position.z;
    }
  }

  GameEngine.Bullet_enemy = Bullet_enemy;
  return GameEngine;
})(GameEngine || {})
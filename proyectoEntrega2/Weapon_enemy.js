var GameEngine = (function (GameEngine) {
  let PI_180 = Math.PI / 180;
  var char_x;
  var char_y;
  var char_z;

  class Weapon_enemy {
    constructor(scene, x, y, z) {
      this.x = x + 50;
      this.y = y;
      this.z = z;
      this.id = "arma_enemigo";
      this.size = 10;
      this.tiempo = 0; //Medirá cada cuanto tiempo se va a agregar una bala.
      //arma
      this.mesh = BABYLON.MeshBuilder.CreateBox(this.id, {
        height: 1,
        width: 1,
        depth: 3
      }, scene);
      this.mesh.material = new BABYLON.StandardMaterial(this.id + "_material", scene);
      this.mesh.material.diffuseColor = new BABYLON.Color3(0, 0, 1);
      this.box = BABYLON.MeshBuilder.CreateBox("mango", {
        height: 2,
        width: 1,
        depth: 1
      }, scene);
      this.box.material = new BABYLON.StandardMaterial(this.id + "_material", scene);
      this.box.material.diffuseColor = new BABYLON.Color3(0, 0, 1);
      this.scene = scene;
      this.init();
      this.balas = [];
      this.bool = true;
      //this.balas.push(new GameEngine.Bullet_enemy(scene,x,y,z,0));
      char_x = 0;
      char_y = 0;
      char_z = 0;
    }

    init() {
      this.mesh.position.x = this.x;
      this.mesh.position.y = this.y;
      this.mesh.position.z = this.z;
    }

    //Actualiza la posición del personaje.
    setPosChar(x, y, z) {
      char_x = x;
      char_y = y;
      char_z = z;
    }

    setPosition(x, y, z) {
      this.mesh.position.x = x + 5;
      this.mesh.position.y = y;
      this.mesh.position.z = z;
      this.box.position.x = this.mesh.position.x;
      this.box.position.y = this.mesh.position.y - .5;
      this.box.position.z = this.mesh.position.z - 1.3;
    }

    getX() {
      return this.mesh.position.x;
    }

    getY() {
      return this.mesh.position.y;
    }

    getZ() {
      return this.mesh.position.z;
    }

    rotation(rotacion, angle, space) {
      //this.mesh.rotate(rotacion, angle, space);
      //this.box.rotate(rotacion, angle,space);
    }

    update(elapse, state) {
      if (this.bool) {
        for (let i = 0; i < this.balas.length; i++) {
          //this.balas[i].setPosition(this.mesh.position.x, this.mesh.position.y, this.mesh.position.z);
          if (this.balas[i].tvida <= 0) {
            this.balas.splice(i, 1);
          }
        }

        for (let i = 0; i < this.balas.length; i++) {
          this.balas[i].update(elapse);
        }
        this.tiempo += elapse;

        if (this.tiempo >= 1 && state == "Disparando") {
          this.tiempo = 0;
          var dx = char_x - this.mesh.position.x;
          var dz = char_z - this.mesh.position.z;
          var ang = Math.atan2(dz, dx);
          this.balas.push(new GameEngine.Bullet_enemy(this.scene, this.x, this.y, this.z, ang, this.mesh.position.x, this.mesh.position.y, this.mesh.position.z));
        }
      } else {
        for (let k = 0; k < this.balas.length; k++) {
          this.balas[k].mesh.dispose();
        }
        this.balas = [];
      }
    }
  }

  GameEngine.Weapon_enemy = Weapon_enemy;
  return GameEngine;
})(GameEngine || {})
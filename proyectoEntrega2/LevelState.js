var GameEngine = (function (GameEngine) {
  let keys = {};
  // Puntos para generar Power-Up
  let PointsTlacuache = 1000;
  // Puntos por neutralizar enemigo
  let PointsEnemyNormal = 50;
  // Puntos por neutralizar enemigo con tlacuache
  let PointsEnemyTlacuache = 100;
  // Tlacuaches por Power-Up
  let BulletsTlacuache = 5;
  // Numero maximo de enemigos tipo 1
  let MaxEnemy1 = 4;
  // Numero maximo de enemigos tipo 2
  let MaxEnemy2 = 4;
  //Coordenadas de los enemigos
  let xe, ye, ze;
  //Definira cual de las posibles posiciones le toca al enemigo por aparecer
  let turnoEne=1;
  class LevelState {
    constructor(engine, game) {
      this.ready = false;
      //Variable para contar cada cuanto tiempo saldrá un enemigo
      this.tiempo = 0;
      //Variable para contar el tiempo de intangibilidad
      this.contIntangible = 0;
      //Arreglo de enemigos
      this.enemigos = [];
      this.enemigos2 = [];
      this.engine = engine;
      this.game = game;
      this.CpowerUp = 0;
      this.powerUp = false;

      this.scene = new BABYLON.Scene(engine);
      this.scene.gravity = new BABYLON.Vector3(0, -9.81, 0);
      this.scene.collisionsEnabled = true;
      this.scene.clearColor = new BABYLON.Color3(0.46484375, 0.6171875, 0.79296875);
      this.assetsManager = new BABYLON.AssetsManager(this.scene);
      this.weapon = this.assetsManager.addMeshTask("weapon", "", "babylon/", "gun.babylon");

      // Audio de fondo del videojuego.
      this.background_audio = new Audio("audio/castle_corridor.mp3");
      this.background_audio.loop = true;
      this.background_audio.volume = 0.5;
      this.background_audio.addEventListener("canplay", function () {
        this.play();
      });

      // Audio cuando sube de vida el estudiante.
      this.health_up = new Audio("audio/health_up.mp3");
      this.health_up.volume = 1;

      // Audio cuando el estudiante es herido.
      this.student_hurt = new Audio("audio/student_hurt.mp3");
      this.student_hurt.volume = 1;

      // Audio cuando el porro 1 es herido.
      this.porro1_hurt = new Audio("audio/porro1_hurt.mp3");
      this.porro1_hurt.volume = 1;

      // Audio cuando el porro 2 es herido.
      this.porro2_hurt = new Audio("audio/porro2_hurt.mp3");
      this.porro2_hurt.volume = 1;

      // Comida para aumentar vida
      // Pollo
      this.pollo = this.assetsManager.addMeshTask("pollo", "", "babylon/", "pollo.babylon");
      this.pollo.onSuccess = function (task) {
        let loadedMeshes = task.loadedMeshes;
        for (let i = 0; i < loadedMeshes.length; i++) {
          loadedMeshes[i].position = new BABYLON.Vector3(100, 8, 100);
          loadedMeshes[i].scaling = new BABYLON.Vector3(4, 4, 4);
        }
      }

      // Uvas
      this.uvas = this.assetsManager.addMeshTask("uvas", "", "babylon/", "uvas.babylon");
      this.uvas.onSuccess = function (task) {
        let loadedMeshes = task.loadedMeshes;
        for (let i = 0; i < loadedMeshes.length; i++) {
          loadedMeshes[i].position = new BABYLON.Vector3(200, 8, 100);
          loadedMeshes[i].scaling = new BABYLON.Vector3(4, 4, 4);
        }
      }

      // Pan
      this.pan = this.assetsManager.addMeshTask("pan", "", "babylon/", "pan.babylon");
      this.pan.onSuccess = function (task) {
        let loadedMeshes = task.loadedMeshes;
        for (let i = 0; i < loadedMeshes.length; i++) {
          loadedMeshes[i].position = new BABYLON.Vector3(200, 8, 200);
          loadedMeshes[i].scaling = new BABYLON.Vector3(4, 4, 4);
        }
      }

      // Queso
      this.queso = this.assetsManager.addMeshTask("queso", "", "babylon/", "queso.babylon");
      this.queso.onSuccess = function (task) {
        let loadedMeshes = task.loadedMeshes;
        for (let i = 0; i < loadedMeshes.length; i++) {
          loadedMeshes[i].position = new BABYLON.Vector3(100, 8, 200);
          loadedMeshes[i].scaling = new BABYLON.Vector3(4, 4, 4);
        }
      }

      // Cafeteria
      this.cafeteria = this.assetsManager.addMeshTask("cafeteria", "", "babylon/", "cafeteria2.babylon");
      this.cafeteria.onSuccess = function (task) {
        let loadedMeshes = task.loadedMeshes; // contiene todas las mallas del archivo
        for (let i = 0; i < loadedMeshes.length; i++) {
          console.log(loadedMeshes[i].name); // puedes ver el nombre de las mallas por si quieres modificarlas después
          // puedes aprovechar a cambiarles el tamaño o la posición o los materiales
          loadedMeshes[i].position = new BABYLON.Vector3(0, -3, -150);
          loadedMeshes[i].scaling = new BABYLON.Vector3(5, 3, 8);
          let wallMaterial = new BABYLON.StandardMaterial("wallMaterial", this.scene);
          wallMaterial.diffuseTexture = new BABYLON.Texture("img/tiles.png", this.scene);
          wallMaterial.diffuseTexture.uScale = 1;
          wallMaterial.diffuseTexture.vScale = 1;
          wallMaterial.specularColor = new BABYLON.Color3(0, 0, 0);
          loadedMeshes[i].material = wallMaterial;
          loadedMeshes[i].rotate(BABYLON.Axis.Y, 2 * Math.PI, BABYLON.Space.WORLD);
          loadedMeshes[i].receiveShadows = true;
          loadedMeshes[i].checkCollisions = true;
        }
      }

      // Mesas
      this.mesa = this.assetsManager.addMeshTask("mesa", "", "babylon/", "picnicTable.babylon");
      this.mesa.onSuccess = function (task) {
        let loadedMeshes = task.loadedMeshes; // contiene todas las mallas del archivo
        for (let i = 0; i < loadedMeshes.length; i++) {
          loadedMeshes[i].position = new BABYLON.Vector3(-100, 8, -300);
          loadedMeshes[i].scaling = new BABYLON.Vector3(5, 5, 5);
          loadedMeshes[i].receiveShadows = true;
          loadedMeshes[i].checkCollisions = true;
        }
        for (var numMesas = 0; numMesas < 10; numMesas++) {
          var min = 0;
          var max = -500;
          var random = Math.random() * (+max - +min) + +min;
          var random2 = Math.random() * (+max - +min) + +min;
          let mesa2 = loadedMeshes[0].createInstance("mesa" + numMesas);
          mesa2.position = new BABYLON.Vector3(random, 8, random2);
        }
      }

      // Prometeo
      this.prome = this.assetsManager.addMeshTask("prometeo", "", "babylon/", "fountain.babylon");
      this.prome.onSuccess = function (task) {
        let loadedMeshes = task.loadedMeshes; // contiene todas las mallas del archivo
        for (let i = 0; i < loadedMeshes.length; i++) {
          loadedMeshes[i].position = new BABYLON.Vector3(0, 3, 450);
          loadedMeshes[i].scaling = new BABYLON.Vector3(60, 25, 60);
          loadedMeshes[i].receiveShadows = true;
          loadedMeshes[i].checkCollisions = true;
        }
      }

      this.prometeo = this.assetsManager.addMeshTask("prometeo", "", "babylon/", "monito.babylon");
      this.prometeo.onSuccess = function (task) {
        let loadedMeshes = task.loadedMeshes; // contiene todas las mallas del archivo
        for (let i = 0; i < loadedMeshes.length; i++) {
          loadedMeshes[i].position = new BABYLON.Vector3(0, 40, 450);
          loadedMeshes[i].scaling = new BABYLON.Vector3(0.4, 0.4, 0.4);
          loadedMeshes[i].receiveShadows = true;
          loadedMeshes[i].checkCollisions = true;
        }
      }

      //Biblioteca
      this.biblioteca = this.assetsManager.addMeshTask("biblioteca", "", "", "babylon/biblioteca.babylon");
      this.biblioteca.onSuccess = function (task) {
        let loadedMeshes = task.loadedMeshes; // contiene todas las mallas del archivo
        for (let i = 0; i < loadedMeshes.length; i++) {
          //console.log(loadedMeshes[i].name);
          loadedMeshes[i].position = new BABYLON.Vector3(550, -3, 250);
          loadedMeshes[i].scaling = new BABYLON.Vector3(8, 4, 22);
          let wallMaterial = new BABYLON.StandardMaterial("wallMaterial", this.scene);
          wallMaterial.diffuseTexture = new BABYLON.Texture("img/books.jpg", this.scene);
          wallMaterial.diffuseTexture.uScale = 1;
          wallMaterial.diffuseTexture.vScale = 1;
          wallMaterial.specularColor = new BABYLON.Color3(0, 0, 0);
          loadedMeshes[i].material = wallMaterial;
          loadedMeshes[i].rotate(BABYLON.Axis.Y, Math.PI, BABYLON.Space.WORLD);
          loadedMeshes[i].receiveShadows = true;
          loadedMeshes[i].checkCollisions = true;
        }
      }

      //Edificio matematicas
      this.edificioMate = this.assetsManager.addMeshTask("edificioMate", "", "", "babylon/edificio_mate.babylon");
      this.edificioMate.onSuccess = function (task) {
        let loadedMeshes = task.loadedMeshes; // contiene todas las mallas del archivo
        for (let i = 0; i < loadedMeshes.length; i++) {
          //console.log(loadedMeshes[i].name);
          loadedMeshes[i].position = new BABYLON.Vector3(-550, -3, 350);
          loadedMeshes[i].scaling = new BABYLON.Vector3(8, 4, 12);
          let wallMaterial = new BABYLON.StandardMaterial("wallMaterial", this.scene);
          wallMaterial.diffuseTexture = new BABYLON.Texture("img/maths.jpg", this.scene);
          wallMaterial.diffuseTexture.uScale = 1;
          wallMaterial.diffuseTexture.vScale = 1;
          wallMaterial.specularColor = new BABYLON.Color3(0, 0, 0);
          loadedMeshes[i].material = wallMaterial;
          loadedMeshes[i].receiveShadows = true;
          loadedMeshes[i].checkCollisions = true;
        }
      }

      //Edificio fisica
      this.edificioFisica = this.assetsManager.addMeshTask("edificioFisica", "", "", "babylon/edificio_fisica.babylon");
      this.edificioFisica.onSuccess = function (task) {
        let loadedMeshes = task.loadedMeshes; // contiene todas las mallas del archivo
        for (let i = 0; i < loadedMeshes.length; i++) {
          //console.log(loadedMeshes[i].name);
          loadedMeshes[i].position = new BABYLON.Vector3(-1050, -3, 1050);
          loadedMeshes[i].scaling = new BABYLON.Vector3(60, 4, 12);
          let wallMaterial = new BABYLON.StandardMaterial("wallMaterial", this.scene);
          wallMaterial.diffuseTexture = new BABYLON.Texture("img/physics.jpg", this.scene);
          wallMaterial.diffuseTexture.uScale = 1;
          wallMaterial.diffuseTexture.vScale = 1;
          wallMaterial.specularColor = new BABYLON.Color3(0, 0, 0);
          loadedMeshes[i].material = wallMaterial;
          loadedMeshes[i].receiveShadows = true;
          loadedMeshes[i].checkCollisions = true;
        }
      }

      //Edificio O
      this.edificioO = this.assetsManager.addMeshTask("edificioO", "", "", "babylon/edificioO.babylon");
      this.edificioO.onSuccess = function (task) {
        let loadedMeshes = task.loadedMeshes; // contiene todas las mallas del archivo
        for (let i = 0; i < loadedMeshes.length; i++) {
          //console.log(loadedMeshes[i].name);
          loadedMeshes[i].position = new BABYLON.Vector3(-1050, -3, -1050);
          loadedMeshes[i].scaling = new BABYLON.Vector3(30, 4, 6);
          let wallMaterial = new BABYLON.StandardMaterial("wallMaterial", this.scene);
          wallMaterial.diffuseTexture = new BABYLON.Texture("img/o.jpg", this.scene);
          wallMaterial.diffuseTexture.uScale = 3;
          wallMaterial.diffuseTexture.vScale = 3;
          wallMaterial.specularColor = new BABYLON.Color3(0, 0, 0);
          loadedMeshes[i].material = wallMaterial;
          loadedMeshes[i].rotate(BABYLON.Axis.Y, Math.PI / 2, BABYLON.Space.WORLD);
          loadedMeshes[i].receiveShadows = true;
          loadedMeshes[i].checkCollisions = true;
        }
      }

      //Edificio P
      this.edificioP = this.assetsManager.addMeshTask("edificioP", "", "", "babylon/edificioO.babylon");
      this.edificioP.onSuccess = function (task) {
        let loadedMeshes = task.loadedMeshes; // contiene todas las mallas del archivo
        for (let i = 0; i < loadedMeshes.length; i++) {
          //console.log(loadedMeshes[i].name);
          loadedMeshes[i].position = new BABYLON.Vector3(-1950, -3, -1050);
          loadedMeshes[i].scaling = new BABYLON.Vector3(30, 4, 6);
          let wallMaterial = new BABYLON.StandardMaterial("wallMaterial", this.scene);
          wallMaterial.diffuseTexture = new BABYLON.Texture("img/p.png", this.scene);
          wallMaterial.diffuseTexture.uScale = 3;
          wallMaterial.diffuseTexture.vScale = 3;
          wallMaterial.specularColor = new BABYLON.Color3(0, 0, 0);
          loadedMeshes[i].material = wallMaterial;
          loadedMeshes[i].rotate(BABYLON.Axis.Y, Math.PI / 2, BABYLON.Space.WORLD);
          loadedMeshes[i].receiveShadows = true;
          loadedMeshes[i].checkCollisions = true;
        }
      }

      // Arboles
      var posX1 = 600;
      var posZ1 = 1500;
      let arbol1 = this.assetsManager.addMeshTask("arbol", "", "", "babylon/tree1.babylon");
      arbol1.onSuccess = function (task) {
        let loadedMeshes = task.loadedMeshes;
        for (let i = 0; i < loadedMeshes.length; i++) {
          loadedMeshes[i].position = new BABYLON.Vector3(posX1, 0, posZ1);
          loadedMeshes[i].scaling = new BABYLON.Vector3(10, 10, 10);
          loadedMeshes[i].receiveShadows = true;
          loadedMeshes[i].checkCollisions = true;
        }
        for (var numArboles = 0; numArboles < 10; numArboles++) {
          posX1 -= 200;
          let arbol2 = loadedMeshes[0].createInstance("arbol" + numArboles);
          arbol2.position = new BABYLON.Vector3(posX1, 0, posZ1);
        }
        for (var numArboles = 0; numArboles < 30; numArboles++) {
          var min = -2400;
          var max = 2400;
          var random = Math.random() * (+max - +min) + +min;
          var random2 = Math.random() * (+max - +min) + +min;
          let arbol2 = loadedMeshes[0].createInstance("arbol" + numArboles);
          arbol2.position = new BABYLON.Vector3(random, 0, random2);
        }
        posZ1 = -150;
        for (var numArboles = 0; numArboles < 10; numArboles++) {
          let arbol2 = loadedMeshes[0].createInstance("arbol" + numArboles);
          arbol2.position = new BABYLON.Vector3(-1500, 0, posZ1);
          posZ1 -= 250;
        }
      }

      //Bancas
      var posX = 300;
      var posZ = 900;
      for (var i = 0; i < 7; i++) {
        let banca1 = this.assetsManager.addMeshTask("banca" + i, "", "babylon/", "modern_bench2.babylon");
        banca1.onSuccess = function (task) {
          let loadedMeshes = task.loadedMeshes;
          for (let i = 0; i < loadedMeshes.length; i++) {
            loadedMeshes[i].position = new BABYLON.Vector3(posX, -0.1, posZ);
            loadedMeshes[i].scaling = new BABYLON.Vector3(10, 7, 10);
            loadedMeshes[i].receiveShadows = true;
            loadedMeshes[i].checkCollisions = true;
          }
          posX -= 70;
        }
      }

      let self = this;
      this.assetsManager.onFinish = function () {
        document.getElementById("theCanvas").focus();
        self.init();
      }
      this.assetsManager.load();
    }

    init() {
      // Variable para saber que ya está listo el nivel.
      this.ready = true;

      // Al canvas le damos la función focus().
      document.getElementById("theCanvas").focus();

      // Con jQuery agregamos la mirilla del arma una vez que ya se cargó la escena.
      $(document).ready(function () {
        $('div').remove();
        $('body').append('<img id="viseur" src="assets/viseur.png"/>');
      });

      BABYLON.Animation.AllowMatricesInterpolation = false;

      this.scene.ambientColor = new BABYLON.Color3(0.75, 0.75, 0.75);

      // Luces
      this.light = new BABYLON.DirectionalLight("directionalLight",
        new BABYLON.Vector3(-1, -2, -1),
        this.scene);
      this.light.position = new BABYLON.Vector3(0, 150, 0);
      this.light2 = new BABYLON.DirectionalLight("directionalLight",
        new BABYLON.Vector3(1, -2, 1),
        this.scene);
      this.light2.position = new BABYLON.Vector3(0, 150, 0);

      // Dibujamos una bola amarilla que representa a la luz.
      var lightSphere = BABYLON.Mesh.CreateSphere("lightSphere", 100, 20, this.scene);
      lightSphere.position = this.light.position;
      lightSphere.material = new BABYLON.StandardMaterial("light", this.scene);
      lightSphere.material.emissiveColor = new BABYLON.Color3(1, 1, 0);

      let shadowGenerator = new BABYLON.ShadowGenerator(1024, this.light);

      let worldSize = 4600;

      let ground = BABYLON.Mesh.CreateGround("ground", worldSize, worldSize, 2, this.scene);
      let groundMaterial = new BABYLON.StandardMaterial("groundMaterial", this.scene);
      groundMaterial.diffuseTexture = new BABYLON.Texture("img/floor.png", this.scene);
      groundMaterial.diffuseTexture.uScale = 150;
      groundMaterial.diffuseTexture.vScale = 150;
      groundMaterial.specularColor = new BABYLON.Color3(0, 0, 0);
      ground.receiveShadows = true;
      ground.checkCollisions = true;
      ground.material = groundMaterial;

      // Sky material
      var skyboxMaterial = new BABYLON.SkyMaterial("skyMaterial", this.scene);
      skyboxMaterial.backFaceCulling = false;

      // En esta caja se define el cielo y contiene al mundo, debe ser más grande para que no se lo "coma".
      var skybox = BABYLON.Mesh.CreateBox("skyBox", worldSize + 30, this.scene);
      skybox.material = skyboxMaterial;

      let whiteMaterial = new BABYLON.StandardMaterial("whiteMaterial", this.scene);
      whiteMaterial.emissiveColor = new BABYLON.Color3(1, 1, 1);

      let wallMaterial = new BABYLON.StandardMaterial("wallMaterial", this.scene);
      wallMaterial.diffuseTexture = new BABYLON.Texture("img/bricks.jpg", this.scene);
      wallMaterial.diffuseTexture.uScale = 25;
      wallMaterial.specularColor = new BABYLON.Color3(0, 0, 0);

      // Bordes del escenario para evitar que el personaje caiga al abismo
      let wall1 = BABYLON.MeshBuilder.CreateBox("Wall1", {
        width: worldSize,
        height: 100,
        depth: 10
      }, this.scene);
      wall1.position = new BABYLON.Vector3(0, 30, -worldSize / 2);
      wall1.checkCollisions = true;
      wall1.receiveShadows = true;
      wall1.material = wallMaterial;

      let wall2 = BABYLON.MeshBuilder.CreateBox("Wall2", {
        width: worldSize,
        height: 100,
        depth: 10
      }, this.scene);
      wall2.position = new BABYLON.Vector3(0, 30, worldSize / 2);
      wall2.checkCollisions = true;
      wall2.receiveShadows = true;
      wall2.material = wallMaterial;

      let wall3 = BABYLON.MeshBuilder.CreateBox("Wall3", {
        width: worldSize,
        height: 100,
        sideOrientation: 1,
        depth: 10
      }, this.scene);
      wall3.position = new BABYLON.Vector3(worldSize / 2, 30, 0);
      wall3.checkCollisions = true;
      wall3.receiveShadows = true;
      wall3.material = wallMaterial;
      wall3.rotate(BABYLON.Axis.Y, 2 * Math.PI / 4, BABYLON.Space.WORLD);

      let wall4 = BABYLON.MeshBuilder.CreateBox("Wall4", {
        width: worldSize,
        height: 100,
        depth: 10
      }, this.scene);
      wall4.position = new BABYLON.Vector3(-worldSize / 2, 30, 0);
      wall4.checkCollisions = true;
      wall4.receiveShadows = true;
      wall4.material = wallMaterial;
      wall4.rotate(BABYLON.Axis.Y, 2 * Math.PI / 4, BABYLON.Space.WORLD);

      let boxMaterial = new BABYLON.StandardMaterial("boxMat", this.scene);
      boxMaterial.diffuseTexture = new BABYLON.Texture("img/box.jpeg", this.scene);
      boxMaterial.diffuseTexture.hasAlpha = true;
      boxMaterial.specularColor = new BABYLON.Color3(0, 0, 0);

      // Crea una caja de madera.
      // La mitad del tamaño de la arena como mínimo y la otra como máximo.
      var min = -2400;
      var max = 2400;
      var random = Math.random() * (+max - +min) + +min;
      var random2 = Math.random() * (+max - +min) + +min;
      // Crea una caja.
      var box = BABYLON.Mesh.CreateBox("box", 20, this.scene);
      box.material = boxMaterial
      box.position = new BABYLON.Vector3(random, 10, random2);
      box.checkCollisions = true;
      // For para crear 50 cajas de madera.
      for (var i = 0; i < 50; i++) {
        random = Math.random() * (+max - +min) + +min;
        random2 = Math.random() * (+max - +min) + +min;
        let box2 = box.createInstance("box" + i);
        box2.position = new BABYLON.Vector3(random, 10, random2);
      }

      // Personaje principal.
      this.student = new GameEngine.Student(this);

      // Definimos las vidas del personaje principal.
      this.numeroVidas = 5;
      this.life = new GameEngine.Life(-300, 400, 200, 40, this.numeroVidas);

      // Registro de entrada del teclado
      this.scene.actionManager = new BABYLON.ActionManager(this.scene);
      this.scene.actionManager.registerAction(
        new BABYLON.ExecuteCodeAction(BABYLON.ActionManager.OnKeyDownTrigger,
          function (evt) {
            keys[evt.sourceEvent.key] = true;
          }
        )
      );
      this.scene.actionManager.registerAction(
        new BABYLON.ExecuteCodeAction(BABYLON.ActionManager.OnKeyUpTrigger,
          function (evt) {
            delete keys[evt.sourceEvent.key];
          }
        )
      );
    }

    processInput() {
      if (this.ready) {
        this.student.processInput();
      }
    }

    update(elapsed) {
      // Verificamos si ya ha pasado el tiempo suficiente para generar otro enemigo
      this.tiempo += elapsed;
      if(this.tiempo >= 10){
        switch(turnoEne){
          case 1:
            xe = -370;
            ye = 10;
            ze = 645;
            turnoEne = 2;
            break;
          case 2:
            xe = 539;
            ye = 10;
            ze = 822;
            turnoEne = 3;
            break;
          case 3: 
            xe = -328;
            ye = 10;
            ze = -296;
            turnoEne =1;
            break;  
        }
        //Creamos más enemigos tipo 1, de ser posible
        if(this.enemigos.length < MaxEnemy1){          
            this.enemigos.push(new GameEngine.Enemy(xe,10.05,ze+30, this.scene));
        }else{
          for(let u = 0; u < this.enemigos.length; u++){
            if(this.enemigos[u].Lifepoints < 1){
              this.enemigos[u] = new GameEngine.Enemy(xe,10.05,ze+30, this.scene);
              u = this.enemigos.length;
            }
          }
        }
        //Creamos mas enemigos de tipo 2, de ser posible       
        if(this.enemigos2.length < MaxEnemy2){          
          this.enemigos2.push(new GameEngine.Enemy2(xe+20,10.05,ze, this.scene));
        }else{
          for(let v = 0; v < this.enemigos2.length; v++){
            if(this.enemigos2[v].Lifepoints < 1){
              this.enemigos2[v] = new GameEngine.Enemy2(xe+20,10.05,ze, this.scene);
              v = this.enemigos2.length;
            }
          }
        }
        this.tiempo =0;
      }

      if(this.ready) {
        // Actualizamos estudiante
        this.student.update();    
        // Verifiamos si se activa el Power-Up    
        if((this.life.points - this.CpowerUp) > 1000){
          this.student.weapon.TBullets = BulletsTlacuache;
          this.CpowerUp += 1000;
          this.student.weapon.tlacuache = true;
        }
        // Actualizamos el indicador de arma,numero de balas en el cargador y numero de tlacuaches disponibles
        this.life.update(this.student.weapon.tlacuache, this.student.weapon.numBalls, this.student.weapon.TBullets);
        for (let i = 0; i < this.enemigos.length; i++) {
          this.enemigos[i].update(elapsed, this.student.camera.position);
        }
        for (let i = 0; i < this.enemigos2.length; i++) {
          this.enemigos2[i].update(elapsed, this.student.camera.position);
          this.enemigos2[i].setPosChar(this.student.camera.position.x,
            this.student.camera.position.y,
            this.student.camera.position.z);
        }

        // Para cada bala del enemigo2 se revisa si hizo una colisión con el estudiante.
        for (let j = 0; j < this.enemigos2.length; j++) {
          for (var i = 0; i < this.enemigos2[j].arma.balas.length; i++) {
            if (this.enemigos2[j].detectaColision(this.student.camera.position) && (this.student.intangible == false)) {
              this.life.totalLifes--;
              this.student.intangible = true;
              this.student_hurt.play();
            }
            if ((this.student.intangible == false) && this.enemigos2[j].arma.balas[i].mesh.intersectsMesh(this.student.body, false) && this.life.totalLifes > 0) {
              this.life.totalLifes--;
              this.student.intangible = true;
              this.student_hurt.play();
            }
          }
        }

        // Verificamos colicion del jugador con enemigos tipo 1
        for (let i = 0; i < this.enemigos.length; i++) {
            if (this.enemigos[i].detectaColision(this.student.camera.position) && (this.student.intangible == false) && (this.enemigos[i].Lifepoints > 0)) {
              this.life.totalLifes--;
              this.student.intangible = true;
              this.student_hurt.play();
            }
        }

        // Verificamos colisiones de las balas del jugador con enemigos tipo 1
        for (let e = 0; e < this.enemigos.length; e++) {
          for (let b = 0; b < this.student.weapon.b.length; b++) {
            if (this.enemigos[e].enemigo.intersectsMesh(this.student.weapon.b[b].mesh, false)) {
              if (this.student.weapon.tlacuache)
                this.enemigos[e].Lifepoints--;
              this.enemigos[e].Lifepoints--;
              if (this.enemigos[e].Lifepoints == 0)
                if (this.student.weapon.tlacuache) {
                  this.life.points += PointsEnemyTlacuache;
                } else {
                  this.life.points += PointsEnemyNormal;
                }
              this.porro1_hurt.play();
            }
          }
        }
        // Verificamos colisiones de las balas del jugador con enemigos tipo 2
        for (let e = 0; e < this.enemigos2.length; e++) {
          for (let b = 0; b < this.student.weapon.b.length; b++) {
            if (this.enemigos2[e].enemigo.intersectsMesh(this.student.weapon.b[b].mesh, false)) {                                 
              if (this.student.weapon.tlacuache)
                this.enemigos2[e].Lifepoints--;
              this.enemigos2[e].Lifepoints--;
              if (this.enemigos2[e].Lifepoints == 0)
                if (this.student.weapon.tlacuache) {
                  this.life.points += PointsEnemyTlacuache;
                } else {
                  this.life.points += PointsEnemyNormal;
                }
              this.porro2_hurt.play();
            }
          }
        }

        // Aumenta la vida al tocar comida.
        var min = -1040;
        var max = 1040;
        var random = Math.random() * (+max - +min) + +min;
        var random2 = Math.random() * (+max - +min) + +min;

        var polloMesh = this.scene.getMeshByName("TurkeyLeg.001");
        if (this.student.weapon.mesh.intersectsMesh(polloMesh, false)) {
          if (this.life.totalLifes < this.numeroVidas) {
            this.life.totalLifes++;
            // Cambia la posicion de la comida.
            polloMesh.position = new BABYLON.Vector3(random, 8, random2);
            this.health_up.play();
          }
        }
        var panMesh = this.scene.getMeshByName("SplitTopBread.001");
        if (this.student.weapon.mesh.intersectsMesh(panMesh, false)) {
          if (this.life.totalLifes < this.numeroVidas) {
            this.life.totalLifes++;
            // Cambia la posicion de la comida.
            panMesh.position = new BABYLON.Vector3(random, 8, random2);
            this.health_up.play();
          }
        }
        var quesoMesh = this.scene.getMeshByName("CheeseWedge.001");
        if (this.student.weapon.mesh.intersectsMesh(quesoMesh, false)) {
          if (this.life.totalLifes < this.numeroVidas) {
            this.life.totalLifes++;
            // Cambia la posicion de la comida.
            quesoMesh.position = new BABYLON.Vector3(random, 8, random2);
            this.health_up.play();
          }
        }
        var uvasMesh = this.scene.getMeshByName("Grapes.001");
        if (this.student.weapon.mesh.intersectsMesh(uvasMesh, false)) {
          if (this.life.totalLifes < this.numeroVidas) {
            this.life.totalLifes++;
            // Cambia la posicion de la comida.
            uvasMesh.position = new BABYLON.Vector3(random, 8, random2);
            this.health_up.play();
          }
        }

        // Si el estudiante es intangible cuenta 3 segundos para volverlo tangible.
        if (this.student.intangible) {
          this.contIntangible += elapsed;

          if (this.contIntangible > 3) {
            this.student.intangible = false;
            this.contIntangible = 0;
          }
        }

        // Caso cuando ya no quedan vidas.
        if (this.life.totalLifes < 1) {
          this.background_audio.pause();
          document.exitPointerLock();
          this.changeState("scene_2");
        }
      }
    }

    /*
    * Metodo que cambia el estado del juego
    */
    changeState(state_name) {
      this.game.changeState(state_name);
    }

    /*
    * Metodo que renderiza la escena y el HUD
    */
    render() {
      if (this.ready) {
        this.life.render();
        this.scene.render();
      }
    }

    _initMesh(task) {
      this.assets[task.name] = task.loadedMeshes;
      for (var i = 0; i < task.loadedMeshes.length; i++) {
        var mesh = task.loadedMeshes[i];
        mesh.isVisible = false;
      }
    }

    /*
    * Metodo que activa el Power-Up
    */
    powerUp() {
      if ((this.life.points - this.CpowerUp) > 1000) {
        this.student.weapon.TBullets = BulletsTlacuache;
        this.CpowerUp += 1000;
        this.student.weapon.tlacuache = true;
      }
    }

  }

  GameEngine.LevelState = LevelState;
  return GameEngine;
})(GameEngine || {})
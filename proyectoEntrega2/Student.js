var GameEngine = (function (GameEngine) {
  let Key = {
    _pressed: {},

    LEFT: 37,
    UP: 38,
    RIGHT: 39,
    DOWN: 40,
    SPACE: 32,
    ZETA: 90,

    isPress: function (keyCode) {
      return this._pressed[keyCode];
    },
    onKeyDown: function (keyCode) {
      this._pressed[keyCode] = true;
    },
    onKeyUp: function (keyCode) {
      delete this._pressed[keyCode];
    }
  }

  let t = 0;

  class Student {
    constructor(game) {
      this.scene = game.scene;
      this.spawnPoint = new BABYLON.Vector3(0, 10, -10);
      this.game = game;
      this.controlEnabled = false;
      this.intangible = false;
      this.weapon = new GameEngine.Weapon(game, this);

      var _this = this;
      var canvas = this.scene.getEngine().getRenderingCanvas();
      canvas.addEventListener("click", function (evt) {
        var width = _this.scene.getEngine().getRenderWidth();
        var height = _this.scene.getEngine().getRenderHeight();

        //this.weapon = new GameEngine.Weapon(game, this);
        if (_this.controlEnabled) {
          var pickInfo = _this.scene.pick(width / 2, height / 2, null, false, _this.camera);
          _this.handleUserMouse(evt, pickInfo);
        }
      }, false);

      // La altura de los ojos del estudiante.
      this.height = 5;
      // La velocidad del jugador.
      this.speed = 3;
      // La inercia del jugador.
      this.inertia = 0.9;
      // La inercia angular del jugador.
      this.angularInertia = 0;
      // La sensibilidad del mouse, (entre más bajo más sensible).
      this.angularSensibility = 1000;

      // Creamos una FreeCamera
      this.camera = new BABYLON.FreeCamera("camera", new BABYLON.Vector3(0, 10, 10), this.scene);
      // Regresa el canvas donde se está ejecutando el scene.
      this.camera.attachControl(this.scene.getEngine().getRenderingCanvas());
      this.camera.ellipsoid = new BABYLON.Vector3(2, this.height, 2);

      this.body = BABYLON.MeshBuilder.CreateSphere("Ball", {
        diameterX: 10,
        diameterY: 25,
        diameterZ: 0
      }, this.scene);

      this.camera.checkCollisions = true;
      this.camera.applyGravity = true;
      // Agregamos la funcionalidad de las teclas
      this.camera.keysUp = [87]; // W
      this.camera.keysDown = [83]; // S
      this.camera.keysLeft = [65]; // A
      this.camera.keysRight = [68]; // D
      this.camera.speed = this.speed;
      this.camera.inertia = this.inertia;
      this.camera.angularInertia = this.angularInertia;
      this.camera.angularSensibility = this.angularSensibility;
      this.camera.layerMask = 2;
      this.camera.position.x = 150;
      this.weapon.position = this.camera.globalposition;
      this.weapon.mesh.parent = this.camera;
      this.body.parent = this.camera;

      this._initPointerLock();

      this.scene.activeCameras.push(this.camera);
      this.scene.activeCamera = this.camera;

    }

    /**
    * Procesamos la entrada del teclado
    * Este metodo se uso para cuando se propuso que el jugador pudiera cambiar de armas
    */
    processInput() {
      if (t == 90) {
        this.weapon.tlacuache = !this.weapon.tlacuache;
        t = 0;
      }
    }

    /**
    * Metodo que actualiza la situacion del estudiante
    */
    update(elapsed) {
      this.weapon.canFire = true;
      this.weapon.update();
    }

    /**
    * No se utilizo este metodo, porque Babylon se encarga de manejarlo por nosotros
    */
   render() {
   }

   /**
   * Metodo que se encarga de manejar la mira del arma
   */
    _initPointerLock() {
      var _this = this;
      // Solicitud para bloquear el puntero
      var canvas = this.scene.getEngine().getRenderingCanvas();
      canvas.addEventListener("click", function (evt) {
        canvas.requestPointerLock = canvas.requestPointerLock || canvas.msRequestPointerLock || canvas.mozRequestPointerLock || canvas.webkitRequestPointerLock;
        if (canvas.requestPointerLock) {
          canvas.requestPointerLock();
        }
      }, false);

      // Detector para eventos al actualizar el puntero
      var pointerlockchange = function (event) {
        _this.controlEnabled = (document.mozPointerLockElement === canvas || document.webkitPointerLockElement === canvas || document.msPointerLockElement === canvas || document.pointerLockElement === canvas);
        if (!_this.controlEnabled) {
          _this.camera.detachControl(canvas);
        } else {
          _this.camera.attachControl(canvas);
        }
      };

      document.addEventListener("pointerlockchange", pointerlockchange, false);
      document.addEventListener("mspointerlockchange", pointerlockchange, false);
      document.addEventListener("mozpointerlockchange", pointerlockchange, false);
      document.addEventListener("webkitpointerlockchange", pointerlockchange, false);
    }

    /**
     * Menajador de entrada del usuario en teclado
     */
    handleUserKeyboard(keycode) {
      switch (keycode) {}
    }

    /**
     * Manejador de entrada del usuario en Mouse
     * click = Disparo
     * @param pickInfo Recoge los datos del puntero cuando se hace click.
     */
    handleUserMouse(evt, pickInfo) {
      this.weapon.processInput(pickInfo);
    }

  }

  GameEngine.Student = Student;
  return GameEngine;
})(GameEngine || {})
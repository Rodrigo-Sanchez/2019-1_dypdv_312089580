var GameEngine = (function (GameEngine) {

  class EndState {
    constructor(engine, game) {
      let self = this;

      this.engine = engine;
      this.game = game;

      this.scene = new BABYLON.Scene(engine);
      this.scene.clearColor = new BABYLON.Color3(0, 0, 0);

      this.camera = new BABYLON.UniversalCamera("camera", new BABYLON.Vector3(0, 0, -10), this.scene);
      this.camera.setTarget(BABYLON.Vector3.Zero());

      this.advancedTexture = BABYLON.GUI.AdvancedDynamicTexture.CreateFullscreenUI("GUI");

      this.image = new BABYLON.GUI.Image("img", "img/end.png");
      this.image.width = "1178px";
      this.image.height = "883px";
      this.image.stretch = BABYLON.GUI.Image.STRETCH_UNIFORM;
      this.advancedTexture.addControl(this.image);
      this.image.isVisible = true;

      let button1 = BABYLON.GUI.Button.CreateSimpleButton("Button_1", "Empezar de nuevo");
      button1.width = 0.2;
      button1.height = "40px";
      button1.top = "150px";
      button1.left = "250px";
      button1.color = "black";
      button1.background = "white";
      this.advancedTexture.addControl(button1);
      button1.onPointerDownObservable.add(function (info) {
        self.changeState("scene_1");
      });

      // Con jQuery quitamos la mirilla del arma una vez que terminó la partida.
      $(document).ready(function () {
        $('#viseur').remove();
        $('body').prepend('<div id="particles-js"></div>');
        $('body').append('<script src="particles.js"></script>');
        $('body').append('<script src="app.js"></script>');
      });
    }

    changeState(state_name) {
      this.game.changeState(state_name);
    }

    processInput(keys) {}

    update(elapsed) {}

    render() {
      this.scene.render();
    }
  }

  GameEngine.EndState = EndState;
  return GameEngine;
})(GameEngine || {})
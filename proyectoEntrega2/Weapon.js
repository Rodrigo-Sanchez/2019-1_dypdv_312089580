var GameEngine = (function (GameEngine) {
    /*
    Clase que modela el arma del jugador
  */
  class Weapon {
    constructor(game, student) {

      this.game = game;
      this.student = student;
      this.loader = new BABYLON.AssetsManager(game.scene);
      // Modelo del arma
      var wp = game.scene.getMeshByName("weapon");
      wp.isVisible = true;
      wp.rotationQuaternion = null;
      wp.rotation.x = -Math.PI / 2;
      wp.rotation.y = Math.PI;
      wp.position = new BABYLON.Vector3(-0.3, 3, 2.5);
      this.mesh = wp;
      this.mesh.parent = student.camera;
      this._initialRotation = this.mesh.rotation.clone();
      // Rango de disparo
      this.fireRate = 250.0;
      this._currentFireRate = this.fireRate;
      this.canFire = true;
      // Particulas de disparo 
      var scene = this.game.scene;
      var particleSystem = new BABYLON.ParticleSystem("particles", 100, scene);
      particleSystem.emitter = this.mesh; // the starting object, the emitter
      particleSystem.particleTexture = new BABYLON.Texture("assets/particles/gunshot_125.png", scene);
      particleSystem.emitRate = 5;
      particleSystem.blendMode = BABYLON.ParticleSystem.BLENDMODE_STANDARD;
      particleSystem.minEmitPower = 1;
      particleSystem.maxEmitPower = 3;
      particleSystem.colorDead = new BABYLON.Color4(1, 1, 1, 0.0);
      particleSystem.minLifeTime = 0.2;
      particleSystem.maxLifeTime = 0.2;
      particleSystem.updateSpeed = 0.03;
      this.particleSystem = particleSystem;
      var _this = this;
      this.game.scene.registerBeforeRender(function () {
        if (!_this.canFire) {
          _this._currentFireRate -= game.engine.getDeltaTime() / 1000;
          if (_this._currentFireRate >= 0) {
            _this.canFire = true;
            _this._currentFireRate = _this.fireRate;
          }
        }
      });
      //Numero de balas del arma (por carga)
      this.numBalls = 15;
      //Arreglo de balas
      this.b = [];
      // Indicador si lanza tlacuaches
      this.tlacuache = false;
      // Numero de tlacuaches al Power-Up
      this.TBullets = 5;
      // Indicador de inicio de disparos
      this.startFire = false;
      this.t = 0;
      // Audio de disparo
      this.audio_gun = new Audio("audio/gun.mp3");
      this.audio_gun.volume = 1;
    }

    /**
     * Metodo de procesamiento de informacion
     */
    processInput(pickInfo) {
        // Verificamos si se puede disparar
      if (this.canFire) {
        this.audio_gun.currentTime = 0;
        // Verificamos si hay balas en el cargador
        if (this.numBalls > 0) {
            // Verificamos si ya se inicio el arma
          if (!this.start)
            this.start = true;
            // Verificamos si disparamos tlacuache o bala normal
          if (this.tlacuache) {
            var bull = new GameEngine.Bullet_Tlacuache(this.game, this);
            this.TBullets--;
            if (this.TBullets == 0)
              this.tlacuache = false;
          } else {
            var bull = new GameEngine.Bullet(this.game, this);
          }
          //indicamos la direccion de la bala
          if (this.b.length < 15) {
            bull.processInput(pickInfo);
            this.b.push(bull);
          }
          this.numBalls--;
        } else {
          this.canFire = false;
        }
        // Aumentamos la velocidad de respuesta de la animacion del arma
        this.render();
        this.audio_gun.play();
      } else {
        // Nada que hacer: No podemos disparar
        this.audio_gun.pause();
      }
    }

    update() {
        // Verificamos el tamaño del cargador
      if (this.b.length <= 15) {
        // Verificamos el numero de balas vivas
        var bulletLives = 0;
        for (var i = 0; i < this.b.length; i++) {
            // Se actualizan las balas
            this.b[i].update();
            if (this.b[i].isLive == true)
                bulletLives++;
        }
        // Reiniciamos el cargador 
        if (bulletLives == 0 && this.start) {
          this.numBalls = 15;
          this.b = [];
        }
      }
    }

    /**
     * Animacion del arma
     */
    render() {
      if (this.canFire)
        this.particleSystem.start();
      var start = this._initialRotation.clone();
      var end = start.clone();
      end.x += Math.PI / 10;
      // Creamos la animacion del objeto
      var display = new BABYLON.Animation(
        "fire",
        "rotation",
        60,
        BABYLON.Animation.ANIMATIONTYPE_VECTOR3,
        BABYLON.Animation.ANIMATIONLOOPMODE_CONSTANT);
      // Claves de animacion
      var keys = [{
        frame: 0,
        value: start
      }, {
        frame: 10,
        value: end
      }, {
        frame: 100,
        value: start
      }];
      // Asignamos las llaves
      display.setKeys(keys);
      // agregamos la animacion al modelo
      this.mesh.animations.push(display);
      var _this = this;
      this.game.scene.beginAnimation(this.mesh, 0, 100, false, 10, function () {
        _this.particleSystem.stop();
      });
    }

    /**
    * Metodo para cambio de tipo de bala
    */
    changeBullet() {
      this.tlacuache = !this.tlacuache;
    }
  }

  GameEngine.Weapon = Weapon;
  return GameEngine;
})(GameEngine || {})
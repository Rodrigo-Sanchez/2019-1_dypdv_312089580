var GameEngine = (function (GameEngine) {

  class MenuState {
    constructor(engine, game) {
      let self = this;

      this.engine = engine;
      this.game = game;

      this.scene = new BABYLON.Scene(engine);
      this.scene.clearColor = new BABYLON.Color3(0, 0, 0);

      this.camera = new BABYLON.UniversalCamera("camera", new BABYLON.Vector3(0, 0, -10), this.scene);
      this.camera.setTarget(BABYLON.Vector3.Zero());

      this.advancedTexture = BABYLON.GUI.AdvancedDynamicTexture.CreateFullscreenUI("GUI");

      // Agregamos el logo del juego en la parte superior de la pantalla.
      this.image = new BABYLON.GUI.Image("img", "img/call.jpg");
      this.image.width = "316px";
      this.image.height = "274px";
      this.image.top = "50px";
      this.image.verticalAlignment = BABYLON.GUI.Control.VERTICAL_ALIGNMENT_TOP;
      this.image.stretch = BABYLON.GUI.Image.STRETCH_UNIFORM;
      this.advancedTexture.addControl(this.image);
      this.image.isVisible = true;

      // Agregamos el botón para comenzar el juego.
      let button1 = BABYLON.GUI.Button.CreateSimpleButton("Button_1", "Empezar el juego");
      button1.width = 0.2;
      button1.height = "40px";
      button1.color = "black";
      button1.top = "100px";
      button1.background = "white";
      this.advancedTexture.addControl(button1);
      button1.onPointerDownObservable.add(function (info) {
        self.changeState("scene_1");
      });

      // Agregamos el botón para desplegar el gameplay.
      let button2 = BABYLON.GUI.Button.CreateSimpleButton("Button_2", "Gameplay");
      button2.width = 0.2;
      button2.height = "40px";
      button2.top = "150px";
      button2.color = "black";
      button2.background = "white";
      this.advancedTexture.addControl(button2);
      button2.onPointerDownObservable.add(function (info) {
        self.image = new BABYLON.GUI.Image("img", "img/gameplay.jpg");
        self.image.width = "282px";
        self.image.height = "97px";
        self.image.verticalAlignment = BABYLON.GUI.Control.VERTICAL_ALIGNMENT_BOTTOM;
        self.image.stretch = BABYLON.GUI.Image.STRETCH_UNIFORM;
        self.advancedTexture.addControl(self.image);
        self.image.isVisible = true;
      });
    }

    changeState(state_name) {
      this.game.changeState(state_name);
    }

    processInput(keys) {}

    update(elapsed) {}

    render() {
      this.scene.render();
    }
  }

  GameEngine.MenuState = MenuState;
  return GameEngine;
})(GameEngine || {})
var GameEngine = (function(GameEngine) {
  let cw;
  let ch;

  class Level1State {
    constructor(game, w, h) {
      this.game = game;
      cw = w;
      ch = h;

      this.camera = new GameEngine.Camera(cw/2, 2000, cw, ch);
      this.player = new GameEngine.Player(cw/2, 2000, 16, 32);
      this.life = new GameEngine.Life(10, 1900, 28);
      this.level = new GameEngine.Level();
      this.background_audio = new Audio("audios/level_1.mp3");
      this.background_audio.loop = true;
      this.background_audio.volume = 0.5;
      this.background_audio.addEventListener("canplay", function() {
        this.play();
      });
    }

    processInput() {
      this.player.processInput();
    }

    update(elapsed) {
      if((this.player.tryGrabLadder) && (!this.player.inLadder)) {
        this.checkCollisionLadders(this.player, this.level);
      }
      this.player.update(elapsed);
      if(this.player.inLadder) {
        this.checkInLadders(this.player, this.level);
      }
      this.checkCollisionPlatforms(this.player, this.level);
      if(this.player.inFloor) {
        this.player.inLadder = false;
      }
      this.player.setState();
      this.camera.update(this.player, this.level);
      //this.checkCollisionWalls();

      // update enemies
      for(let i=0, l=this.level.enemies.length; i<l; i++) {
        if(Math.abs(this.camera.x - this.level.enemies[i].x) < cw/2) {
          this.level.enemies[i].active = true;
        }
        this.level.enemies[i].update(elapsed, this.level, cw, ch, this.camera, this.player);
      }

      // Verificamos la colisión entre el jugador y los enemigos.
      for(let i=0, l=this.level.enemies.length; i<l; i++) {
        this.checkCollisionEnemy(this.player, this.level.enemies[i]);
      }
      //this.refreshInvencible();

      // Dibuja la vida del jugador.
      this.life.x = this.camera.x-125;
      this.life.y = this.camera.y-118;
    }

    refreshInvencible() {
      let aux =0;
    }

    checkCollisionEnemy(player, enemy) {
      if(Math.abs(player.x - enemy.x) < player.w_2 + enemy.w_2 && 
         Math.abs(player.y - enemy.y) < player.h_2 + enemy.h_2 && 
         !this.player.invencible) {
          this.life.lifes--;
          this.player.state = "hurt";
          //this.player.invencible = true;
   }

    }

    checkCollisionLadders(player, level) {
      let player_tile_pos = level.getTilePos(player.x, player.y);
      let ladder;

      // top
      ladder = level.getPlatform(player_tile_pos.x, player_tile_pos.y-1);
      if(ladder && (ladder.type === "Ladder") && (player.ladderVy < 0)) {
        this.player.x = ladder.x;
        this.player.inLadder = true;
        return;
      }

      //center
      ladder = level.getPlatform(player_tile_pos.x, player_tile_pos.y);
      if(ladder && (ladder.type === "Ladder")) {
        this.player.x = ladder.x;
        this.player.inLadder = true;
        return;
      }

      // bottom
      ladder = level.getPlatform(player_tile_pos.x, player_tile_pos.y+1);
      if(ladder && (ladder.type === "Ladder") && (player.ladderVy > 0)) {
        this.player.x = ladder.x;
        this.player.inLadder = true;
        return;
      }
    }

    checkInLadders(player, level) {
      let player_tile_pos = level.getTilePos(player.x, player.y + player.h_2);
      let ladder;
      //center
      ladder = level.getPlatform(player_tile_pos.x, player_tile_pos.y);
      if(ladder && (ladder.type === "Ladder")) {
        this.player.inLadder = true;
      } else {
        this.player.inLadder = false;
      }
    }

    checkCollisionWalls() {
      if(this.player.x < this.camera.x -cw/2 +this.player.w_2) {
        this.player.x = this.camera.x -cw/2 +this.player.w_2;
      }
      if(this.player.x > this.camera.x +cw/2 -this.player.w_2) {
        this.player.x = this.camera.x +cw/2 -this.player.w_2;
      }
      if(this.player.y > ch-this.player.h_2) {
        this.player.y = 0;
        this.player.vy = 0;
      }
    }

    checkCollisionPlatforms(player, level) {
      let player_tile_pos = level.getTilePos(player.x, player.y);

      //center
      this.reactCollision(player, level.getPlatform(player_tile_pos.x,   player_tile_pos.y));
      // left
      this.reactCollision(player, level.getPlatform(player_tile_pos.x-1, player_tile_pos.y));
      // right
      this.reactCollision(player, level.getPlatform(player_tile_pos.x+1, player_tile_pos.y));

      // top
      this.reactCollision(player, level.getPlatform(player_tile_pos.x,   player_tile_pos.y-1));
      // bottom
      this.reactCollision(player, level.getPlatform(player_tile_pos.x,   player_tile_pos.y+1));

      // left top
      this.reactCollision(player, level.getPlatform(player_tile_pos.x-1, player_tile_pos.y-1));
      // right top
      this.reactCollision(player, level.getPlatform(player_tile_pos.x+1, player_tile_pos.y-1));

      // left bottom
      this.reactCollision(player, level.getPlatform(player_tile_pos.x-1, player_tile_pos.y+1));
      // right bottom
      this.reactCollision(player, level.getPlatform(player_tile_pos.x+1, player_tile_pos.y+1));
    }

    reactCollision(player, platform, type) {
      if(platform &&
           platform.type === "Platform" &&
           Math.abs(player.x - platform.x) < player.w_2 + platform.w_2 && 
           Math.abs(player.y - platform.y) < player.h_2 + platform.h_2 ) {

        let overlapX = (player.w_2 + platform.w_2) - Math.abs(player.x - platform.x);
        let overlapY = (player.h_2 + platform.h_2) - Math.abs(player.y - platform.y);

        if(overlapX < overlapY) {
          if(player.x - platform.x > 0) {
            player.x += overlapX;
          } else {
            player.x -= overlapX;
          }
        } else if(overlapX > overlapY) {
          if(player.y - platform.y > 0) {
            player.y += overlapY;
            if(player.vy < 0) {
              player.vy = 0;
            }
          } else {
            player.y -= overlapY;
            if(player.vy > 0) {
              player.inFloor = true;
              player.vy = 0;
            }
          }
        }
      }
    }

    render(ctx) {
      ctx.fillStyle = "#5ad0e1";
      ctx.fillRect(0, 0, cw, ch);

      this.camera.applyTransform(ctx);
      this.level.render(ctx);
      this.player.render(ctx);
      // render enemies
      for(let i=0, l=this.level.enemies.length; i<l; i++) {
        this.level.enemies[i].render(ctx);
      }
      // Dibuja la vida del jugador.
      this.life.render(ctx);
      this.camera.releaseTransform(ctx);
    }
  }

  GameEngine.Level1State = Level1State;
  return GameEngine;
})(GameEngine || {})
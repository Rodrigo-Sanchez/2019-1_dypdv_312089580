var GameEngine = (function(GameEngine) {
  let gravity = 16;

  class Enemy3 {
    constructor(x, y, w, h) {
      this.sprite = new GameEngine.Sprite(x, y, w, h, "images/enemy03.png", 2, 16, 16);

      this.x = x;
      this.y = y;
      this.w = w;
      this.h = h;
      this.frameCounter = 0;
      this.framesPerChange = 16;
      this.numFrameAnimation = 2;
      this.w_2 = w/2;
      this.h_2 = h/2;
      this.speed = 120;
      this.jump_heigth = 366;
      this.inFloor = false;
      this.vx = -25;
      this.vy = 0;
      this.canJump = true;
      this.active = false;
    }

    update(elapsed, level, cw, ch, camera, player) {
      if(this.active) {
        this.inFloor = false;
        //this.vx += gravity;
        this.vy += gravity;

        this.x += this.vx * elapsed;
        this.y += this.vy * elapsed;
        this.sprite.x = this.x;
        this.sprite.y = this.y;

        this.frameCounter = (this.frameCounter +1)%(this.framesPerChange*this.numFrameAnimation);
        this.sprite.currentFrame = parseInt(this.frameCounter/this.framesPerChange);
      }
    }

    render(ctx) {
      if(this.active) {
        this.sprite.render(ctx);
      }
    }
  }

  GameEngine.Enemy3 = Enemy3;
  return GameEngine;
})(GameEngine || {})
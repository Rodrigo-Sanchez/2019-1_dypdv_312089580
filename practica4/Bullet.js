var GameEngine = (function(GameEngine) {

  class Bullet extends GameEngine.Sprite {
    constructor(speed=300, activeTime=2) {
      super(2000, 100, 10, 10, "images/bullet.png", 14, 16, 16);
      this.isAlive = false;
      this.speed = speed;
      this.activeTime = activeTime;
      this.auxActiveTime = 1000;
      this.x = 0;
      this.y = 0;
      this.size = 2;
      this.radius = this.size;
    }

    activate(ox, oy, x, y, vx, vy) {
      this.x = ox + x * Math.cos(-0.2) - y * Math.sin(-0.2);
      this.y = oy + y * Math.cos(-0.2) + x * Math.sin(-0.2);

      this.vx = this.direction * vx + this.speed * Math.cos(-0.2);
      this.vy = vy + this.speed * Math.sin(-0.2);
      this.isAlive = true;
    }

    update(elapse) {
      this.x += this.vx * elapse;
      this.y += this.vy * elapse;

      this.auxActiveTime += elapse;
      if(this.auxActiveTime > this.activeTime) {
        this.isAlive = false;
        this.auxActiveTime = 0;
      }
    }

    render(ctx) {
      /*ctx.fillStyle = "#ffffff";
      ctx.beginPath();
      ctx.arc(this.x, this.y, this.size, 0, PI2);
      ctx.fill();*/
      super.render(ctx);
    }
  }

  GameEngine.Bullet = Bullet;
  return GameEngine;
})(GameEngine || {})
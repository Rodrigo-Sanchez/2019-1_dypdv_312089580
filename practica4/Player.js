var GameEngine = (function(GameEngine) {
  let gravity = 16;

  let KEY = GameEngine.KEY;

  class Player extends GameEngine.Sprite {
    constructor(x, y, w, h) {
      super(x, y, w*2, h, "images/megaman.svg", 14, 32, 32);

      this.frameCounter = 0;
      this.framesPerChange = 8;
      this.ladderCounter = 0;
      this.ladderFramesPerChange = 12;
      this.w_2 = w/2;
      this.h_2 = h/2;
      this.jump_heigth = 250;
      this.inFloor = false;
      this.shooting = false;
      this.speed = 95;
      this.vx = 0;
      this.vy = 0;
      this.canJump = true;
      this.canShoot = true;
      this.invencible = false;
      this.weapon = new GameEngine.Weapon(this.direction, 100, 0.2);
    }

    processInput() {
      this.vx = 0;
      this.ladderVy = 0;
      this.tryGrabLadder = false;

      if(KEY.isReleased(KEY.X_KEY)) {
        this.canJump = true;
      }
      if((KEY.isPress(KEY.X_KEY)) && (this.canJump) && (this.inFloor)) {
        this.vy = -this.jump_heigth;
        this.canJump = false;
        if(KEY.isPress(KEY.Z_KEY)) {
          this.shooting = true;
        } else {
          this.shooting = false;
        }
      }
      if((KEY.isPress(KEY.X_KEY)) && (this.inLadder)) {
        this.inLadder = false;
      }
      if(KEY.isPress(KEY.Z_KEY) && (this.canShoot)) {
        this.shooting = true;
      } else {
        this.shooting = false;
      }
      if(KEY.isReleased(KEY.Z_KEY)) {
        this.canShoot = true;
      }
      if(KEY.isPress(KEY.LEFT)) {
        this.vx = -this.speed;
        this.direction = -1;
      }
      if(KEY.isPress(KEY.RIGHT)) {
        this.vx = this.speed;
        this.direction = 1;
      }
      if(KEY.isPress(KEY.UP)) {
        this.tryGrabLadder = true;
        this.ladderVy = -this.speed/2;
        if(this.inLadder) {
          if(((this.ladderCounter++)%this.ladderFramesPerChange) === 0 && (!this.shooting)) {
            this.direction *= -1;
          }
          this.canShoot = this.vy === 0 ? true : false;
        }
      }
      if(KEY.isPress(KEY.DOWN)) {
        this.tryGrabLadder = true;
        this.ladderVy = this.speed/2;
        if(this.inLadder) {
          if(((this.ladderCounter++)%this.ladderFramesPerChange) === 0 && (!this.shooting)) {
            this.direction *= -1;
          }
          this.canShoot = this.vy === 0 ? true : false;
        }
      }
    }

    setState() {
      if(!this.inLadder) {
        if(this.vx !== 0) {
          this.state = "walking";
        } else if(this.inFloor) {
          this.state = "still";
        }
        if(this.shooting) {
            if(this.vx == 0) {
              this.state = "shooting";
            } else {
              this.state = "walking_shooting";
            }
        }
        if(!this.inFloor) {
          this.state = "jumping";
          if(this.shooting) {
            this.state = "jumping_shooting";
          }
        }
      } else {
        if(this.shooting) {
          this.state = "ladder_shooting"
        } else {
        this.state = "ladder";
        }
      }  
    }

    update(elapsed) {
      this.inFloor = false;

      if(!this.inLadder) {
        this.vy += gravity;
        super.update(elapsed);
      } else {
        this.vx = 0;
        this.vy = this.ladderVy;
        super.update(elapsed);
      }

      // Update del weapon.
      this.weapon.auxDelayTime += elapsed;
      if(this.shooting) {
        if(this.weapon.delayActivation < this.weapon.auxDelayTime) {
          this.weapon.shot(this.x, this.y, 5, 0, this.vx, this.vy, this.rotation);
          this.weapon.auxDelayTime = 0;
        }
      }
      this.weapon.update(elapsed);
    }
    
    render(ctx) {
      if(this.state === "still") {
        this.currentFrame = 0;
      } else if(this.state === "walking") {
        this.frameCounter = (this.frameCounter+1)%(this.framesPerChange*4);
        let theFrame = parseInt(this.frameCounter/this.framesPerChange);
        if(theFrame === 3) {
          theFrame = 1;
        }
        this.currentFrame = 1 + theFrame;
      } else if(this.state === "jumping") {
        this.currentFrame = 4;
      } else if(this.state === "ladder") {
        this.currentFrame = 5;
      } else if(this.state === "shooting") {
        this.currentFrame = 7;
      } else if(this.state === "walking_shooting") {
        this.frameCounter = (this.frameCounter+1)%(this.framesPerChange*4);
        let theFrame = parseInt(this.frameCounter/this.framesPerChange);
        if(theFrame === 3) {
          theFrame = 1;
        }
        this.currentFrame = 8 + theFrame;
      } else if(this.state === "jumping_shooting") {
        this.currentFrame = 11;
      } else if(this.state === "ladder_shooting") {
        this.currentFrame = 12;
      } else if(this.state === "hurt") {
        this.currentFrame = 13;
      }

      // Dibuja el contenedor rectangular del player.
      /* ctx.fillStyle = "rgba(100, 50, 25, 0.5)";
      ctx.beginPath();
      ctx.moveTo(this.x - this.w_2, this.y - this.h_2);
      ctx.lineTo(this.x - this.w_2, this.y + this.h_2);
      ctx.lineTo(this.x + this.w_2, this.y + this.h_2);
      ctx.lineTo(this.x + this.w_2, this.y - this.h_2);
      ctx.lineTo(this.x - this.w_2, this.y - this.h_2);
      ctx.fill(); */
      this.weapon.render(ctx);
      super.render(ctx);
    }
  }

  GameEngine.Player = Player;
  return GameEngine;
})(GameEngine || {})
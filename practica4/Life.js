var GameEngine = (function(GameEngine) {

  class Life {
    constructor(x, y, lifes) {
      this.x = x;
      this.y = y;
      this.lifes = lifes;
    }
    
    render(ctx) {
      ctx.fillStyle = "#000";
      ctx.fillRect(this.x, this.y-1, 10, 60);
      ctx.fillStyle = "#f8ee81";
      // Error de cálculo, las vidas las calcula de arriba a abajo. :v
      for(let lifes2 = this.lifes; lifes2 >= 0; lifes2--) {
        ctx.strokeRect(this.x+1, this.y+(2*lifes2), 8, 2);
        ctx.fillRect(this.x+1, this.y+(2*lifes2), 8, 2);
      }
      /* for(let lifes2 = 0; lifes2 < this.lifes; lifes2++) {
        ctx.fillRect(this.x, this.y+(3*this.lifes), 10, 3);
        console.log(lifes2);
      } */
    }
  }

  GameEngine.Life = Life;
  return GameEngine;
})(GameEngine || {})